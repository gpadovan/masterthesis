# Master Thesis repository

Thesis title: "Search for invisible Higgs boson decays in the VBF production channel with the ATLAS experiment at the LHC"

Advisor: Prof. S. Giagu (Sapienza University of Rome)

Period: March 2019 - March 2020

## Description of the project and contents
This repository contains the code developed during the Master thesis project. The thesis was focused on the search for invisible Higgs boson decays in the VBF production channel and was developed inside the ATLAS-Roma1 research group.

The code contained in this repository performs the selection of events according to the measurement strategy defined by the VBF+MET analysis team of the ATLAS experiment.

The repository inlcudes the following contents:
* **analysisSingleSelector**: folder containing the majority of the source code that performs the selection of events, both on data, nominal MC and systematic-variate MC.
* **outputFilesSingleSelector**: folder containig the output rootfiles coming from the selection of events and summary files containing result of events selection for data and all MC samples. The folder contains also the final rootfiles to be given to HistFitter framework to perform statistical analysis of results.
* **plotsSingleSelector**: folder containing all plots. In particular are available: plots of all the kinematical variables used in the selection of events, prefit summary plot, plots of some example systematics, final brazilian plots representing results of limit extraction with CLs method.
* **postfit**: folder containing output rootfiles from HistFitter framework for statistical analysis and some additional service code to produce postfit plots, summary tables etc... 
* **MasterThesis_PadovanoGiovanni.pdf**: final .pdf version of the thesis.

## Index of the programs

1. **Launcher.C**: launches TSelector ntupleToHistoSelector.C on nominal TTrees
2. **Plotter.C**: plots all kinematical variables of interest
3. **PlotterPrefitHisto.C**: produces the final prefit histogram (data+MC, signal region + all control regions)
4. **Launcher_sys.C**: launches TSelector ntupleToHistoSelector_sys.C on all TTrees (nominal + systematic-variate)
5. **MakeDataDrivenHisto.C**: produces the histograms for data-driven estimated MC samples (multijet, eleFakes)
6. **mergeSysFiles.sh**: makes hadd of all rootfiles containing histograms of the analysis
7. **MakeHistoRatio.C**: makes ratios of histograms (systematic-variate/nominal) to prepare the input for statistical analysis with HistFitter
8. **MakeInputFit.C**: prapares input for HistFitter, renaming histograms with the nomenclature defined by the VBF+MET analysis team
9. **MakePlotsSystematics.C**: creates demo histogram to show the effetc of some systematics
10. **SummaryFitResults.C**: produces brazilian plot with results of limit extraction
11. **LauncherReopt.C**: obsolete
12. **cpInputToHistFitterDir.sh**: obsolete
  
**Programs referring to the fit procedure and postfit analysis**

13. **MakeYieldsTables.sh**: calls HistFitter to produce YieldsTables
14. **MakeRootFiles.sh**: creates rootfiles from HistFitter .tex output, with histograms representing all postfit yields
15. **PlotterPostFitHisto.C**: produces the final postfit summary plot
16. **PrintPostfitYields.C**: produces a .txt file with all the postfit yields

## References for the fitting procedure
To implement the fitting procedure the original code by the VBF+MET analysis team has to be used, available on GitLab repository: https://gitlab.cern.ch/VBFInv/StatsTools . The main developer is Loan Truong (loan.truong@cern.ch).

It is teherefore necessary to:
- clone locally the StatsTools repository
- follow the instructions and get a working installation of the HistFitter framework for statistical analysis
- run the fit passing as input rootfiles produced by the selection code contained in this repository.

After the fit has been performed results can be extracted and represented graphically using some postfit macros, contained in this repository.

## Complete procedure to follow to implement the analysis

**First part: selection and preparation of input for fit**
* run 1., nominal selection
* run 5., sample data-driven
* run 2., plot kinematical variables
* run 3., plot prefit
* run 4., systematic selection
* run 6., merge rootfiles
* run 7., was used to evaluate ratios, in the final version is useless, but you run it to be consistent with previous architecture
* run 8., prepares input for fit. To be launcehd 3 times with different option for systematics study. Need to rename the files properly
* move input file for fit into the proper folder in the HistFitter framework `[...]/StatsTools/makeHistFitterConfig/input`

**Second part: perform the fit**

*This part of the procedure relies on the StatsTools framework developed by the VBF+MET analysis team. It is therefore necessary to install the framework and move accrodingly to the folders of the installation.*

* move to the folder `[...]/StatsTools/makeHistFitterConfig`
* run fit using proper config file
* move to folder `[...]/StatsTools/HistFitterCode/HistFitterTutorial`
* run 13., produce yields tables in .tex

**Third part: extract fit results**
* move to the forlder `[...]/postfit`
* run 14., creates rootfiles with histograms representing postfit yields (also useless prefit yields)
* run 15., produce final postfit plot
* run 16., produces summary .txt file with all postfit yields

--------------------------------
Author: G. Padovano, INFN Roma & Sapienza Universita' di Roma
