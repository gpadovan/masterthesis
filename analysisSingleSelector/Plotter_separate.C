// Plotter_separate.C
// plots hitograms for all interestin kinematics variables

// backup_v5 _v6 _v7 _v8

#include "/afs/cern.ch/user/g/gpadovan/work/atlasrootstyle/AtlasStyle.C"

void doPlot(TString control_region, TString variable){

  SetAtlasStyle(); 
  TH1::SetDefaultSumw2();
  gStyle->SetOptStat(0);

  TCanvas *c1 = nullptr;
  
  const TString Lint = "36.2";
  const TString ytitle = "Events";
  const TString y_ratiotitle = "Data / SM";
  const Float_t y_pad_divider = 0.3;
  
  const TString xtitle = ( variable  == ("lead_jet_fjvt") ? "lead. jet fJVT" :
			   ( variable  == ("sublead_jet_fjvt") ? "sublead. jet fJVT" :
			     "error name of variable") );
			     
  TString control_region_tag = control_region == ("SR") ? "Signal Region, SR" : 
    ( control_region == ("CRWe") ? "W #rightarrow e#nu, WCR" :
      ( control_region == ("CRWm") ? "W #rightarrow #mu#nu, WCR" :
	( control_region == ("CRZee") ? "Z #rightarrow ee, ZCR" :
	  ( control_region == ("CRZmm") ? "Z #rightarrow #mu#mu, ZCR" :
	    "error name of control region" ) ) ) );  
  
  TFile *inputdata = nullptr;
  inputdata = new TFile("../outputFilesSingleSelector/nominal/outfile_data.root");
  TH1F *h_data = dynamic_cast<TH1F*> (inputdata->Get(control_region + "_" + variable));
  
  //blinding SR
  //if(control_region=="SR") for(UInt_t i = 0; i < h_data->GetNbinsX(); i++) {h_data->SetBinContent(i+1,0); h_data->SetBinError(i+1,0);}
  
  vector<TString> samples = {"W_strong","W_EWK","Z_strong","Z_EWK","ttbar","VBFH125","ggFH125"};
  vector<TFile*> inputfiles;

  for(UInt_t i = 0; i < samples.size(); i++) inputfiles.push_back(new TFile("../outputFilesSingleSelector/nominal/outfile_"+samples.at(i)+".root"));
  
  std::map<TString, TH1F*> map_mc;
  std::map<TString, TString> map_legend;

  for(UInt_t i = 0; i < inputfiles.size(); i++){
    map_mc[samples.at(i)] = dynamic_cast<TH1F*> (inputfiles.at(i)->Get(control_region + "_" + variable));
    map_legend[samples.at(i)] = samples.at(i);
  }

  map_mc["W_strong"]    -> SetLineColor(TColor::GetColor("#5DBCD2"));         map_mc["W_strong"]    -> SetFillColor(TColor::GetColor("#5DBCD2"));
  map_mc["W_EWK"]   -> SetLineColor(TColor::GetColor("#9FDAEA"));         map_mc["W_EWK"]   -> SetFillColor(TColor::GetColor("#9FDAEA"));
  map_mc["Z_strong"]  -> SetLineColor(TColor::GetColor("#F3A66E"));         map_mc["Z_strong"]  -> SetFillColor(TColor::GetColor("#F3A66E"));
  map_mc["Z_EWK"]   -> SetLineColor(TColor::GetColor("#FAB637"));         map_mc["Z_EWK"]   -> SetFillColor(TColor::GetColor("#FAB637"));
  map_mc["ttbar"] -> SetLineColor(TColor::GetColor("#4183C0"));         map_mc["ttbar"] -> SetFillColor(TColor::GetColor("#4183C0"));
  map_mc["VBFH125"]     -> SetLineColor(TColor::GetColor("#FFFFFF"));         map_mc["VBFH125"]     -> SetFillColor(TColor::GetColor("#FFFFFF"));
  map_mc["ggFH125"]   -> SetLineColor(TColor::GetColor("#FFFFFF"));        map_mc["ggFH125"]   -> SetFillColor(TColor::GetColor("#FFFFFF"));


  /*
  //colori originali di Guglielmo  
  map_mc["W_strong"]    -> SetLineColor(TColor::GetColor("#9FDAEA"));         map_mc["W_strong"]    -> SetFillColor(TColor::GetColor("#9FDAEA"));
  map_mc["W_EWK"]   -> SetLineColor(TColor::GetColor("#5DBCD2"));         map_mc["W_EWK"]   -> SetFillColor(TColor::GetColor("#5DBCD2"));
  map_mc["Z_strong"]  -> SetLineColor(TColor::GetColor("#4183C0"));         map_mc["Z_strong"]  -> SetFillColor(TColor::GetColor("#4183C0"));
  map_mc["Z_EWK"]   -> SetLineColor(TColor::GetColor("#FAB637"));         map_mc["Z_EWK"]   -> SetFillColor(TColor::GetColor("#FAB637"));
  map_mc["ttbar"] -> SetLineColor(TColor::GetColor("#F3A66E"));         map_mc["ttbar"] -> SetFillColor(TColor::GetColor("#F3A66E"));
  map_mc["VBFH125"]     -> SetLineColor(TColor::GetColor("#D44D30"));         map_mc["VBFH125"]     -> SetFillColor(TColor::GetColor("#D44D30"));
  map_mc["ggFH125"]   -> SetLineColor(TColor::GetColor("#FD6965"));        map_mc["ggFH125"]   -> SetFillColor(TColor::GetColor("#FD6965"));
  */

  map_legend["W_strong"] = "W (strong)"; // scritte calligrafiche con LaTex per la legenda
  map_legend["W_EWK"] = "W (EWK)";
  map_legend["Z_strong"] = "Z (strong)";
  map_legend["Z_EWK"] = "Z (EWK)";
  map_legend["ttbar"] = "t#bar{t}";
  map_legend["VBFH125"] = "VBF (H 125 GeV)";
  map_legend["ggFH125"] = "ggF (H 125 GeV)";

  vector<TString> order_bkg;
  vector<TString> order_sgn_bkg;
  if(control_region == "SR")       order_bkg = {"ttbar","W_EWK","Z_EWK","W_strong","Z_strong"};
  if(control_region == "CRWe")  order_bkg = {"ttbar","Z_EWK","W_EWK","Z_strong","W_strong"};
  if(control_region == "CRWm")    order_bkg = {"ttbar","Z_EWK","W_EWK","Z_strong","W_strong"};
  if(control_region == "CRZee")    order_bkg = {"ttbar","W_EWK","Z_EWK","W_strong","Z_strong"};
  if(control_region == "CRZmm")    order_bkg = {"ttbar","W_EWK","Z_EWK","W_strong","Z_strong"};
  
  order_sgn_bkg.push_back("ggFH125");
  order_sgn_bkg.push_back("VBFH125");
  
  THStack *h_stack_bkg = new THStack ("h_stack_bkg","h_stack_bkg"); 
  for(UInt_t i = 0; i < order_bkg.size(); i++) h_stack_bkg->Add( map_mc[order_bkg.at(i)] );

  THStack *h_stack_sgn_bkg = new THStack ("h_stack_sgn_bkg","h_stack_sgn_bkg"); 
  for(UInt_t i = 0; i < order_sgn_bkg.size(); i++) h_stack_sgn_bkg->Add( map_mc[order_sgn_bkg.at(i)] );
  
  //creazione del plot
  TH1F *empty_scale = dynamic_cast<TH1F*>(map_mc[samples.at(0)]->Clone("empty_scale"));
  empty_scale->Reset();
  empty_scale->GetYaxis()->SetRangeUser(/*1e-2*/2e-2,1e05); // cambia con i miei range (fatto)
    
  empty_scale->SetTitle(";;"+ytitle);

  c1 = new TCanvas(control_region + "_" + variable,"",800,600);
  TPad *p1 = new TPad("p1","p1",0,y_pad_divider,1,1);
  p1->SetBottomMargin(0);
  p1->SetTickx();
  p1->SetTicky();
  p1->SetLogy();
  p1->Draw();
  p1->cd();
  
  empty_scale->Draw();
  //h_data->Draw("same"); //istruzione inutile (inserita con guglielmo)
  h_stack_bkg->Draw("same HIST");

  auto header = new TLatex;
  header->DrawLatexNDC(0.195,0.885,"#sqrt{s} = 13 TeV, "+Lint+" fb^{-1}");
  header->DrawLatexNDC(0.195,0.82,control_region_tag);

  c1->cd();

  TPad *p2 = new TPad("p2","p2",0,0,1,y_pad_divider);
  p2->SetTopMargin(0);
  p2->SetBottomMargin(0.3);
  p2->SetTickx();
  p2->SetTicky();
  p2->SetGridy();
  p2->Draw();
  p2->cd();

  TList *mc_list_bkg = new TList; // somma di MC di fondo
  for(UInt_t i = 0; i < order_bkg.size(); i++) mc_list_bkg->Add(map_mc[order_bkg.at(i)]);

  TList *mc_list_sgn_bkg = new TList; // somma di MC di segnale e fondo
  for(UInt_t i = 0; i < order_sgn_bkg.size(); i++) mc_list_sgn_bkg->Add(map_mc[order_sgn_bkg.at(i)]);
  
  TH1F *h_yield_data = dynamic_cast<TH1F*>(h_data->Clone("yields_data"));
  TH1F *h_ratio = dynamic_cast<TH1F*>(h_data->Clone("ratio_data"));
  
  TH1F *mc_merged_bkg = dynamic_cast<TH1F*>(map_mc[samples.at(0)]->Clone("mc_merged_bkg"));
  mc_merged_bkg->Reset();
  mc_merged_bkg->Merge(mc_list_bkg);
  
  TH1F *mc_merged_sgn_bkg = dynamic_cast<TH1F*>(map_mc[samples.at(0)]->Clone("mc_merged_sgn_bkg"));
  mc_merged_sgn_bkg->Reset();
  mc_merged_sgn_bkg->Merge(mc_list_sgn_bkg);
  

  p1->cd();
  TH1F *border_sgn_bkg = dynamic_cast<TH1F*>(mc_merged_sgn_bkg->Clone("border_sgn_bkg")); //disegno bordo istogramma di sopra con sgn+bkg
  TH1F *border_bkg = dynamic_cast<TH1F*>(mc_merged_bkg->Clone("border_bkg")); //disegno bordo istogramma di sopra con bkg
  TH1F *mc_Cband = dynamic_cast<TH1F*>(border_sgn_bkg->Clone("mc_Cband")); // disegno bordo nel RPlot

  // plot B+S only in the SR plots
  if(control_region.Contains("SR")) {
    border_sgn_bkg->SetLineColor(/*TColor::GetColor("#D44D30")*/kBlack); //vecchio colore gugliemo: 13
    border_sgn_bkg->SetFillColor(TColor::GetColor("#F3A66E"));
    border_sgn_bkg->SetLineWidth(3); //aggiungo spessore riga (non c'era da guglielmo)
    border_sgn_bkg->SetLineStyle(2);
    border_sgn_bkg->SetFillStyle(0);
    border_sgn_bkg->SetMarkerSize(0);
    border_sgn_bkg->Draw("SAME HIST");
  }
  
  border_bkg->SetLineColor(13);
  border_bkg->SetLineWidth(2); //aggiungo spessore riga (non c'era da guglielmo)
  border_bkg->SetFillStyle(0);
  border_bkg->SetMarkerSize(0);
  border_bkg->Draw("SAME HIST");
  
  mc_Cband->SetFillColor(13);
  mc_Cband->SetFillStyle(3345);
  mc_Cband->SetLineColor(13);
  mc_Cband->SetLineWidth(1);
  mc_Cband->SetMarkerSize(0);
  
  TGraphAsymmErrors *band_p1 = new TGraphAsymmErrors();
  for(UInt_t i = 1; i < mc_Cband->GetNbinsX()+1; i++){
    band_p1->SetPoint(i, mc_merged_bkg->GetBinCenter(i), mc_merged_bkg->GetBinContent(i));
    band_p1->SetPointEYlow(i, mc_merged_bkg->GetBinErrorLow(i));
    band_p1->SetPointEYhigh(i, mc_merged_bkg->GetBinErrorUp(i));
    band_p1->SetPointEXlow(i, mc_merged_bkg->GetBinCenter(i) - mc_merged_bkg->GetBinLowEdge(i));
    band_p1->SetPointEXhigh(i, mc_merged_bkg->GetBinLowEdge(i+1) - mc_merged_bkg->GetBinCenter(i));
  }
  band_p1->SetFillStyle(3345);
  band_p1->SetFillColor(13);
  band_p1->SetMarkerSize(0);
  band_p1->SetLineWidth(0);
  band_p1->SetLineColor(13);
  band_p1->Draw("e2 SAME");
  
  h_data->SetMarkerSize(1);
  h_data->Draw("same ep");

  TLegend *ld = nullptr;
  ld = new TLegend(0.67,0.47,0.96,0.89); //(x1,y1,x2,y2);
  // ad hoc positioning of legend for jj_deta variable
  if(variable.Contains("jj_deta")) {
    ld = new TLegend(0.67-0.48,0.47-0.4,0.96-0.48,0.89-0.4); //(x1,y1,x2,y2);
  }
  ld->SetTextFont(42);
  ld->SetFillColor(0);
  ld->SetFillStyle(0);
  ld->SetBorderSize(0);
  ld->AddEntry(h_data,"Data", "p");
  if(control_region.Contains("SR"))  ld->AddEntry(border_sgn_bkg, "Signal");
  ld->AddEntry(mc_Cband,"SM weight unc.","lf");
  std::reverse(order_bkg.begin(),order_bkg.end());
  for(UInt_t i = 0; i < order_bkg.size(); i++) ld->AddEntry(map_mc[order_bkg.at(i)], map_legend[order_bkg.at(i)], "f"); 
  ld->Draw();

  p2->cd();

  TH1F *mc_merged_bkg_copy = (TH1F*)mc_merged_bkg->Clone("mc_merged_bkg_copy");
  for(UInt_t j = 0; j < mc_merged_bkg->GetNbinsX(); j++) mc_merged_bkg_copy->SetBinError(j+1,0);
  h_ratio->Divide(mc_merged_bkg_copy); 

  h_ratio->GetYaxis()->SetLabelSize(0.11);
  h_ratio->GetYaxis()->SetTitleSize(0.11);
  h_ratio->GetYaxis()->SetTitleOffset(0.63);

  h_ratio->GetXaxis()->SetLabelSize(0.11);
  h_ratio->GetXaxis()->SetTitleSize(0.11);
  h_ratio->GetXaxis()->SetTitleOffset(1.1);

  h_ratio->SetMarkerSize(1);
  h_ratio->SetMarkerStyle(20);
  
  TGraphAsymmErrors *band = new TGraphAsymmErrors();
  for(UInt_t i = 1; i < mc_Cband->GetNbinsX()+1; i++){
    band->SetPoint(i, mc_merged_bkg->GetBinCenter(i), 1);
    band->SetPointEYlow(i, mc_merged_bkg->GetBinErrorLow(i)/ mc_merged_bkg->GetBinContent(i));
    band->SetPointEYhigh(i, mc_merged_bkg->GetBinErrorUp(i)/ mc_merged_bkg->GetBinContent(i));
    band->SetPointEXlow(i, mc_merged_bkg->GetBinCenter(i) - mc_merged_bkg->GetBinLowEdge(i));
    band->SetPointEXhigh(i, mc_merged_bkg->GetBinLowEdge(i+1) - mc_merged_bkg->GetBinCenter(i));
  }

  c1->Update();
  band->SetFillStyle(3345);
  band->SetLineColor(16);
  band->SetFillColor(kGray);

  h_ratio->GetYaxis()->SetNdivisions(505);

  h_ratio->Draw("ep");
  band->Draw("e2 SAME");
  h_ratio->Draw("ep same");
  p2->RedrawAxis();

  const Float_t y_ratiorange_up = 1.9/*2.*/;//1.24; //1.12 //vecchi range Guglielmo
  const Float_t y_ratiorange_down   = 0.1/*0.*/; //0.76; //0.88

  h_ratio->GetYaxis()->SetRangeUser(y_ratiorange_down,y_ratiorange_up);
  h_ratio->SetTitle(";" + xtitle + ";" + y_ratiotitle);
  h_ratio->Draw("e same");

  p2->RedrawAxis();

  TH1F *empty_gray = new TH1F("eg","",1,0,1);
  empty_gray->SetLineColor(16);
  empty_gray->SetFillColor(16);
  empty_gray->SetFillStyle(3345);
  
  TLegend *lr = new TLegend(0.19,0.48-0.03+0.005,0.54,0.34-0.03+0.005);
  lr->SetBorderSize(0);
  lr->SetFillStyle(0);
  lr->SetTextSize(0.09);
  lr->SetTextFont(42);
  lr->AddEntry(empty_gray,"Stat. only","f");
  lr->Draw();

  p2->RedrawAxis("g");
  p1->RedrawAxis();

  TString outPlotPath = "/afs/cern.ch/user/g/gpadovan/work/vbfinv/plotsSingleSelector/";
  
  c1->SaveAs(outPlotPath+control_region+"_"+variable+"_separate.pdf");
  
  //pulisco la memoria prima di chiudere la funzione
  delete c1;
  h_ratio->Delete();
  mc_merged_bkg->Delete();
  map_mc.clear();
  
}


void Plotter_separate(void){

  vector<TString> varkin_SR = {"lead_jet_fjvt", "sublead_jet_fjvt"};
  vector<TString> varkin_CRWe = {"lead_jet_fjvt", "sublead_jet_fjvt"};
  vector<TString> varkin_CRWm = {"lead_jet_fjvt", "sublead_jet_fjvt"};
  vector<TString> varkin_CRZee = {"lead_jet_fjvt", "sublead_jet_fjvt"};
  vector<TString> varkin_CRZmm = {"lead_jet_fjvt", "sublead_jet_fjvt"};

  for(TString v : varkin_SR) doPlot("SR",v);
  for(TString v : varkin_CRWe) doPlot("CRWe",v);
  for(TString v : varkin_CRWm) doPlot("CRWm",v);
  for(TString v : varkin_CRZee) doPlot("CRZee",v);
  for(TString v : varkin_CRZmm) doPlot("CRZmm",v);
  
}
