# mergeSysFiles.sh

#backup_v5 _v6 _v7 _v8

echo "================ merge sampple: W_strong ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_W_strong.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/W_strong*.root 

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/W_strong*.root

echo "================ merge sampple: W_EWK ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_W_EWK.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/W_EWK*.root

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/W_EWK*.root

echo "================ merge sampple: Z_strong ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_Z_strong.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/Z_strong*.root

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/Z_strong*.root

echo "================ merge sampple: Z_EWK ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_Z_EWK.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/Z_EWK*.root

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/Z_EWK*.root

echo "================ merge sampple: ttbar ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_ttbar.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/ttbar*.root

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/ttbar*.root

echo "================ merge sampple: VBFH125 ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_VBFH125.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/VBFH125*.root

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/VBFH125*.root

echo "================ merge sampple: ggFH125 ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_ggFH125.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/ggFH125*.root

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/ggFH125*.root

echo "================ merge sampple: multijet ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_multijet.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/multijet*.root

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/multijet*.root

echo "================ merge sampple: eleFakes ================"
hadd -f /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/final_eleFakes.root /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/eleFakes*.root

#rm /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/systematics/eleFakes*.root
