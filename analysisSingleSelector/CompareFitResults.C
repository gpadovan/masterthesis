//CompareFitResults.C


void CompareFitResults() {

  TString fitName = TString("fJVT"); // choose from "withDD" "fJVT"
  
  gStyle->SetOptStat(0);
  
  std::map<TString, Float_t > map_obs;
  std::map<TString, Float_t > map_exp;
  std::map<TString, Float_t > map_exp1min;
  std::map<TString, Float_t > map_exp1plus;
  std::map<TString, Float_t > map_exp2min;
  std::map<TString, Float_t > map_exp2plus;
  
  vector<TString> limit_names = {"without_fJVT","with_fJVT"};
  
  map_obs["without_fJVT"] = 0.293792;
  map_exp["without_fJVT"] = 0.247631;
  map_exp1min["without_fJVT"] = 0.180854;
  map_exp1plus["without_fJVT"] = 0.340993;
  map_exp2min["without_fJVT"] = 0.134765;
  map_exp2plus["without_fJVT"] = 0.453685;
  
  map_obs["with_fJVT"] = 0.249542;
  map_exp["with_fJVT"] = 0.240536;
  map_exp1min["with_fJVT"] = 0.174989;
  map_exp1plus["with_fJVT"] = 0.331949;
  map_exp2min["with_fJVT"] = 0.129799;
  map_exp2plus["with_fJVT"] = 0.440725;
    

  TCanvas* canv = new TCanvas("canvas","canvas for plotting", 1280, 1024);
  canv->cd();
  
  TGraphAsymmErrors* graph_1sig = new TGraphAsymmErrors();
  TGraphAsymmErrors* graph_2sig = new TGraphAsymmErrors();
  TGraphAsymmErrors* graph_exp = new TGraphAsymmErrors();
  TGraphAsymmErrors* graph_obs = new TGraphAsymmErrors();
  
  for( Int_t i=0; i<limit_names.size(); i++) {
    
    graph_2sig->SetPoint(i,i+0.5,map_exp[limit_names.at(i)]);  
    graph_2sig->SetPointError(i,0.5,0.5,map_exp[limit_names.at(i)]-map_exp2min[limit_names.at(i)],map_exp2plus[limit_names.at(i)]-map_exp[limit_names.at(i)]);
    
    graph_1sig->SetPoint(i,i+0.5,map_exp[limit_names.at(i)]);  
    graph_1sig->SetPointError(i,0.5,0.5,map_exp[limit_names.at(i)]-map_exp1min[limit_names.at(i)],map_exp1plus[limit_names.at(i)]-map_exp[limit_names.at(i)]);
    
    graph_exp->SetPoint(i,i+0.5,map_exp[limit_names.at(i)]);  
    graph_exp->SetPointError(i,0.5,0.5,0,0);

    graph_obs->SetPoint(i,i+0.5,map_obs[limit_names.at(i)]);  
    graph_obs->SetPointError(i,0.5,0.5,0,0);
    
  }

  TH1F* empty = new TH1F("","",limit_names.size(),0,limit_names.size());
  empty->GetYaxis()->SetRangeUser(0.04,0.65);
  empty->GetYaxis()->SetTitle("Limit on BR(H #rightarrow inv.)");
  empty->GetXaxis()->SetLabelSize(0.055);
  empty->GetXaxis()->SetBinLabel(1,"without fJVT");
  empty->GetXaxis()->SetBinLabel(2,"with fJVT");

  empty->Draw();
  
  graph_2sig->SetFillColor(kGreen);
 
  graph_2sig->Draw("psame");
  graph_2sig->Draw("E2same");
    
  graph_1sig->SetFillColor(kYellow);
  graph_1sig->Draw("E2same");  
  
  graph_exp->SetLineWidth(2);
  graph_exp->SetLineStyle(3);
  graph_exp->Draw("esame");

  graph_obs->SetLineWidth(2);
  graph_obs->SetLineStyle(1);
  graph_obs->Draw("esame");
  
  canv->RedrawAxis();
  canv->SetTicks(1,1);
  
  TLegend* legend = new TLegend(.15,.75-0.05,.5 ,.85, "");
  legend->SetBorderSize(0);
  legend->AddEntry(graph_obs, "Observed CLs");
  legend->AddEntry(graph_exp, "Expected CLs");
  legend->AddEntry(graph_1sig,"Expeted CLs #pm 1 #sigma");
  legend->AddEntry(graph_2sig,"Expeted CLs #pm 2 #sigma");
  legend->Draw(); 
  
  canv->SaveAs("/afs/cern.ch/user/g/gpadovan/work/vbfinv/plotsSingleSelector/plots_limits/comparison_limits.pdf");
  

}
