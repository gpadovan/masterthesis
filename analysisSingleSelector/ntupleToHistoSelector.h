//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Oct 31 20:18:49 2019 by ROOT version 6.14/04
// from TTree dataNominal/dataNominal
// found on file: ../data/vbfinv_v26/data.root
//////////////////////////////////////////////////////////

// backup_v5 _v6 _v7 _v8

#ifndef ntupleToHistoSelector_h
#define ntupleToHistoSelector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>


// my ROOT header files
#include "TLorentzVector.h"

// Headers needed by this particular selector
#include <vector>



class ntupleToHistoSelector : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain
   
   TString option;


   // ==== declaration of variables
   vector<TString> bins_3bs;
   vector<TString> bins_7bs;
   
   Float_t lumiInt; // integrated luminosity
   Float_t m_w; // modified weight, accounting for normalization to int. luminosity

   Float_t SR_n_evts; // conters of evts. in each region

   Float_t CRWe_n_evts;
   Float_t CRWm_n_evts;
   
   Float_t CRZee_n_evts;
   Float_t CRZmm_n_evts;
   
   Float_t CRWeLowMetSig_n_evts;

   Float_t CRWep_n_evts; // charge-split regions counter of evts.
   Float_t CRWen_n_evts;
   
   Float_t CRWmp_n_evts;
   Float_t CRWmn_n_evts;
   
   Float_t CRWepLowMetSig_n_evts;
   Float_t CRWenLowMetSig_n_evts;

   std::map<TString, Float_t> SR_n_evts_3bs; // 3-bin strategy  counter of evts.
   
   std::map<TString, Float_t> CRZee_n_evts_3bs;
   std::map<TString, Float_t> CRZmm_n_evts_3bs;
   
   std::map<TString, Float_t> CRWep_n_evts_3bs;
   std::map<TString, Float_t> CRWen_n_evts_3bs;
   
   std::map<TString, Float_t> CRWmp_n_evts_3bs;
   std::map<TString, Float_t> CRWmn_n_evts_3bs;
   
   std::map<TString, Float_t> CRWepLowMetSig_n_evts_3bs;
   std::map<TString, Float_t> CRWenLowMetSig_n_evts_3bs;
   
   std::map<TString, Float_t> SR_n_evts_7bs; // 7-bin strategy  counter of evts.
   
   std::map<TString, Float_t> CRZee_n_evts_7bs;
   std::map<TString, Float_t> CRZmm_n_evts_7bs;
   
   std::map<TString, Float_t> CRWep_n_evts_7bs;
   std::map<TString, Float_t> CRWen_n_evts_7bs;
   
   std::map<TString, Float_t> CRWmp_n_evts_7bs;
   std::map<TString, Float_t> CRWmn_n_evts_7bs;
   
   std::map<TString, Float_t> CRWepLowMetSig_n_evts_7bs;
   std::map<TString, Float_t> CRWenLowMetSig_n_evts_7bs;   

   /* Float_t SR_low_dphi_n_evts; // 7-bin-strategy regions counter of evts. */
   /* Float_t SR_high_dphi_n_evts; */
   /* Float_t SR_high_n_jet_n_evts; */
   
   /* Float_t CRZee_low_dphi_n_evts; */
   /* Float_t CRZee_high_dphi_n_evts; */
   /* Float_t CRZee_high_n_jet_n_evts; */
   
   /* Float_t CRZmm_low_dphi_n_evts; */
   /* Float_t CRZmm_high_dphi_n_evts; */
   /* Float_t CRZmm_high_n_jet_n_evts; */

   /* Float_t CRWep_low_dphi_n_evts; */
   /* Float_t CRWep_high_dphi_n_evts; */
   /* Float_t CRWep_high_n_jet_n_evts; */
   
   /* Float_t CRWen_low_dphi_n_evts; */
   /* Float_t CRWen_high_dphi_n_evts; */
   /* Float_t CRWen_high_n_jet_n_evts; */

   /* Float_t CRWmp_low_dphi_n_evts; */
   /* Float_t CRWmp_high_dphi_n_evts; */
   /* Float_t CRWmp_high_n_jet_n_evts; */

   /* Float_t CRWmn_low_dphi_n_evts; */
   /* Float_t CRWmn_high_dphi_n_evts; */
   /* Float_t CRWmn_high_n_jet_n_evts; */

   /* Float_t CRWepLowMetSig_low_dphi_n_evts; */
   /* Float_t CRWepLowMetSig_high_dphi_n_evts; */
   /* Float_t CRWepLowMetSig_high_n_jet_n_evts; */

   /* Float_t CRWenLowMetSig_low_dphi_n_evts; */
   /* Float_t CRWenLowMetSig_high_dphi_n_evts; */
   /* Float_t CRWenLowMetSig_high_n_jet_n_evts; */
   
   
   /* Bool_t passJetCuts; // vecchie variabili. RIMUOVERE. */
   /* Bool_t passMetCuts; */
   /* Bool_t passMetNolepCuts; */
   
   Bool_t passSR; // bool variables to select regions
  
   Bool_t passCRWe;
   Bool_t passCRWm;
   
   Bool_t passCRZee;
   Bool_t passCRZmm;
   
   Bool_t passCRWeLowMetSig;    
   
   Bool_t passCRWep; // charge-split regions bools
   Bool_t passCRWen;

   Bool_t passCRWmp;
   Bool_t passCRWmn;

   Bool_t passCRWepLowMetSig;
   Bool_t passCRWenLowMetSig;
   
   std::map<TString, Bool_t> pass3bs; // additional 3-bin strategy bools maps
   
   std::map<TString, Bool_t> passSR_3bs;

   std::map<TString, Bool_t> passCRZee_3bs;
   std::map<TString, Bool_t> passCRZmm_3bs;
   
   std::map<TString, Bool_t> passCRWep_3bs;
   std::map<TString, Bool_t> passCRWen_3bs;
   
   std::map<TString, Bool_t> passCRWmp_3bs;
   std::map<TString, Bool_t> passCRWmn_3bs;
   
   std::map<TString, Bool_t> passCRWepLowMetSig_3bs;
   std::map<TString, Bool_t> passCRWenLowMetSig_3bs;
   
   Bool_t pass_low_dphi; // additional 7-bin strategy bools
   Bool_t pass_high_dphi;
   
   Bool_t pass_low_jj_mass;
   Bool_t pass_medium_jj_mass;
   Bool_t pass_high_jj_mass;
   
   std::map<TString, Bool_t> pass7bs; // additional 7-bin strategy bools maps
   
   std::map<TString, Bool_t> passSR_7bs;

   std::map<TString, Bool_t> passCRZee_7bs;
   std::map<TString, Bool_t> passCRZmm_7bs;
   
   std::map<TString, Bool_t> passCRWep_7bs;
   std::map<TString, Bool_t> passCRWen_7bs;
   
   std::map<TString, Bool_t> passCRWmp_7bs;
   std::map<TString, Bool_t> passCRWmn_7bs;
   
   std::map<TString, Bool_t> passCRWepLowMetSig_7bs;
   std::map<TString, Bool_t> passCRWenLowMetSig_7bs;
   
   
   /* Bool_t pass_low_dphi; // common 7-bin-strategy bools */
   /* Bool_t pass_high_dphi; */
   /* Bool_t pass_high_n_jet; */
   
   /* Bool_t passSR_low_dphi; // 7-bin-strategy regions bools */
   /* Bool_t passSR_high_dphi; */
   /* Bool_t passSR_high_n_jet; */
   
   /* Bool_t passCRZee_low_dphi; */
   /* Bool_t passCRZee_high_dphi; */
   /* Bool_t passCRZee_high_n_jet; */
   
   /* Bool_t passCRZmm_low_dphi; */
   /* Bool_t passCRZmm_high_dphi; */
   /* Bool_t passCRZmm_high_n_jet; */

   /* Bool_t passCRWep_low_dphi; */
   /* Bool_t passCRWep_high_dphi; */
   /* Bool_t passCRWep_high_n_jet; */
   
   /* Bool_t passCRWen_low_dphi; */
   /* Bool_t passCRWen_high_dphi; */
   /* Bool_t passCRWen_high_n_jet; */

   /* Bool_t passCRWmp_low_dphi; */
   /* Bool_t passCRWmp_high_dphi; */
   /* Bool_t passCRWmp_high_n_jet; */

   /* Bool_t passCRWmn_low_dphi; */
   /* Bool_t passCRWmn_high_dphi; */
   /* Bool_t passCRWmn_high_n_jet; */

   /* Bool_t passCRWepLowMetSig_low_dphi; */
   /* Bool_t passCRWepLowMetSig_high_dphi; */
   /* Bool_t passCRWepLowMetSig_high_n_jet; */

   /* Bool_t passCRWenLowMetSig_low_dphi; */
   /* Bool_t passCRWenLowMetSig_high_dphi; */
   /* Bool_t passCRWenLowMetSig_high_n_jet; */

   Bool_t pass_n_jet; // bool variables to make cuts
   Bool_t pass_lead_jet_pt;
   Bool_t pass_sublead_jet_pt;
   Bool_t pass_jj_dphi;
   Bool_t pass_opp_emisph;
   Bool_t pass_jj_deta;
   Bool_t pass_jj_mass;

   Bool_t pass_met_tst_et;
   Bool_t pass_met_tst_j1_dphi;
   Bool_t pass_met_tst_j2_dphi;

   Bool_t pass_met_tst_nolep_et;
   Bool_t pass_met_tst_nolep_j1_dphi;
   Bool_t pass_met_tst_nolep_j2_dphi;

   Bool_t pass_met_cst_jet;


   Bool_t passMetTrig; // bool variables to manage triggers
   Bool_t passLepTrig;
   Bool_t passHLT_xe70_mht;
   Bool_t passHLT_xe90_mht_L1XE50;
   Bool_t passHLT_xe100_mht_L1XE50;
   Bool_t passHLT_xe110_mht_L1XE50;
   Bool_t passHLT_noalg_J400;

   TLorentzVector lead_el_vect;
   TLorentzVector sublead_el_vect;
   TLorentzVector el_vect_tot;
   Double_t ee_mass;

   TLorentzVector lead_mu_vect;
   TLorentzVector sublead_mu_vect;
   TLorentzVector mu_vect_tot;
   Double_t mm_mass;

   // maps for cutflows
   std::map<TString, Float_t> SR_cut_flow;
   std::map<TString, Float_t> CRWe_cut_flow;
   std::map<TString, Float_t> CRWm_cut_flow;   
   std::map<TString, Float_t> CRZee_cut_flow;
   std::map<TString, Float_t> CRZmm_cut_flow;
   
   std::map<TString, Float_t> CRWeLowMetSig_cut_flow;

   // vectors for cutflows keys
   vector<TString> SR_cf_key;
   vector<TString> CRWe_cf_key;
   vector<TString> CRWm_cf_key;
   vector<TString> CRZee_cf_key;
   vector<TString> CRZmm_cf_key;

   vector<TString> CRWeLowMetSig_cf_key;
   
   // ==== declaration of ptr to files
   TFile* outfile;

   TString outfile_name;
   
   // ==== declaration of prt to histograms
   
   // leading electron pt
   TH1F* SR_lead_el_pt;
   TH1F* CRWe_lead_el_pt;
   TH1F* CRWm_lead_el_pt;
   TH1F* CRZee_lead_el_pt;
   TH1F* CRZmm_lead_el_pt;

   // leading muon pt
   TH1F* SR_lead_mu_pt;
   TH1F* CRWe_lead_mu_pt;
   TH1F* CRWm_lead_mu_pt;
   TH1F* CRZee_lead_mu_pt;
   TH1F* CRZmm_lead_mu_pt;

   // subleading electron pt
   TH1F* SR_sublead_el_pt;
   TH1F* CRWe_sublead_el_pt;
   TH1F* CRWm_sublead_el_pt;
   TH1F* CRZee_sublead_el_pt;
   TH1F* CRZmm_sublead_el_pt;

   // subleading muon pt
   TH1F* SR_sublead_mu_pt;
   TH1F* CRWe_sublead_mu_pt;
   TH1F* CRWm_sublead_mu_pt;
   TH1F* CRZee_sublead_mu_pt;
   TH1F* CRZmm_sublead_mu_pt;

   // leading jet pt
   TH1F* SR_lead_jet_pt;
   TH1F* CRWe_lead_jet_pt;
   TH1F* CRWm_lead_jet_pt;
   TH1F* CRZee_lead_jet_pt;
   TH1F* CRZmm_lead_jet_pt;

   // subleading jet pt
   TH1F* SR_sublead_jet_pt;
   TH1F* CRWe_sublead_jet_pt;
   TH1F* CRWm_sublead_jet_pt;
   TH1F* CRZee_sublead_jet_pt;
   TH1F* CRZmm_sublead_jet_pt;

   // jj_deta
   TH1F* SR_jj_deta;
   TH1F* CRWe_jj_deta;
   TH1F* CRWm_jj_deta;
   TH1F* CRZmm_jj_deta;
   TH1F* CRZee_jj_deta;

   // jj_dphi
   TH1F* SR_jj_dphi;
   TH1F* CRWe_jj_dphi;
   TH1F* CRWm_jj_dphi;
   TH1F* CRZee_jj_dphi;
   TH1F* CRZmm_jj_dphi;

   // jj_mass
   TH1F* SR_jj_mass;
   TH1F* CRWe_jj_mass;
   TH1F* CRWm_jj_mass;
   TH1F* CRZee_jj_mass;
   TH1F* CRZmm_jj_mass;
   
   // additional histograms maps for single bins of 3-bin strategy
   std::map<TString, TH1F*> SR_jj_mass_3bs;

   std::map<TString, TH1F*> CRZee_jj_mass_3bs;
   std::map<TString, TH1F*> CRZmm_jj_mass_3bs;
   
   std::map<TString, TH1F*> CRWep_jj_mass_3bs;
   std::map<TString, TH1F*> CRWen_jj_mass_3bs;
   
   std::map<TString, TH1F*> CRWmp_jj_mass_3bs;
   std::map<TString, TH1F*> CRWmn_jj_mass_3bs;
   
   std::map<TString, TH1F*> CRWepLowMetSig_jj_mass_3bs;
   std::map<TString, TH1F*> CRWenLowMetSig_jj_mass_3bs;
      
   // additional histograms maps for single bins of 7-bin strategy
   std::map<TString, TH1F*> SR_jj_mass_7bs;

   std::map<TString, TH1F*> CRZee_jj_mass_7bs;
   std::map<TString, TH1F*> CRZmm_jj_mass_7bs;
   
   std::map<TString, TH1F*> CRWep_jj_mass_7bs;
   std::map<TString, TH1F*> CRWen_jj_mass_7bs;
   
   std::map<TString, TH1F*> CRWmp_jj_mass_7bs;
   std::map<TString, TH1F*> CRWmn_jj_mass_7bs;
   
   std::map<TString, TH1F*> CRWepLowMetSig_jj_mass_7bs;
   std::map<TString, TH1F*> CRWenLowMetSig_jj_mass_7bs;


   /* // additional histograms to produce 3-bin-strategy prefit plot */
   /* TH1F* CRWep_jj_mass; */
   /* TH1F* CRWen_jj_mass; */

   /* TH1F* CRWmp_jj_mass; */
   /* TH1F* CRWmn_jj_mass; */
   
   /* TH1F* CRWepLowMetSig_jj_mass; */
   /* TH1F* CRWenLowMetSig_jj_mass;    */
   
   /* // additional histograms to produce 7-bin-strategy prefit plot */
   /* TH1F* SR_low_dphi_jj_mass; */
   /* TH1F* SR_high_dphi_jj_mass; */
   /* TH1F* SR_high_n_jet_jj_mass; */

   /* TH1F* CRZee_low_dphi_jj_mass; */
   /* TH1F* CRZee_high_dphi_jj_mass; */
   /* TH1F* CRZee_high_n_jet_jj_mass; */

   /* TH1F* CRZmm_low_dphi_jj_mass; */
   /* TH1F* CRZmm_high_dphi_jj_mass; */
   /* TH1F* CRZmm_high_n_jet_jj_mass; */

   /* TH1F* CRWep_low_dphi_jj_mass; */
   /* TH1F* CRWep_high_dphi_jj_mass; */
   /* TH1F* CRWep_high_n_jet_jj_mass; */
   
   /* TH1F* CRWen_low_dphi_jj_mass; */
   /* TH1F* CRWen_high_dphi_jj_mass; */
   /* TH1F* CRWen_high_n_jet_jj_mass; */

   /* TH1F* CRWmp_low_dphi_jj_mass; */
   /* TH1F* CRWmp_high_dphi_jj_mass; */
   /* TH1F* CRWmp_high_n_jet_jj_mass; */
   
   /* TH1F* CRWmn_low_dphi_jj_mass; */
   /* TH1F* CRWmn_high_dphi_jj_mass; */
   /* TH1F* CRWmn_high_n_jet_jj_mass; */
   
   /* TH1F* CRWepLowMetSig_low_dphi_jj_mass; */
   /* TH1F* CRWepLowMetSig_high_dphi_jj_mass; */
   /* TH1F* CRWepLowMetSig_high_n_jet_jj_mass; */
   
   /* TH1F* CRWenLowMetSig_low_dphi_jj_mass; */
   /* TH1F* CRWenLowMetSig_high_dphi_jj_mass; */
   /* TH1F* CRWenLowMetSig_high_n_jet_jj_mass; */
   
   
   // met_tst_et
   TH1F* SR_met_tst_et;
   TH1F* CRWe_met_tst_et;
   TH1F* CRWm_met_tst_et;
   TH1F* CRZmm_met_tst_et;
   TH1F* CRZee_met_tst_et;
   
   // met_tst_nolep_et
   TH1F* SR_met_tst_nolep_et;
   TH1F* CRWe_met_tst_nolep_et;
   TH1F* CRWm_met_tst_nolep_et;
   TH1F* CRZmm_met_tst_nolep_et;
   TH1F* CRZee_met_tst_nolep_et;

   // leading jet_fjvt
   TH1F* SR_lead_jet_fjvt;
   TH1F* CRWe_lead_jet_fjvt;
   TH1F* CRWm_lead_jet_fjvt;
   TH1F* CRZmm_lead_jet_fjvt;
   TH1F* CRZee_lead_jet_fjvt;

   // subleading jet_fjvt
   TH1F* SR_sublead_jet_fjvt;
   TH1F* CRWe_sublead_jet_fjvt;
   TH1F* CRWm_sublead_jet_fjvt;
   TH1F* CRZmm_sublead_jet_fjvt;
   TH1F* CRZee_sublead_jet_fjvt;

   Bool_t print_n_evts;
   Bool_t print_charge_split_regions_n_evts;
   Bool_t print_3bin_strategy_regions_n_evts;
   Bool_t print_7bin_strategy_regions_n_evts;
   Bool_t print_cut_flows;
   
   
   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Float_t> w = {fReader, "w"};
   TTreeReaderValue<Float_t> xeSFTrigWeight = {fReader, "xeSFTrigWeight"};
   TTreeReaderValue<Float_t> xeSFTrigWeight__1up = {fReader, "xeSFTrigWeight__1up"};
   TTreeReaderValue<Float_t> xeSFTrigWeight__1down = {fReader, "xeSFTrigWeight__1down"};
   TTreeReaderValue<Float_t> eleANTISF = {fReader, "eleANTISF"};
   TTreeReaderValue<Int_t> runNumber = {fReader, "runNumber"};
   TTreeReaderValue<Int_t> randomRunNumber = {fReader, "randomRunNumber"};
   TTreeReaderValue<ULong64_t> eventNumber = {fReader, "eventNumber"};
   TTreeReaderValue<Int_t> trigger_met = {fReader, "trigger_met"};
   TTreeReaderValue<Int_t> trigger_met_encodedv2 = {fReader, "trigger_met_encodedv2"};
   TTreeReaderValue<Int_t> l1_met_trig_encoded = {fReader, "l1_met_trig_encoded"};
   TTreeReaderValue<Int_t> trigger_met_encoded = {fReader, "trigger_met_encoded"};
   TTreeReaderValue<Bool_t> passVjetsFilter = {fReader, "passVjetsFilter"};
   TTreeReaderValue<Bool_t> passVjetsPTV = {fReader, "passVjetsPTV"};
   TTreeReaderValue<Int_t> trigger_lep = {fReader, "trigger_lep"};
   TTreeReaderValue<Int_t> passJetCleanTight = {fReader, "passJetCleanTight"};
   TTreeReaderValue<Float_t> averageIntPerXing = {fReader, "averageIntPerXing"};
   TTreeReaderValue<Int_t> n_vx = {fReader, "n_vx"};
   TTreeReaderValue<Int_t> n_jet = {fReader, "n_jet"};
   TTreeReaderValue<Int_t> n_el = {fReader, "n_el"};
   TTreeReaderValue<Int_t> n_mu = {fReader, "n_mu"};
   TTreeReaderValue<Int_t> n_ph = {fReader, "n_ph"};
   TTreeReaderValue<Int_t> n_tau = {fReader, "n_tau"};
   TTreeReaderValue<Double_t> jj_mass = {fReader, "jj_mass"};
   TTreeReaderValue<Double_t> jj_deta = {fReader, "jj_deta"};
   TTreeReaderValue<Double_t> jj_dphi = {fReader, "jj_dphi"};
   TTreeReaderValue<Double_t> met_tst_j1_dphi = {fReader, "met_tst_j1_dphi"};
   TTreeReaderValue<Double_t> met_tst_j2_dphi = {fReader, "met_tst_j2_dphi"};
   TTreeReaderValue<Double_t> met_tst_nolep_j1_dphi = {fReader, "met_tst_nolep_j1_dphi"};
   TTreeReaderValue<Double_t> met_tst_nolep_j2_dphi = {fReader, "met_tst_nolep_j2_dphi"};
   TTreeReaderValue<Float_t> met_tst_et = {fReader, "met_tst_et"};
   TTreeReaderValue<Float_t> met_tst_nolep_et = {fReader, "met_tst_nolep_et"};
   TTreeReaderValue<Float_t> met_tst_phi = {fReader, "met_tst_phi"};
   TTreeReaderValue<Float_t> met_tst_nolep_phi = {fReader, "met_tst_nolep_phi"};
   TTreeReaderValue<Double_t> met_cst_jet = {fReader, "met_cst_jet"};
   TTreeReaderValue<Float_t> met_soft_tst_et = {fReader, "met_soft_tst_et"};
   TTreeReaderArray<float> mu_charge = {fReader, "mu_charge"};
   TTreeReaderArray<float> mu_pt = {fReader, "mu_pt"};
   TTreeReaderArray<float> el_charge = {fReader, "el_charge"};
   TTreeReaderArray<float> el_pt = {fReader, "el_pt"};
   TTreeReaderArray<float> jet_pt = {fReader, "jet_pt"};
   TTreeReaderArray<float> jet_timing = {fReader, "jet_timing"};
   TTreeReaderArray<float> mu_phi = {fReader, "mu_phi"};
   TTreeReaderArray<float> el_phi = {fReader, "el_phi"};
   TTreeReaderArray<float> mu_eta = {fReader, "mu_eta"};
   TTreeReaderArray<float> el_eta = {fReader, "el_eta"};
   TTreeReaderArray<float> jet_phi = {fReader, "jet_phi"};
   TTreeReaderArray<float> jet_eta = {fReader, "jet_eta"};
   TTreeReaderArray<float> jet_m = {fReader, "jet_m"};
   TTreeReaderArray<float> jet_jvt = {fReader, "jet_jvt"};
   TTreeReaderValue<Float_t> met_significance = {fReader, "met_significance"};
   TTreeReaderValue<Float_t> max_mj_over_mjj = {fReader, "max_mj_over_mjj"};
   TTreeReaderValue<Float_t> maxCentrality = {fReader, "maxCentrality"};
   TTreeReaderValue<Int_t> n_baseel = {fReader, "n_baseel"};
   TTreeReaderValue<Int_t> n_basemu = {fReader, "n_basemu"};
   TTreeReaderValue<Int_t> n_bjet = {fReader, "n_bjet"};
   TTreeReaderArray<float> j3_centrality = {fReader, "j3_centrality"};
   TTreeReaderArray<float> j3_min_mj_over_mjj = {fReader, "j3_min_mj_over_mjj"};
   TTreeReaderArray<float> j3_dRj1 = {fReader, "j3_dRj1"};
   TTreeReaderArray<float> j3_dRj2 = {fReader, "j3_dRj2"};
   TTreeReaderArray<float> j3_minDR = {fReader, "j3_minDR"};
   TTreeReaderArray<float> j3_mjclosest = {fReader, "j3_mjclosest"};
   TTreeReaderArray<float> j3_min_mj = {fReader, "j3_min_mj"};
   TTreeReaderValue<Float_t> mj34 = {fReader, "mj34"};
   TTreeReaderValue<Float_t> max_j_eta = {fReader, "max_j_eta"};
   TTreeReaderArray<float> jet_fjvt = {fReader, "jet_fjvt"};
   TTreeReaderArray<float> basemu_pt = {fReader, "basemu_pt"};
   TTreeReaderArray<float> basemu_eta = {fReader, "basemu_eta"};
   TTreeReaderArray<float> basemu_phi = {fReader, "basemu_phi"};
   TTreeReaderArray<int> basemu_charge = {fReader, "basemu_charge"};
   TTreeReaderArray<float> basemu_ptvarcone30 = {fReader, "basemu_ptvarcone30"};
   TTreeReaderArray<float> baseel_pt = {fReader, "baseel_pt"};
   TTreeReaderArray<float> baseel_eta = {fReader, "baseel_eta"};
   TTreeReaderArray<float> baseel_phi = {fReader, "baseel_phi"};
   TTreeReaderArray<int> baseel_charge = {fReader, "baseel_charge"};
   TTreeReaderArray<float> baseel_ptvarcone20 = {fReader, "baseel_ptvarcone20"};
   TTreeReaderArray<float> basemu_ptvarcone20 = {fReader, "basemu_ptvarcone20"};
   TTreeReaderArray<float> basemu_z0 = {fReader, "basemu_z0"};
   TTreeReaderArray<float> basemu_d0sig = {fReader, "basemu_d0sig"};
   TTreeReaderArray<float> basemu_topoetcone20 = {fReader, "basemu_topoetcone20"};
   TTreeReaderArray<float> basemu_topoetcone30 = {fReader, "basemu_topoetcone30"};
   TTreeReaderArray<int> basemu_type = {fReader, "basemu_type"};
   TTreeReaderArray<float> baseel_z0 = {fReader, "baseel_z0"};
   TTreeReaderArray<float> baseel_d0sig = {fReader, "baseel_d0sig"};
   TTreeReaderArray<float> baseel_topoetcone20 = {fReader, "baseel_topoetcone20"};
   TTreeReaderArray<float> ph_pt = {fReader, "ph_pt"};
   TTreeReaderArray<float> ph_phi = {fReader, "ph_phi"};
   TTreeReaderArray<float> ph_eta = {fReader, "ph_eta"};
   TTreeReaderArray<float> tau_pt = {fReader, "tau_pt"};
   TTreeReaderArray<float> tau_phi = {fReader, "tau_phi"};
   TTreeReaderArray<float> tau_eta = {fReader, "tau_eta"};
   TTreeReaderValue<Float_t> met_soft_tst_phi = {fReader, "met_soft_tst_phi"};
   TTreeReaderValue<Float_t> met_soft_tst_sumet = {fReader, "met_soft_tst_sumet"};
   TTreeReaderValue<Float_t> met_tenacious_tst_et = {fReader, "met_tenacious_tst_et"};
   TTreeReaderValue<Float_t> met_tenacious_tst_phi = {fReader, "met_tenacious_tst_phi"};
   TTreeReaderValue<Float_t> met_tenacious_tst_nolep_et = {fReader, "met_tenacious_tst_nolep_et"};
   TTreeReaderValue<Float_t> met_tenacious_tst_nolep_phi = {fReader, "met_tenacious_tst_nolep_phi"};
   TTreeReaderValue<Float_t> met_tenacious_tst_j1_dphi = {fReader, "met_tenacious_tst_j1_dphi"};
   TTreeReaderValue<Float_t> met_tenacious_tst_j2_dphi = {fReader, "met_tenacious_tst_j2_dphi"};
   TTreeReaderValue<Float_t> met_tenacious_tst_nolep_j1_dphi = {fReader, "met_tenacious_tst_nolep_j1_dphi"};
   TTreeReaderValue<Float_t> met_tenacious_tst_nolep_j2_dphi = {fReader, "met_tenacious_tst_nolep_j2_dphi"};
   TTreeReaderValue<Float_t> met_tight_tst_et = {fReader, "met_tight_tst_et"};
   TTreeReaderValue<Float_t> met_tight_tst_phi = {fReader, "met_tight_tst_phi"};
   TTreeReaderValue<Float_t> met_tight_tst_nolep_et = {fReader, "met_tight_tst_nolep_et"};
   TTreeReaderValue<Float_t> met_tight_tst_nolep_phi = {fReader, "met_tight_tst_nolep_phi"};
   TTreeReaderValue<Double_t> metsig_tst = {fReader, "metsig_tst"};


   ntupleToHistoSelector(TTree * /*tree*/ =0) { }
   virtual ~ntupleToHistoSelector() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(ntupleToHistoSelector,0);

};

#endif

#ifdef ntupleToHistoSelector_cxx
void ntupleToHistoSelector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t ntupleToHistoSelector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef ntupleToHistoSelector_cxx
