# just copy the input from service directory into the directory on which acts the code written by Loan.

# .root with all the systematics. Can be used for any fit
cp /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/inputHistFitter/inputFit_3bs.root /afs/cern.ch/user/g/gpadovan/work/StatsTools/makeHistFitterConfig/input/

# .root with jet+met systematics. Can be used for sys studies on fit
cp /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/inputHistFitter/inputFit_3bs_jet_metSys.root /afs/cern.ch/user/g/gpadovan/work/StatsTools/makeHistFitterConfig/input/

# .root with lep+met systematics. Can be used for sys studies on fit
cp /afs/cern.ch/user/g/gpadovan/work/vbfinv/outputFilesSingleSelector/inputHistFitter/inputFit_3bs_lep_metSys.root /afs/cern.ch/user/g/gpadovan/work/StatsTools/makeHistFitterConfig/input/
