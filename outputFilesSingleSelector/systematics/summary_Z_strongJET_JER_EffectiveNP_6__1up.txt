====  summary of selection results  ====
      sample: Z_strongJET_JER_EffectiveNP_6__1up
========================================

==== summary of evts. in all regions ====
SR:	1178
CRWe:	4.54567
CRWm:	36.8238
CRZee:	86.7289
CRZmm:	107.982
CRWeLowMetSig:	43.2847

==== events in charge-split regions ====
CRWep:	0.865219
CRWen:	3.68045
CRWmp:	18.3579
CRWmn:	18.4659
CRWepLowMetSig:	23.4787
CRWenLowMetSig:	19.806

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	537.161
SR_n_evts_bin2:	376.305
SR_n_evts_bin3:	264.538
== total:	 1178

CRZee_n_evts_bin1:	40.0729
CRZee_n_evts_bin2:	27.968
CRZee_n_evts_bin3:	18.6881
== total:	 86.729

CRZmm_n_evts_bin1:	53.9575
CRZmm_n_evts_bin2:	27.4276
CRZmm_n_evts_bin3:	26.597
== total:	 107.982

CRWep_n_evts_bin1:	0.340158
CRWep_n_evts_bin2:	0.209793
CRWep_n_evts_bin3:	0.315268
== total:	 0.865219

CRWen_n_evts_bin1:	1.44323
CRWen_n_evts_bin2:	1.64458
CRWen_n_evts_bin3:	0.592641
== total:	 3.68045

CRWmp_n_evts_bin1:	9.01926
CRWmp_n_evts_bin2:	3.82894
CRWmp_n_evts_bin3:	5.50968
== total:	 18.3579

CRWmn_n_evts_bin1:	10.4723
CRWmn_n_evts_bin2:	4.35306
CRWmn_n_evts_bin3:	3.64057
== total:	 18.4659

CRWepLowMetSig_n_evts_bin1:	10.0747
CRWepLowMetSig_n_evts_bin2:	5.99832
CRWepLowMetSig_n_evts_bin3:	7.40561
== total:	 23.4787

CRWenLoeMetSig_n_evts_bin1:	9.41709
CRWenLoeMetSig_n_evts_bin2:	7.78324
CRWenLoeMetSig_n_evts_bin3:	2.60568
== total:	 19.806

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	366.403
SR_n_evts_bin2:	202.674
SR_n_evts_bin3:	130.647
SR_n_evts_bin4:	170.758
SR_n_evts_bin5:	173.631
SR_n_evts_bin6:	133.891
SR_n_evts_bin7:	0
== total:	 1178

CRZee_n_evts_bin1:	26.2262
CRZee_n_evts_bin2:	13.8416
CRZee_n_evts_bin3:	10.255
CRZee_n_evts_bin4:	13.8467
CRZee_n_evts_bin5:	14.1264
CRZee_n_evts_bin6:	8.43303
CRZee_n_evts_bin7:	0
== total:	 86.729

CRZmm_n_evts_bin1:	38.8769
CRZmm_n_evts_bin2:	14.6223
CRZmm_n_evts_bin3:	14.3389
CRZmm_n_evts_bin4:	15.0806
CRZmm_n_evts_bin5:	12.8053
CRZmm_n_evts_bin6:	12.258
CRZmm_n_evts_bin7:	0
== total:	 107.982

CRWep_n_evts_bin1:	0.116267
CRWep_n_evts_bin2:	0.285509
CRWep_n_evts_bin3:	-0.169528
CRWep_n_evts_bin4:	0.22389
CRWep_n_evts_bin5:	-0.0757157
CRWep_n_evts_bin6:	0.484796
CRWep_n_evts_bin7:	0
== total:	 0.865219

CRWen_n_evts_bin1:	0.913443
CRWen_n_evts_bin2:	0.561609
CRWen_n_evts_bin3:	0.252532
CRWen_n_evts_bin4:	0.529789
CRWen_n_evts_bin5:	1.08297
CRWen_n_evts_bin6:	0.340109
CRWen_n_evts_bin7:	0
== total:	 3.68045

CRWmp_n_evts_bin1:	6.08243
CRWmp_n_evts_bin2:	0.742119
CRWmp_n_evts_bin3:	1.1657
CRWmp_n_evts_bin4:	2.93683
CRWmp_n_evts_bin5:	3.08682
CRWmp_n_evts_bin6:	4.34398
CRWmp_n_evts_bin7:	0
== total:	 18.3579

CRWmn_n_evts_bin1:	5.43887
CRWmn_n_evts_bin2:	3.4784
CRWmn_n_evts_bin3:	2.34401
CRWmn_n_evts_bin4:	5.0334
CRWmn_n_evts_bin5:	0.874662
CRWmn_n_evts_bin6:	1.29657
CRWmn_n_evts_bin7:	0
== total:	 18.4659

CRWepLowMetSig_n_evts_bin1:	4.45013
CRWepLowMetSig_n_evts_bin2:	4.1874
CRWepLowMetSig_n_evts_bin3:	3.36331
CRWepLowMetSig_n_evts_bin4:	5.62459
CRWepLowMetSig_n_evts_bin5:	1.81092
CRWepLowMetSig_n_evts_bin6:	4.0423
CRWepLowMetSig_n_evts_bin7:	0
== total:	 23.4787

CRWenLoeMetSig_n_evts_bin1:	5.51382
CRWenLoeMetSig_n_evts_bin2:	5.30133
CRWenLoeMetSig_n_evts_bin3:	-0.489864
CRWenLoeMetSig_n_evts_bin4:	3.90327
CRWenLoeMetSig_n_evts_bin5:	2.48191
CRWenLoeMetSig_n_evts_bin6:	3.09555
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 19.806

==== SR cutflow ====
passMetTrig:		37191.6
0_el_0_mu:		31225
pass_n_jet:		7374.51
pass_lead_jet_pt:		7243.41
pass_sublead_jet_pt:		6715.8
pass_jj_dphi:		5796.07
pass_opp_emisph:		5782.53
pass_jj_deta:		2647.09
pass_jj_mass:		2361.06
pass_met_tst_et:		1212.57
pass_met_tst_j1_dphi:		1212.12
pass_met_tst_j2_dphi:		1212.07
pass_met_cst_jet:		1178

==== CRWe cutflow ====
passLepTrig:		14744.8
1_el_0_mu:		5397.07
el_pt:		5128.21
pass_n_jet:		1700.3
pass_lead_jet_pt:		1290.15
pass_sublead_jet_pt:		637.818
pass_jj_dphi:		420.825
pass_opp_emisph:		399.794
pass_jj_deta:		107.995
pass_jj_mass:		98.6274
pass_met_tst_nolep_et:		48.4915
pass_met_tst_nolep_j1_dphi:		48.4915
pass_met_tst_nolep_j2_dphi:		48.4915
pass_met_cst_jet:		47.8303
met_significance:		4.54567

==== CRWm cutflow ====
passLepTrig:		14744.8
0_el_1_mu:		1476.93
mu_pt:		1414.59
pass_n_jet:		302.485
pass_lead_jet_pt:		275.062
pass_sublead_jet_pt:		237.672
pass_jj_dphi:		190.148
pass_opp_emisph:		187.553
pass_jj_deta:		79.7869
pass_jj_mass:		71.8061
pass_met_tst_nolep_et:		38.0097
pass_met_tst_nolep_j1_dphi:		38.0097
pass_met_tst_nolep_j2_dphi:		38.0097
pass_met_cst_jet:		36.8238

==== CRZee cutflow ====
passLepTrig:		14744.8
2_el_0_mu:		2638.99
el_pt:		2629.86
opp_charge:		2547.62
ee_mass:		2128.36
pass_n_jet:		550.739
pass_lead_jet_pt:		523.774
pass_sublead_jet_pt:		471.766
pass_jj_dphi:		407.66
pass_opp_emisph:		406.616
pass_jj_deta:		168.068
pass_jj_mass:		149.458
pass_met_tst_nolep_et:		87.7554
pass_met_tst_nolep_j1_dphi:		87.7339
pass_met_tst_nolep_j2_dphi:		87.7339
pass_met_cst_jet:		86.7289

==== CRZmm cutflow ====
passLepTrig:		14744.8
0_el_2_mu:		3437.24
mu_pt:		3427.06
opp_charge:		3425.66
mm_mass:		2826.14
pass_n_jet:		727.116
pass_lead_jet_pt:		717.86
pass_sublead_jet_pt:		642.627
pass_jj_dphi:		560.055
pass_opp_emisph:		560.355
pass_jj_deta:		243.585
pass_jj_mass:		214.216
pass_met_tst_nolep_et:		109.683
pass_met_tst_nolep_j1_dphi:		109.683
pass_met_tst_nolep_j2_dphi:		109.683
pass_met_cst_jet:		107.982

==== CRWeLowMetSig cutflow ====
passLepTrig:		14744.8
1_el_0_mu:		5397.07
el_pt:		5128.21
pass_n_jet:		1700.3
pass_lead_jet_pt:		1290.15
pass_sublead_jet_pt:		637.818
pass_jj_dphi:		420.825
pass_opp_emisph:		399.794
pass_jj_deta:		107.995
pass_jj_mass:		98.6274
pass_met_tst_nolep_et:		48.4915
pass_met_tst_nolep_j1_dphi:		48.4915
pass_met_tst_nolep_j2_dphi:		48.4915
pass_met_cst_jet:		47.8303
low_met_significance:		43.2847

