====  summary of selection results  ====
      sample: W_EWKMUON_SCALE__1up
========================================

==== summary of evts. in all regions ====
SR:	175.273
CRWe:	160.759
CRWm:	211.98
CRZee:	0
CRZmm:	0
CRWeLowMetSig:	105.177

==== events in charge-split regions ====
CRWep:	107.263
CRWen:	53.4959
CRWmp:	134.472
CRWmn:	77.5073
CRWepLowMetSig:	59.3331
CRWenLowMetSig:	45.8437

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	33.7861
SR_n_evts_bin2:	52.0756
SR_n_evts_bin3:	89.4118
== total:	 175.273

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
== total:	 0

CRWep_n_evts_bin1:	22.9861
CRWep_n_evts_bin2:	31.3735
CRWep_n_evts_bin3:	52.9036
== total:	 107.263

CRWen_n_evts_bin1:	11.045
CRWen_n_evts_bin2:	19.5054
CRWen_n_evts_bin3:	22.9455
== total:	 53.4959

CRWmp_n_evts_bin1:	29.1795
CRWmp_n_evts_bin2:	39.1176
CRWmp_n_evts_bin3:	66.1753
== total:	 134.472

CRWmn_n_evts_bin1:	15.5815
CRWmn_n_evts_bin2:	27.5385
CRWmn_n_evts_bin3:	34.3873
== total:	 77.5073

CRWepLowMetSig_n_evts_bin1:	11.1294
CRWepLowMetSig_n_evts_bin2:	18.9479
CRWepLowMetSig_n_evts_bin3:	29.2558
== total:	 59.3331

CRWenLoeMetSig_n_evts_bin1:	13.1485
CRWenLoeMetSig_n_evts_bin2:	14.9139
CRWenLoeMetSig_n_evts_bin3:	17.7814
== total:	 45.8437

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	19.5851
SR_n_evts_bin2:	25.1351
SR_n_evts_bin3:	38.5474
SR_n_evts_bin4:	14.2011
SR_n_evts_bin5:	26.9404
SR_n_evts_bin6:	50.8643
SR_n_evts_bin7:	0
== total:	 175.273

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	0
CRZee_n_evts_bin6:	0
CRZee_n_evts_bin7:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0

CRWep_n_evts_bin1:	16.0137
CRWep_n_evts_bin2:	17.0333
CRWep_n_evts_bin3:	23.4086
CRWep_n_evts_bin4:	6.9724
CRWep_n_evts_bin5:	14.3403
CRWep_n_evts_bin6:	29.495
CRWep_n_evts_bin7:	0
== total:	 107.263

CRWen_n_evts_bin1:	7.99361
CRWen_n_evts_bin2:	9.53368
CRWen_n_evts_bin3:	10.8989
CRWen_n_evts_bin4:	3.05143
CRWen_n_evts_bin5:	9.97168
CRWen_n_evts_bin6:	12.0466
CRWen_n_evts_bin7:	0
== total:	 53.4959

CRWmp_n_evts_bin1:	18.6502
CRWmp_n_evts_bin2:	20.2488
CRWmp_n_evts_bin3:	27.1174
CRWmp_n_evts_bin4:	10.5293
CRWmp_n_evts_bin5:	18.8688
CRWmp_n_evts_bin6:	39.0579
CRWmp_n_evts_bin7:	0
== total:	 134.472

CRWmn_n_evts_bin1:	8.67848
CRWmn_n_evts_bin2:	14.8019
CRWmn_n_evts_bin3:	15.6486
CRWmn_n_evts_bin4:	6.90304
CRWmn_n_evts_bin5:	12.7365
CRWmn_n_evts_bin6:	18.7386
CRWmn_n_evts_bin7:	0
== total:	 77.5073

CRWepLowMetSig_n_evts_bin1:	5.7522
CRWepLowMetSig_n_evts_bin2:	8.33523
CRWepLowMetSig_n_evts_bin3:	13.5199
CRWepLowMetSig_n_evts_bin4:	5.37716
CRWepLowMetSig_n_evts_bin5:	10.6127
CRWepLowMetSig_n_evts_bin6:	15.7359
CRWepLowMetSig_n_evts_bin7:	0
== total:	 59.3331

CRWenLoeMetSig_n_evts_bin1:	8.22898
CRWenLoeMetSig_n_evts_bin2:	5.94636
CRWenLoeMetSig_n_evts_bin3:	8.35865
CRWenLoeMetSig_n_evts_bin4:	4.91951
CRWenLoeMetSig_n_evts_bin5:	8.96754
CRWenLoeMetSig_n_evts_bin6:	9.42271
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 45.8437

==== SR cutflow ====
passMetTrig:		4790.81
0_el_0_mu:		2090.82
pass_n_jet:		894.729
pass_lead_jet_pt:		887.354
pass_sublead_jet_pt:		841.893
pass_jj_dphi:		658.858
pass_opp_emisph:		655.129
pass_jj_deta:		347.78
pass_jj_mass:		341.268
pass_met_tst_et:		177.447
pass_met_tst_j1_dphi:		177.447
pass_met_tst_j2_dphi:		177.447
pass_met_cst_jet:		175.273

==== CRWe cutflow ====
passLepTrig:		3584.97
1_el_0_mu:		1747.75
el_pt:		1701.45
pass_n_jet:		964.873
pass_lead_jet_pt:		962.234
pass_sublead_jet_pt:		953.995
pass_jj_dphi:		791.047
pass_opp_emisph:		791.047
pass_jj_deta:		390.34
pass_jj_mass:		380.445
pass_met_tst_nolep_et:		267.394
pass_met_tst_nolep_j1_dphi:		267.394
pass_met_tst_nolep_j2_dphi:		267.394
pass_met_cst_jet:		265.936
met_significance:		160.759

==== CRWm cutflow ====
passLepTrig:		3584.97
0_el_1_mu:		1467.22
mu_pt:		1428.14
pass_n_jet:		818.064
pass_lead_jet_pt:		816.741
pass_sublead_jet_pt:		812.126
pass_jj_dphi:		666.597
pass_opp_emisph:		666.503
pass_jj_deta:		321.173
pass_jj_mass:		311.598
pass_met_tst_nolep_et:		213.33
pass_met_tst_nolep_j1_dphi:		213.33
pass_met_tst_nolep_j2_dphi:		213.33
pass_met_cst_jet:		211.98

==== CRZee cutflow ====
passLepTrig:		3584.97
2_el_0_mu:		1.04501
el_pt:		1.04501
opp_charge:		0.658755
ee_mass:		0.0902424
pass_n_jet:		0.0902424
pass_lead_jet_pt:		0.0902424
pass_sublead_jet_pt:		0.0902424
pass_jj_dphi:		0.0902424
pass_opp_emisph:		0.0902424
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZmm cutflow ====
passLepTrig:		3584.97
0_el_2_mu:		0.527745
mu_pt:		0.527745
opp_charge:		0.29906
mm_mass:		0.0783653
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRWeLowMetSig cutflow ====
passLepTrig:		3584.97
1_el_0_mu:		1747.75
el_pt:		1701.45
pass_n_jet:		964.873
pass_lead_jet_pt:		962.234
pass_sublead_jet_pt:		953.995
pass_jj_dphi:		791.047
pass_opp_emisph:		791.047
pass_jj_deta:		390.34
pass_jj_mass:		380.445
pass_met_tst_nolep_et:		267.394
pass_met_tst_nolep_j1_dphi:		267.394
pass_met_tst_nolep_j2_dphi:		267.394
pass_met_cst_jet:		265.936
low_met_significance:		105.177

