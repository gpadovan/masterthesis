====  summary of selection results  ====
      sample: W_strongEG_RESOLUTION_ALL__1down
========================================

==== summary of evts. in all regions ====
SR:	927.646
CRWe:	545.214
CRWm:	887.785
CRZee:	0.134099
CRZmm:	0.182382
CRWeLowMetSig:	338.437

==== events in charge-split regions ====
CRWep:	353.57
CRWen:	191.645
CRWmp:	566.489
CRWmn:	321.296
CRWepLowMetSig:	170.946
CRWenLowMetSig:	167.491

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	403.113
SR_n_evts_bin2:	329.031
SR_n_evts_bin3:	195.503
== total:	 927.647

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	-0.00645132
CRZee_n_evts_bin3:	0.14055
== total:	 0.134099

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0.182382
CRZmm_n_evts_bin3:	0
== total:	 0.182382

CRWep_n_evts_bin1:	135.975
CRWep_n_evts_bin2:	129.717
CRWep_n_evts_bin3:	87.8773
== total:	 353.57

CRWen_n_evts_bin1:	91.264
CRWen_n_evts_bin2:	60.7871
CRWen_n_evts_bin3:	39.5935
== total:	 191.645

CRWmp_n_evts_bin1:	248.817
CRWmp_n_evts_bin2:	192.566
CRWmp_n_evts_bin3:	125.106
== total:	 566.489

CRWmn_n_evts_bin1:	161.613
CRWmn_n_evts_bin2:	98.0553
CRWmn_n_evts_bin3:	61.6285
== total:	 321.296

CRWepLowMetSig_n_evts_bin1:	73.3328
CRWepLowMetSig_n_evts_bin2:	61.6473
CRWepLowMetSig_n_evts_bin3:	35.9657
== total:	 170.946

CRWenLoeMetSig_n_evts_bin1:	72.0949
CRWenLoeMetSig_n_evts_bin2:	60.5944
CRWenLoeMetSig_n_evts_bin3:	34.802
== total:	 167.491

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	277.986
SR_n_evts_bin2:	190.624
SR_n_evts_bin3:	100.392
SR_n_evts_bin4:	125.127
SR_n_evts_bin5:	138.407
SR_n_evts_bin6:	95.1108
SR_n_evts_bin7:	0
== total:	 927.646

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0.0966545
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	-0.103106
CRZee_n_evts_bin6:	0.14055
CRZee_n_evts_bin7:	0
== total:	 0.134099

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0.095122
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0.08726
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0.182382

CRWep_n_evts_bin1:	91.1944
CRWep_n_evts_bin2:	68.8085
CRWep_n_evts_bin3:	55.6159
CRWep_n_evts_bin4:	44.7808
CRWep_n_evts_bin5:	60.9089
CRWep_n_evts_bin6:	32.2614
CRWep_n_evts_bin7:	0
== total:	 353.57

CRWen_n_evts_bin1:	63.4158
CRWen_n_evts_bin2:	39.7058
CRWen_n_evts_bin3:	21.2987
CRWen_n_evts_bin4:	27.8482
CRWen_n_evts_bin5:	21.0812
CRWen_n_evts_bin6:	18.2947
CRWen_n_evts_bin7:	0
== total:	 191.645

CRWmp_n_evts_bin1:	169.256
CRWmp_n_evts_bin2:	99.1806
CRWmp_n_evts_bin3:	57.9037
CRWmp_n_evts_bin4:	79.5617
CRWmp_n_evts_bin5:	93.3851
CRWmp_n_evts_bin6:	67.2021
CRWmp_n_evts_bin7:	0
== total:	 566.489

CRWmn_n_evts_bin1:	118.514
CRWmn_n_evts_bin2:	47.4341
CRWmn_n_evts_bin3:	32.9972
CRWmn_n_evts_bin4:	43.0986
CRWmn_n_evts_bin5:	50.6211
CRWmn_n_evts_bin6:	28.6313
CRWmn_n_evts_bin7:	0
== total:	 321.296

CRWepLowMetSig_n_evts_bin1:	49.3941
CRWepLowMetSig_n_evts_bin2:	30.8687
CRWepLowMetSig_n_evts_bin3:	16.6909
CRWepLowMetSig_n_evts_bin4:	23.9387
CRWepLowMetSig_n_evts_bin5:	30.7786
CRWepLowMetSig_n_evts_bin6:	19.2747
CRWepLowMetSig_n_evts_bin7:	0
== total:	 170.946

CRWenLoeMetSig_n_evts_bin1:	51.3394
CRWenLoeMetSig_n_evts_bin2:	23.6217
CRWenLoeMetSig_n_evts_bin3:	19.5175
CRWenLoeMetSig_n_evts_bin4:	20.7555
CRWenLoeMetSig_n_evts_bin5:	36.9726
CRWenLoeMetSig_n_evts_bin6:	15.2846
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 167.491

==== SR cutflow ====
passMetTrig:		82971.4
0_el_0_mu:		48406.2
pass_n_jet:		11584.7
pass_lead_jet_pt:		10715.7
pass_sublead_jet_pt:		7914.01
pass_jj_dphi:		6247.33
pass_opp_emisph:		6145.38
pass_jj_deta:		2836.37
pass_jj_mass:		2554.7
pass_met_tst_et:		956.824
pass_met_tst_j1_dphi:		956.669
pass_met_tst_j2_dphi:		956.581
pass_met_cst_jet:		927.646

==== CRWe cutflow ====
passLepTrig:		54362.7
1_el_0_mu:		22945.2
el_pt:		22298.4
pass_n_jet:		5792.93
pass_lead_jet_pt:		5612.71
pass_sublead_jet_pt:		5110.01
pass_jj_dphi:		4416.97
pass_opp_emisph:		4412.4
pass_jj_deta:		1926.23
pass_jj_mass:		1741.14
pass_met_tst_nolep_et:		900.386
pass_met_tst_nolep_j1_dphi:		900.386
pass_met_tst_nolep_j2_dphi:		900.386
pass_met_cst_jet:		883.652
met_significance:		545.214

==== CRWm cutflow ====
passLepTrig:		54362.7
0_el_1_mu:		20475.3
mu_pt:		19892.9
pass_n_jet:		5154.3
pass_lead_jet_pt:		5056.8
pass_sublead_jet_pt:		4593.67
pass_jj_dphi:		4017.45
pass_opp_emisph:		4011.95
pass_jj_deta:		1764.18
pass_jj_mass:		1589.65
pass_met_tst_nolep_et:		904.039
pass_met_tst_nolep_j1_dphi:		903.954
pass_met_tst_nolep_j2_dphi:		903.954
pass_met_cst_jet:		887.785

==== CRZee cutflow ====
passLepTrig:		54362.7
2_el_0_mu:		40.0916
el_pt:		37.3473
opp_charge:		18.6287
ee_mass:		5.63579
pass_n_jet:		0.909106
pass_lead_jet_pt:		0.909106
pass_sublead_jet_pt:		0.909106
pass_jj_dphi:		0.909106
pass_opp_emisph:		0.909106
pass_jj_deta:		0.180299
pass_jj_mass:		0.180299
pass_met_tst_nolep_et:		0.134099
pass_met_tst_nolep_j1_dphi:		0.134099
pass_met_tst_nolep_j2_dphi:		0.134099
pass_met_cst_jet:		0.134099

==== CRZmm cutflow ====
passLepTrig:		54362.7
0_el_2_mu:		33.4402
mu_pt:		32.7473
opp_charge:		19.1317
mm_mass:		4.01131
pass_n_jet:		1.25432
pass_lead_jet_pt:		1.25432
pass_sublead_jet_pt:		1.25432
pass_jj_dphi:		0.182382
pass_opp_emisph:		0.182382
pass_jj_deta:		0.182382
pass_jj_mass:		0.182382
pass_met_tst_nolep_et:		0.182382
pass_met_tst_nolep_j1_dphi:		0.182382
pass_met_tst_nolep_j2_dphi:		0.182382
pass_met_cst_jet:		0.182382

==== CRWeLowMetSig cutflow ====
passLepTrig:		54362.7
1_el_0_mu:		22945.2
el_pt:		22298.4
pass_n_jet:		5792.93
pass_lead_jet_pt:		5612.71
pass_sublead_jet_pt:		5110.01
pass_jj_dphi:		4416.97
pass_opp_emisph:		4412.4
pass_jj_deta:		1926.23
pass_jj_mass:		1741.14
pass_met_tst_nolep_et:		900.386
pass_met_tst_nolep_j1_dphi:		900.386
pass_met_tst_nolep_j2_dphi:		900.386
pass_met_cst_jet:		883.652
low_met_significance:		338.437

