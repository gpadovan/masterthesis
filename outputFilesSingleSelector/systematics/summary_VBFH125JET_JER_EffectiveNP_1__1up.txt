====  summary of selection results  ====
      sample: VBFH125JET_JER_EffectiveNP_1__1up
========================================

==== summary of evts. in all regions ====
SR:	913.263
CRWe:	0
CRWm:	0
CRZee:	0
CRZmm:	0
CRWeLowMetSig:	0

==== events in charge-split regions ====
CRWep:	0
CRWen:	0
CRWmp:	0
CRWmn:	0
CRWepLowMetSig:	0
CRWenLowMetSig:	0

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	232.001
SR_n_evts_bin2:	291.549
SR_n_evts_bin3:	389.711
== total:	 913.262

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
== total:	 0

CRWep_n_evts_bin1:	0
CRWep_n_evts_bin2:	0
CRWep_n_evts_bin3:	0
== total:	 0

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0
CRWen_n_evts_bin3:	0
== total:	 0

CRWmp_n_evts_bin1:	0
CRWmp_n_evts_bin2:	0
CRWmp_n_evts_bin3:	0
== total:	 0

CRWmn_n_evts_bin1:	0
CRWmn_n_evts_bin2:	0
CRWmn_n_evts_bin3:	0
== total:	 0

CRWepLowMetSig_n_evts_bin1:	0
CRWepLowMetSig_n_evts_bin2:	0
CRWepLowMetSig_n_evts_bin3:	0
== total:	 0

CRWenLoeMetSig_n_evts_bin1:	0
CRWenLoeMetSig_n_evts_bin2:	0
CRWenLoeMetSig_n_evts_bin3:	0
== total:	 0

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	176.003
SR_n_evts_bin2:	190.334
SR_n_evts_bin3:	247.514
SR_n_evts_bin4:	55.998
SR_n_evts_bin5:	101.216
SR_n_evts_bin6:	142.198
SR_n_evts_bin7:	0
== total:	 913.263

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	0
CRZee_n_evts_bin6:	0
CRZee_n_evts_bin7:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0

CRWep_n_evts_bin1:	0
CRWep_n_evts_bin2:	0
CRWep_n_evts_bin3:	0
CRWep_n_evts_bin4:	0
CRWep_n_evts_bin5:	0
CRWep_n_evts_bin6:	0
CRWep_n_evts_bin7:	0
== total:	 0

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0
CRWen_n_evts_bin3:	0
CRWen_n_evts_bin4:	0
CRWen_n_evts_bin5:	0
CRWen_n_evts_bin6:	0
CRWen_n_evts_bin7:	0
== total:	 0

CRWmp_n_evts_bin1:	0
CRWmp_n_evts_bin2:	0
CRWmp_n_evts_bin3:	0
CRWmp_n_evts_bin4:	0
CRWmp_n_evts_bin5:	0
CRWmp_n_evts_bin6:	0
CRWmp_n_evts_bin7:	0
== total:	 0

CRWmn_n_evts_bin1:	0
CRWmn_n_evts_bin2:	0
CRWmn_n_evts_bin3:	0
CRWmn_n_evts_bin4:	0
CRWmn_n_evts_bin5:	0
CRWmn_n_evts_bin6:	0
CRWmn_n_evts_bin7:	0
== total:	 0

CRWepLowMetSig_n_evts_bin1:	0
CRWepLowMetSig_n_evts_bin2:	0
CRWepLowMetSig_n_evts_bin3:	0
CRWepLowMetSig_n_evts_bin4:	0
CRWepLowMetSig_n_evts_bin5:	0
CRWepLowMetSig_n_evts_bin6:	0
CRWepLowMetSig_n_evts_bin7:	0
== total:	 0

CRWenLoeMetSig_n_evts_bin1:	0
CRWenLoeMetSig_n_evts_bin2:	0
CRWenLoeMetSig_n_evts_bin3:	0
CRWenLoeMetSig_n_evts_bin4:	0
CRWenLoeMetSig_n_evts_bin5:	0
CRWenLoeMetSig_n_evts_bin6:	0
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 0

==== SR cutflow ====
passMetTrig:		4165.56
0_el_0_mu:		4165.56
pass_n_jet:		2585.12
pass_lead_jet_pt:		2583.09
pass_sublead_jet_pt:		2565.82
pass_jj_dphi:		2409.78
pass_opp_emisph:		2409.36
pass_jj_deta:		1601.3
pass_jj_mass:		1537.5
pass_met_tst_et:		918.526
pass_met_tst_j1_dphi:		918.526
pass_met_tst_j2_dphi:		918.526
pass_met_cst_jet:		913.263

==== CRWe cutflow ====
passLepTrig:		16.0347
1_el_0_mu:		0
el_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0
met_significance:		0

==== CRWm cutflow ====
passLepTrig:		16.0347
0_el_1_mu:		0
mu_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZee cutflow ====
passLepTrig:		16.0347
2_el_0_mu:		0
el_pt:		0
opp_charge:		0
ee_mass:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZmm cutflow ====
passLepTrig:		16.0347
0_el_2_mu:		0
mu_pt:		0
opp_charge:		0
mm_mass:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRWeLowMetSig cutflow ====
passLepTrig:		16.0347
1_el_0_mu:		0
el_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0
low_met_significance:		0

