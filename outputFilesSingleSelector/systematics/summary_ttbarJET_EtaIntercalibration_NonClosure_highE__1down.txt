====  summary of selection results  ====
      sample: ttbarJET_EtaIntercalibration_NonClosure_highE__1down
========================================

==== summary of evts. in all regions ====
SR:	37.4514
CRWe:	45.6907
CRWm:	50.9621
CRZee:	4.52503
CRZmm:	4.81576
CRWeLowMetSig:	11.0843

==== events in charge-split regions ====
CRWep:	27.8071
CRWen:	17.8836
CRWmp:	29.8858
CRWmn:	21.0763
CRWepLowMetSig:	5.02525
CRWenLowMetSig:	6.05906

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	18.9405
SR_n_evts_bin2:	11.4495
SR_n_evts_bin3:	7.06137
== total:	 37.4514

CRZee_n_evts_bin1:	1.92713
CRZee_n_evts_bin2:	1.61497
CRZee_n_evts_bin3:	0.982931
== total:	 4.52503

CRZmm_n_evts_bin1:	2.38513
CRZmm_n_evts_bin2:	1.63111
CRZmm_n_evts_bin3:	0.799522
== total:	 4.81576

CRWep_n_evts_bin1:	13.2068
CRWep_n_evts_bin2:	8.99128
CRWep_n_evts_bin3:	5.60907
== total:	 27.8071

CRWen_n_evts_bin1:	8.19058
CRWen_n_evts_bin2:	4.77127
CRWen_n_evts_bin3:	4.92177
== total:	 17.8836

CRWmp_n_evts_bin1:	15.0259
CRWmp_n_evts_bin2:	10.6669
CRWmp_n_evts_bin3:	4.19293
== total:	 29.8858

CRWmn_n_evts_bin1:	11.6212
CRWmn_n_evts_bin2:	6.54697
CRWmn_n_evts_bin3:	2.90805
== total:	 21.0763

CRWepLowMetSig_n_evts_bin1:	3.02968
CRWepLowMetSig_n_evts_bin2:	1.33181
CRWepLowMetSig_n_evts_bin3:	0.66376
== total:	 5.02525

CRWenLoeMetSig_n_evts_bin1:	3.33856
CRWenLoeMetSig_n_evts_bin2:	1.24454
CRWenLoeMetSig_n_evts_bin3:	1.47596
== total:	 6.05906

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	12.2993
SR_n_evts_bin2:	8.78614
SR_n_evts_bin3:	4.23785
SR_n_evts_bin4:	6.64116
SR_n_evts_bin5:	2.66339
SR_n_evts_bin6:	2.82352
SR_n_evts_bin7:	0
== total:	 37.4514

CRZee_n_evts_bin1:	1.70084
CRZee_n_evts_bin2:	1.09086
CRZee_n_evts_bin3:	0.503422
CRZee_n_evts_bin4:	0.226292
CRZee_n_evts_bin5:	0.524109
CRZee_n_evts_bin6:	0.47951
CRZee_n_evts_bin7:	0
== total:	 4.52503

CRZmm_n_evts_bin1:	1.60768
CRZmm_n_evts_bin2:	1.17107
CRZmm_n_evts_bin3:	0.449093
CRZmm_n_evts_bin4:	0.777442
CRZmm_n_evts_bin5:	0.460038
CRZmm_n_evts_bin6:	0.350429
CRZmm_n_evts_bin7:	0
== total:	 4.81576

CRWep_n_evts_bin1:	8.44838
CRWep_n_evts_bin2:	6.30056
CRWep_n_evts_bin3:	4.24
CRWep_n_evts_bin4:	4.75837
CRWep_n_evts_bin5:	2.69072
CRWep_n_evts_bin6:	1.36907
CRWep_n_evts_bin7:	0
== total:	 27.8071

CRWen_n_evts_bin1:	5.95771
CRWen_n_evts_bin2:	3.47008
CRWen_n_evts_bin3:	2.34076
CRWen_n_evts_bin4:	2.23287
CRWen_n_evts_bin5:	1.30119
CRWen_n_evts_bin6:	2.58101
CRWen_n_evts_bin7:	0
== total:	 17.8836

CRWmp_n_evts_bin1:	11.0669
CRWmp_n_evts_bin2:	5.53421
CRWmp_n_evts_bin3:	2.09122
CRWmp_n_evts_bin4:	3.95906
CRWmp_n_evts_bin5:	5.13273
CRWmp_n_evts_bin6:	2.10171
CRWmp_n_evts_bin7:	0
== total:	 29.8858

CRWmn_n_evts_bin1:	7.98744
CRWmn_n_evts_bin2:	4.25064
CRWmn_n_evts_bin3:	2.00281
CRWmn_n_evts_bin4:	3.6338
CRWmn_n_evts_bin5:	2.29634
CRWmn_n_evts_bin6:	0.905242
CRWmn_n_evts_bin7:	0
== total:	 21.0763

CRWepLowMetSig_n_evts_bin1:	1.64772
CRWepLowMetSig_n_evts_bin2:	0.667821
CRWepLowMetSig_n_evts_bin3:	0.0648933
CRWepLowMetSig_n_evts_bin4:	1.38195
CRWepLowMetSig_n_evts_bin5:	0.663986
CRWepLowMetSig_n_evts_bin6:	0.598867
CRWepLowMetSig_n_evts_bin7:	0
== total:	 5.02525

CRWenLoeMetSig_n_evts_bin1:	1.47703
CRWenLoeMetSig_n_evts_bin2:	0.757893
CRWenLoeMetSig_n_evts_bin3:	0.818147
CRWenLoeMetSig_n_evts_bin4:	1.86153
CRWenLoeMetSig_n_evts_bin5:	0.486647
CRWenLoeMetSig_n_evts_bin6:	0.657815
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 6.05906

==== SR cutflow ====
passMetTrig:		43682.9
0_el_0_mu:		16853.9
pass_n_jet:		996.429
pass_lead_jet_pt:		847.764
pass_sublead_jet_pt:		566.049
pass_jj_dphi:		361.985
pass_opp_emisph:		342.652
pass_jj_deta:		117.687
pass_jj_mass:		103.091
pass_met_tst_et:		39.3027
pass_met_tst_j1_dphi:		39.1659
pass_met_tst_j2_dphi:		39.1659
pass_met_cst_jet:		37.4514

==== CRWe cutflow ====
passLepTrig:		36930.4
1_el_0_mu:		14407.8
el_pt:		13367.9
pass_n_jet:		924.191
pass_lead_jet_pt:		754.119
pass_sublead_jet_pt:		546.454
pass_jj_dphi:		391.064
pass_opp_emisph:		381.692
pass_jj_deta:		130.434
pass_jj_mass:		115.691
pass_met_tst_nolep_et:		58.2457
pass_met_tst_nolep_j1_dphi:		58.059
pass_met_tst_nolep_j2_dphi:		58.059
pass_met_cst_jet:		56.7751
met_significance:		45.6907

==== CRWm cutflow ====
passLepTrig:		36930.4
0_el_1_mu:		11560.3
mu_pt:		11069.5
pass_n_jet:		777.248
pass_lead_jet_pt:		638.327
pass_sublead_jet_pt:		455.049
pass_jj_dphi:		320.751
pass_opp_emisph:		310.085
pass_jj_deta:		104.085
pass_jj_mass:		91.3473
pass_met_tst_nolep_et:		51.915
pass_met_tst_nolep_j1_dphi:		51.915
pass_met_tst_nolep_j2_dphi:		51.7791
pass_met_cst_jet:		50.9621

==== CRZee cutflow ====
passLepTrig:		36930.4
2_el_0_mu:		1644.93
el_pt:		1580.78
opp_charge:		1477.82
ee_mass:		460.372
pass_n_jet:		47.7634
pass_lead_jet_pt:		40.5619
pass_sublead_jet_pt:		32.5555
pass_jj_dphi:		27.2421
pass_opp_emisph:		26.8045
pass_jj_deta:		8.8517
pass_jj_mass:		7.8381
pass_met_tst_nolep_et:		4.53138
pass_met_tst_nolep_j1_dphi:		4.53138
pass_met_tst_nolep_j2_dphi:		4.53138
pass_met_cst_jet:		4.52503

==== CRZmm cutflow ====
passLepTrig:		36930.4
0_el_2_mu:		1889.29
mu_pt:		1853.24
opp_charge:		1798.65
mm_mass:		568.249
pass_n_jet:		55.0222
pass_lead_jet_pt:		48.5865
pass_sublead_jet_pt:		38.128
pass_jj_dphi:		32.3106
pass_opp_emisph:		31.7565
pass_jj_deta:		11.1038
pass_jj_mass:		9.42516
pass_met_tst_nolep_et:		4.81576
pass_met_tst_nolep_j1_dphi:		4.81576
pass_met_tst_nolep_j2_dphi:		4.81576
pass_met_cst_jet:		4.81576

==== CRWeLowMetSig cutflow ====
passLepTrig:		36930.4
1_el_0_mu:		14407.8
el_pt:		13367.9
pass_n_jet:		924.191
pass_lead_jet_pt:		754.119
pass_sublead_jet_pt:		546.454
pass_jj_dphi:		391.064
pass_opp_emisph:		381.692
pass_jj_deta:		130.434
pass_jj_mass:		115.691
pass_met_tst_nolep_et:		58.2457
pass_met_tst_nolep_j1_dphi:		58.059
pass_met_tst_nolep_j2_dphi:		58.059
pass_met_cst_jet:		56.7751
low_met_significance:		11.0843

