====  summary of selection results  ====
      sample: Z_EWKJET_JER_EffectiveNP_6__1up
========================================

==== summary of evts. in all regions ====
SR:	196.845
CRWe:	0.76642
CRWm:	4.31094
CRZee:	18.1165
CRZmm:	22.0349
CRWeLowMetSig:	5.86392

==== events in charge-split regions ====
CRWep:	0.588
CRWen:	0.17842
CRWmp:	1.74414
CRWmn:	2.5668
CRWepLowMetSig:	2.94159
CRWenLowMetSig:	2.92233

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	39.3001
SR_n_evts_bin2:	61.3331
SR_n_evts_bin3:	96.2113
== total:	 196.844

CRZee_n_evts_bin1:	4.12204
CRZee_n_evts_bin2:	5.34436
CRZee_n_evts_bin3:	8.6501
== total:	 18.1165

CRZmm_n_evts_bin1:	4.31793
CRZmm_n_evts_bin2:	7.09173
CRZmm_n_evts_bin3:	10.6253
== total:	 22.0349

CRWep_n_evts_bin1:	0.155202
CRWep_n_evts_bin2:	0.135162
CRWep_n_evts_bin3:	0.297636
== total:	 0.588

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0.040077
CRWen_n_evts_bin3:	0.138343
== total:	 0.17842

CRWmp_n_evts_bin1:	0.393446
CRWmp_n_evts_bin2:	0.495092
CRWmp_n_evts_bin3:	0.855598
== total:	 1.74414

CRWmn_n_evts_bin1:	0.58539
CRWmn_n_evts_bin2:	0.770693
CRWmn_n_evts_bin3:	1.21072
== total:	 2.5668

CRWepLowMetSig_n_evts_bin1:	0.515694
CRWepLowMetSig_n_evts_bin2:	1.12383
CRWepLowMetSig_n_evts_bin3:	1.30207
== total:	 2.94159

CRWenLoeMetSig_n_evts_bin1:	0.382031
CRWenLoeMetSig_n_evts_bin2:	0.932101
CRWenLoeMetSig_n_evts_bin3:	1.6082
== total:	 2.92233

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	25.9504
SR_n_evts_bin2:	30.7466
SR_n_evts_bin3:	39.8493
SR_n_evts_bin4:	13.3497
SR_n_evts_bin5:	30.5865
SR_n_evts_bin6:	56.3619
SR_n_evts_bin7:	0
== total:	 196.844

CRZee_n_evts_bin1:	2.64546
CRZee_n_evts_bin2:	3.08679
CRZee_n_evts_bin3:	4.00264
CRZee_n_evts_bin4:	1.47658
CRZee_n_evts_bin5:	2.25757
CRZee_n_evts_bin6:	4.64746
CRZee_n_evts_bin7:	0
== total:	 18.1165

CRZmm_n_evts_bin1:	3.08435
CRZmm_n_evts_bin2:	2.97859
CRZmm_n_evts_bin3:	4.58555
CRZmm_n_evts_bin4:	1.23358
CRZmm_n_evts_bin5:	4.11314
CRZmm_n_evts_bin6:	6.03972
CRZmm_n_evts_bin7:	0
== total:	 22.0349

CRWep_n_evts_bin1:	0.0930242
CRWep_n_evts_bin2:	0.0512438
CRWep_n_evts_bin3:	0.0813908
CRWep_n_evts_bin4:	0.0621776
CRWep_n_evts_bin5:	0.0839181
CRWep_n_evts_bin6:	0.216246
CRWep_n_evts_bin7:	0
== total:	 0.588

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0.040077
CRWen_n_evts_bin3:	0.0862128
CRWen_n_evts_bin4:	0
CRWen_n_evts_bin5:	0
CRWen_n_evts_bin6:	0.0521298
CRWen_n_evts_bin7:	0
== total:	 0.17842

CRWmp_n_evts_bin1:	0.305618
CRWmp_n_evts_bin2:	0.390658
CRWmp_n_evts_bin3:	0.199518
CRWmp_n_evts_bin4:	0.0878283
CRWmp_n_evts_bin5:	0.104434
CRWmp_n_evts_bin6:	0.65608
CRWmp_n_evts_bin7:	0
== total:	 1.74414

CRWmn_n_evts_bin1:	0.298221
CRWmn_n_evts_bin2:	0.271161
CRWmn_n_evts_bin3:	0.469519
CRWmn_n_evts_bin4:	0.287169
CRWmn_n_evts_bin5:	0.499532
CRWmn_n_evts_bin6:	0.741197
CRWmn_n_evts_bin7:	0
== total:	 2.5668

CRWepLowMetSig_n_evts_bin1:	0.32024
CRWepLowMetSig_n_evts_bin2:	0.497681
CRWepLowMetSig_n_evts_bin3:	0.526746
CRWepLowMetSig_n_evts_bin4:	0.195455
CRWepLowMetSig_n_evts_bin5:	0.626149
CRWepLowMetSig_n_evts_bin6:	0.775322
CRWepLowMetSig_n_evts_bin7:	0
== total:	 2.94159

CRWenLoeMetSig_n_evts_bin1:	0.203366
CRWenLoeMetSig_n_evts_bin2:	0.169754
CRWenLoeMetSig_n_evts_bin3:	0.830306
CRWenLoeMetSig_n_evts_bin4:	0.178666
CRWenLoeMetSig_n_evts_bin5:	0.762347
CRWenLoeMetSig_n_evts_bin6:	0.77789
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 2.92233

==== SR cutflow ====
passMetTrig:		1669.35
0_el_0_mu:		1390.36
pass_n_jet:		768.743
pass_lead_jet_pt:		767.301
pass_sublead_jet_pt:		762.698
pass_jj_dphi:		633.747
pass_opp_emisph:		633.476
pass_jj_deta:		296.933
pass_jj_mass:		290.202
pass_met_tst_et:		198.167
pass_met_tst_j1_dphi:		198.167
pass_met_tst_j2_dphi:		198.167
pass_met_cst_jet:		196.845

==== CRWe cutflow ====
passLepTrig:		504.077
1_el_0_mu:		114.313
el_pt:		110.069
pass_n_jet:		51.8129
pass_lead_jet_pt:		49.3819
pass_sublead_jet_pt:		44.2433
pass_jj_dphi:		28.8232
pass_opp_emisph:		28.3557
pass_jj_deta:		10.9613
pass_jj_mass:		10.6708
pass_met_tst_nolep_et:		6.77105
pass_met_tst_nolep_j1_dphi:		6.77105
pass_met_tst_nolep_j2_dphi:		6.71955
pass_met_cst_jet:		6.63034
met_significance:		0.76642

==== CRWm cutflow ====
passLepTrig:		504.077
0_el_1_mu:		40.4585
mu_pt:		39.6903
pass_n_jet:		19.9565
pass_lead_jet_pt:		19.7819
pass_sublead_jet_pt:		19.0188
pass_jj_dphi:		14.9169
pass_opp_emisph:		14.8628
pass_jj_deta:		7.41634
pass_jj_mass:		7.23203
pass_met_tst_nolep_et:		4.31094
pass_met_tst_nolep_j1_dphi:		4.31094
pass_met_tst_nolep_j2_dphi:		4.31094
pass_met_cst_jet:		4.31094

==== CRZee cutflow ====
passLepTrig:		504.077
2_el_0_mu:		142.773
el_pt:		142.716
opp_charge:		140.574
ee_mass:		123.925
pass_n_jet:		72.086
pass_lead_jet_pt:		71.8874
pass_sublead_jet_pt:		71.0184
pass_jj_dphi:		59.6798
pass_opp_emisph:		59.6798
pass_jj_deta:		27.2582
pass_jj_mass:		26.4601
pass_met_tst_nolep_et:		18.2045
pass_met_tst_nolep_j1_dphi:		18.2045
pass_met_tst_nolep_j2_dphi:		18.2045
pass_met_cst_jet:		18.1165

==== CRZmm cutflow ====
passLepTrig:		504.077
0_el_2_mu:		167.88
mu_pt:		167.721
opp_charge:		167.721
mm_mass:		147.097
pass_n_jet:		85.1227
pass_lead_jet_pt:		85.0787
pass_sublead_jet_pt:		84.051
pass_jj_dphi:		70.1793
pass_opp_emisph:		70.0559
pass_jj_deta:		34.0415
pass_jj_mass:		33.1278
pass_met_tst_nolep_et:		22.1761
pass_met_tst_nolep_j1_dphi:		22.1761
pass_met_tst_nolep_j2_dphi:		22.1761
pass_met_cst_jet:		22.0349

==== CRWeLowMetSig cutflow ====
passLepTrig:		504.077
1_el_0_mu:		114.313
el_pt:		110.069
pass_n_jet:		51.8129
pass_lead_jet_pt:		49.3819
pass_sublead_jet_pt:		44.2433
pass_jj_dphi:		28.8232
pass_opp_emisph:		28.3557
pass_jj_deta:		10.9613
pass_jj_mass:		10.6708
pass_met_tst_nolep_et:		6.77105
pass_met_tst_nolep_j1_dphi:		6.77105
pass_met_tst_nolep_j2_dphi:		6.71955
pass_met_cst_jet:		6.63034
low_met_significance:		5.86392

