====  summary of selection results  ====
      sample: W_strongJET_JER_EffectiveNP_7restTerm__1up
========================================

==== summary of evts. in all regions ====
SR:	910.181
CRWe:	551.292
CRWm:	842.092
CRZee:	0.204286
CRZmm:	0.095122
CRWeLowMetSig:	332.769

==== events in charge-split regions ====
CRWep:	358.117
CRWen:	193.175
CRWmp:	540.422
CRWmn:	301.67
CRWepLowMetSig:	164.882
CRWenLowMetSig:	167.888

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	405.45
SR_n_evts_bin2:	314.172
SR_n_evts_bin3:	190.559
== total:	 910.181

CRZee_n_evts_bin1:	0.0701869
CRZee_n_evts_bin2:	-0.00645132
CRZee_n_evts_bin3:	0.14055
== total:	 0.204286

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0.095122
CRZmm_n_evts_bin3:	0
== total:	 0.095122

CRWep_n_evts_bin1:	144.346
CRWep_n_evts_bin2:	126.559
CRWep_n_evts_bin3:	87.2118
== total:	 358.117

CRWen_n_evts_bin1:	87.7613
CRWen_n_evts_bin2:	62.4396
CRWen_n_evts_bin3:	42.9743
== total:	 193.175

CRWmp_n_evts_bin1:	241.67
CRWmp_n_evts_bin2:	179.398
CRWmp_n_evts_bin3:	119.354
== total:	 540.422

CRWmn_n_evts_bin1:	153.915
CRWmn_n_evts_bin2:	93.5098
CRWmn_n_evts_bin3:	54.2448
== total:	 301.67

CRWepLowMetSig_n_evts_bin1:	73.9539
CRWepLowMetSig_n_evts_bin2:	58.3884
CRWepLowMetSig_n_evts_bin3:	32.539
== total:	 164.881

CRWenLoeMetSig_n_evts_bin1:	78.9677
CRWenLoeMetSig_n_evts_bin2:	57.6362
CRWenLoeMetSig_n_evts_bin3:	31.2836
== total:	 167.887

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	278.374
SR_n_evts_bin2:	173.683
SR_n_evts_bin3:	102.173
SR_n_evts_bin4:	127.076
SR_n_evts_bin5:	140.489
SR_n_evts_bin6:	88.3853
SR_n_evts_bin7:	0
== total:	 910.181

CRZee_n_evts_bin1:	0.0701869
CRZee_n_evts_bin2:	0.0966545
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	-0.103106
CRZee_n_evts_bin6:	0.14055
CRZee_n_evts_bin7:	0
== total:	 0.204286

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0.095122
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0.095122

CRWep_n_evts_bin1:	99.8826
CRWep_n_evts_bin2:	73.3245
CRWep_n_evts_bin3:	53.9381
CRWep_n_evts_bin4:	44.4637
CRWep_n_evts_bin5:	53.2344
CRWep_n_evts_bin6:	33.2737
CRWep_n_evts_bin7:	0
== total:	 358.117

CRWen_n_evts_bin1:	61.2756
CRWen_n_evts_bin2:	40.0522
CRWen_n_evts_bin3:	22.3691
CRWen_n_evts_bin4:	26.4857
CRWen_n_evts_bin5:	22.3874
CRWen_n_evts_bin6:	20.6052
CRWen_n_evts_bin7:	0
== total:	 193.175

CRWmp_n_evts_bin1:	162.399
CRWmp_n_evts_bin2:	96.4561
CRWmp_n_evts_bin3:	55.2924
CRWmp_n_evts_bin4:	79.2713
CRWmp_n_evts_bin5:	82.9421
CRWmp_n_evts_bin6:	64.0616
CRWmp_n_evts_bin7:	0
== total:	 540.422

CRWmn_n_evts_bin1:	113.931
CRWmn_n_evts_bin2:	44.6951
CRWmn_n_evts_bin3:	29.0407
CRWmn_n_evts_bin4:	39.9841
CRWmn_n_evts_bin5:	48.8146
CRWmn_n_evts_bin6:	25.204
CRWmn_n_evts_bin7:	0
== total:	 301.67

CRWepLowMetSig_n_evts_bin1:	51.7233
CRWepLowMetSig_n_evts_bin2:	27.7736
CRWepLowMetSig_n_evts_bin3:	15.7685
CRWepLowMetSig_n_evts_bin4:	22.2306
CRWepLowMetSig_n_evts_bin5:	30.6148
CRWepLowMetSig_n_evts_bin6:	16.7705
CRWepLowMetSig_n_evts_bin7:	0
== total:	 164.881

CRWenLoeMetSig_n_evts_bin1:	53.3551
CRWenLoeMetSig_n_evts_bin2:	20.6266
CRWenLoeMetSig_n_evts_bin3:	17.1926
CRWenLoeMetSig_n_evts_bin4:	25.6125
CRWenLoeMetSig_n_evts_bin5:	37.0095
CRWenLoeMetSig_n_evts_bin6:	14.0911
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 167.887

==== SR cutflow ====
passMetTrig:		82764.1
0_el_0_mu:		48277.5
pass_n_jet:		11424.8
pass_lead_jet_pt:		10601.8
pass_sublead_jet_pt:		7668.79
pass_jj_dphi:		6028.74
pass_opp_emisph:		5922.81
pass_jj_deta:		2702.13
pass_jj_mass:		2466.89
pass_met_tst_et:		943.919
pass_met_tst_j1_dphi:		943.764
pass_met_tst_j2_dphi:		943.676
pass_met_cst_jet:		910.181

==== CRWe cutflow ====
passLepTrig:		54410.5
1_el_0_mu:		22793.4
el_pt:		22157.7
pass_n_jet:		5599.2
pass_lead_jet_pt:		5469.76
pass_sublead_jet_pt:		5002.03
pass_jj_dphi:		4328.34
pass_opp_emisph:		4323.7
pass_jj_deta:		1882.99
pass_jj_mass:		1718.73
pass_met_tst_nolep_et:		900.257
pass_met_tst_nolep_j1_dphi:		900.257
pass_met_tst_nolep_j2_dphi:		899.723
pass_met_cst_jet:		884.062
met_significance:		551.292

==== CRWm cutflow ====
passLepTrig:		54410.5
0_el_1_mu:		20452
mu_pt:		19762.2
pass_n_jet:		4984.77
pass_lead_jet_pt:		4888.39
pass_sublead_jet_pt:		4433.68
pass_jj_dphi:		3871.83
pass_opp_emisph:		3870.7
pass_jj_deta:		1685.78
pass_jj_mass:		1511.81
pass_met_tst_nolep_et:		847.822
pass_met_tst_nolep_j1_dphi:		847.736
pass_met_tst_nolep_j2_dphi:		847.736
pass_met_cst_jet:		842.092

==== CRZee cutflow ====
passLepTrig:		54410.5
2_el_0_mu:		40.0357
el_pt:		37.3556
opp_charge:		18.277
ee_mass:		6.05077
pass_n_jet:		0.979293
pass_lead_jet_pt:		0.979293
pass_sublead_jet_pt:		0.979293
pass_jj_dphi:		0.979293
pass_opp_emisph:		0.979293
pass_jj_deta:		0.250486
pass_jj_mass:		0.250486
pass_met_tst_nolep_et:		0.204286
pass_met_tst_nolep_j1_dphi:		0.204286
pass_met_tst_nolep_j2_dphi:		0.204286
pass_met_cst_jet:		0.204286

==== CRZmm cutflow ====
passLepTrig:		54410.5
0_el_2_mu:		32.4121
mu_pt:		31.2996
opp_charge:		18.8853
mm_mass:		3.34794
pass_n_jet:		0.182382
pass_lead_jet_pt:		0.182382
pass_sublead_jet_pt:		0.182382
pass_jj_dphi:		0.182382
pass_opp_emisph:		0.182382
pass_jj_deta:		0.182382
pass_jj_mass:		0.182382
pass_met_tst_nolep_et:		0.095122
pass_met_tst_nolep_j1_dphi:		0.095122
pass_met_tst_nolep_j2_dphi:		0.095122
pass_met_cst_jet:		0.095122

==== CRWeLowMetSig cutflow ====
passLepTrig:		54410.5
1_el_0_mu:		22793.4
el_pt:		22157.7
pass_n_jet:		5599.2
pass_lead_jet_pt:		5469.76
pass_sublead_jet_pt:		5002.03
pass_jj_dphi:		4328.34
pass_opp_emisph:		4323.7
pass_jj_deta:		1882.99
pass_jj_mass:		1718.73
pass_met_tst_nolep_et:		900.257
pass_met_tst_nolep_j1_dphi:		900.257
pass_met_tst_nolep_j2_dphi:		899.723
pass_met_cst_jet:		884.062
low_met_significance:		332.769

