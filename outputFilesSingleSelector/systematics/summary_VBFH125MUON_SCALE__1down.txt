====  summary of selection results  ====
      sample: VBFH125MUON_SCALE__1down
========================================

==== summary of evts. in all regions ====
SR:	940.314
CRWe:	0
CRWm:	0
CRZee:	0
CRZmm:	0
CRWeLowMetSig:	0

==== events in charge-split regions ====
CRWep:	0
CRWen:	0
CRWmp:	0
CRWmn:	0
CRWepLowMetSig:	0
CRWenLowMetSig:	0

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	241.863
SR_n_evts_bin2:	298.035
SR_n_evts_bin3:	400.415
== total:	 940.313

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
== total:	 0

CRWep_n_evts_bin1:	0
CRWep_n_evts_bin2:	0
CRWep_n_evts_bin3:	0
== total:	 0

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0
CRWen_n_evts_bin3:	0
== total:	 0

CRWmp_n_evts_bin1:	0
CRWmp_n_evts_bin2:	0
CRWmp_n_evts_bin3:	0
== total:	 0

CRWmn_n_evts_bin1:	0
CRWmn_n_evts_bin2:	0
CRWmn_n_evts_bin3:	0
== total:	 0

CRWepLowMetSig_n_evts_bin1:	0
CRWepLowMetSig_n_evts_bin2:	0
CRWepLowMetSig_n_evts_bin3:	0
== total:	 0

CRWenLoeMetSig_n_evts_bin1:	0
CRWenLoeMetSig_n_evts_bin2:	0
CRWenLoeMetSig_n_evts_bin3:	0
== total:	 0

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	180.785
SR_n_evts_bin2:	197.457
SR_n_evts_bin3:	251.384
SR_n_evts_bin4:	61.0782
SR_n_evts_bin5:	100.578
SR_n_evts_bin6:	149.031
SR_n_evts_bin7:	0
== total:	 940.313

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	0
CRZee_n_evts_bin6:	0
CRZee_n_evts_bin7:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0

CRWep_n_evts_bin1:	0
CRWep_n_evts_bin2:	0
CRWep_n_evts_bin3:	0
CRWep_n_evts_bin4:	0
CRWep_n_evts_bin5:	0
CRWep_n_evts_bin6:	0
CRWep_n_evts_bin7:	0
== total:	 0

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0
CRWen_n_evts_bin3:	0
CRWen_n_evts_bin4:	0
CRWen_n_evts_bin5:	0
CRWen_n_evts_bin6:	0
CRWen_n_evts_bin7:	0
== total:	 0

CRWmp_n_evts_bin1:	0
CRWmp_n_evts_bin2:	0
CRWmp_n_evts_bin3:	0
CRWmp_n_evts_bin4:	0
CRWmp_n_evts_bin5:	0
CRWmp_n_evts_bin6:	0
CRWmp_n_evts_bin7:	0
== total:	 0

CRWmn_n_evts_bin1:	0
CRWmn_n_evts_bin2:	0
CRWmn_n_evts_bin3:	0
CRWmn_n_evts_bin4:	0
CRWmn_n_evts_bin5:	0
CRWmn_n_evts_bin6:	0
CRWmn_n_evts_bin7:	0
== total:	 0

CRWepLowMetSig_n_evts_bin1:	0
CRWepLowMetSig_n_evts_bin2:	0
CRWepLowMetSig_n_evts_bin3:	0
CRWepLowMetSig_n_evts_bin4:	0
CRWepLowMetSig_n_evts_bin5:	0
CRWepLowMetSig_n_evts_bin6:	0
CRWepLowMetSig_n_evts_bin7:	0
== total:	 0

CRWenLoeMetSig_n_evts_bin1:	0
CRWenLoeMetSig_n_evts_bin2:	0
CRWenLoeMetSig_n_evts_bin3:	0
CRWenLoeMetSig_n_evts_bin4:	0
CRWenLoeMetSig_n_evts_bin5:	0
CRWenLoeMetSig_n_evts_bin6:	0
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 0

==== SR cutflow ====
passMetTrig:		4136.24
0_el_0_mu:		4136.24
pass_n_jet:		2660.1
pass_lead_jet_pt:		2657.91
pass_sublead_jet_pt:		2640.22
pass_jj_dphi:		2480.26
pass_opp_emisph:		2479.89
pass_jj_deta:		1648.88
pass_jj_mass:		1582.82
pass_met_tst_et:		945.822
pass_met_tst_j1_dphi:		945.822
pass_met_tst_j2_dphi:		945.822
pass_met_cst_jet:		940.314

==== CRWe cutflow ====
passLepTrig:		14.5759
1_el_0_mu:		0
el_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0
met_significance:		0

==== CRWm cutflow ====
passLepTrig:		14.5759
0_el_1_mu:		0
mu_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZee cutflow ====
passLepTrig:		14.5759
2_el_0_mu:		0
el_pt:		0
opp_charge:		0
ee_mass:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZmm cutflow ====
passLepTrig:		14.5759
0_el_2_mu:		0
mu_pt:		0
opp_charge:		0
mm_mass:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRWeLowMetSig cutflow ====
passLepTrig:		14.5759
1_el_0_mu:		0
el_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0
low_met_significance:		0

