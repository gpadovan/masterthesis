====  summary of selection results  ====
      sample: Z_strongEG_RESOLUTION_ALL__1up
========================================

==== summary of evts. in all regions ====
SR:	1170.81
CRWe:	5.37987
CRWm:	37.3487
CRZee:	89.7104
CRZmm:	105.353
CRWeLowMetSig:	41.0681

==== events in charge-split regions ====
CRWep:	1.65716
CRWen:	3.7227
CRWmp:	18.5636
CRWmn:	18.7852
CRWepLowMetSig:	22.3385
CRWenLowMetSig:	18.7296

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	539.295
SR_n_evts_bin2:	376.971
SR_n_evts_bin3:	254.549
== total:	 1170.82

CRZee_n_evts_bin1:	41.3826
CRZee_n_evts_bin2:	30.523
CRZee_n_evts_bin3:	17.8048
== total:	 89.7104

CRZmm_n_evts_bin1:	52.3198
CRZmm_n_evts_bin2:	25.6817
CRZmm_n_evts_bin3:	27.3515
== total:	 105.353

CRWep_n_evts_bin1:	1.24893
CRWep_n_evts_bin2:	0.111398
CRWep_n_evts_bin3:	0.296836
== total:	 1.65716

CRWen_n_evts_bin1:	1.33393
CRWen_n_evts_bin2:	1.69167
CRWen_n_evts_bin3:	0.697102
== total:	 3.7227

CRWmp_n_evts_bin1:	9.08088
CRWmp_n_evts_bin2:	4.58022
CRWmp_n_evts_bin3:	4.90246
== total:	 18.5636

CRWmn_n_evts_bin1:	10.5299
CRWmn_n_evts_bin2:	4.51683
CRWmn_n_evts_bin3:	3.73848
== total:	 18.7852

CRWepLowMetSig_n_evts_bin1:	9.64963
CRWepLowMetSig_n_evts_bin2:	5.81815
CRWepLowMetSig_n_evts_bin3:	6.87074
== total:	 22.3385

CRWenLoeMetSig_n_evts_bin1:	10.8112
CRWenLoeMetSig_n_evts_bin2:	5.78793
CRWenLoeMetSig_n_evts_bin3:	2.13045
== total:	 18.7296

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	371.643
SR_n_evts_bin2:	200.506
SR_n_evts_bin3:	125.438
SR_n_evts_bin4:	167.652
SR_n_evts_bin5:	176.464
SR_n_evts_bin6:	129.111
SR_n_evts_bin7:	0
== total:	 1170.81

CRZee_n_evts_bin1:	27.5402
CRZee_n_evts_bin2:	15.5902
CRZee_n_evts_bin3:	9.62118
CRZee_n_evts_bin4:	13.8424
CRZee_n_evts_bin5:	14.9328
CRZee_n_evts_bin6:	8.18361
CRZee_n_evts_bin7:	0
== total:	 89.7104

CRZmm_n_evts_bin1:	37.5009
CRZmm_n_evts_bin2:	13.5054
CRZmm_n_evts_bin3:	15.0207
CRZmm_n_evts_bin4:	14.8189
CRZmm_n_evts_bin5:	12.1763
CRZmm_n_evts_bin6:	12.3308
CRZmm_n_evts_bin7:	0
== total:	 105.353

CRWep_n_evts_bin1:	1.00423
CRWep_n_evts_bin2:	0.231275
CRWep_n_evts_bin3:	-0.187961
CRWep_n_evts_bin4:	0.244703
CRWep_n_evts_bin5:	-0.119877
CRWep_n_evts_bin6:	0.484796
CRWep_n_evts_bin7:	0
== total:	 1.65716

CRWen_n_evts_bin1:	0.913607
CRWen_n_evts_bin2:	0.608824
CRWen_n_evts_bin3:	0.252532
CRWen_n_evts_bin4:	0.420324
CRWen_n_evts_bin5:	1.08285
CRWen_n_evts_bin6:	0.44457
CRWen_n_evts_bin7:	0
== total:	 3.7227

CRWmp_n_evts_bin1:	6.37413
CRWmp_n_evts_bin2:	1.56223
CRWmp_n_evts_bin3:	0.797321
CRWmp_n_evts_bin4:	2.70674
CRWmp_n_evts_bin5:	3.01799
CRWmp_n_evts_bin6:	4.10514
CRWmp_n_evts_bin7:	0
== total:	 18.5636

CRWmn_n_evts_bin1:	5.26884
CRWmn_n_evts_bin2:	3.5847
CRWmn_n_evts_bin3:	2.34888
CRWmn_n_evts_bin4:	5.26102
CRWmn_n_evts_bin5:	0.932129
CRWmn_n_evts_bin6:	1.3896
CRWmn_n_evts_bin7:	0
== total:	 18.7852

CRWepLowMetSig_n_evts_bin1:	4.24041
CRWepLowMetSig_n_evts_bin2:	4.11481
CRWepLowMetSig_n_evts_bin3:	3.51733
CRWepLowMetSig_n_evts_bin4:	5.40921
CRWepLowMetSig_n_evts_bin5:	1.70334
CRWepLowMetSig_n_evts_bin6:	3.35341
CRWepLowMetSig_n_evts_bin7:	0
== total:	 22.3385

CRWenLoeMetSig_n_evts_bin1:	6.89579
CRWenLoeMetSig_n_evts_bin2:	3.66087
CRWenLoeMetSig_n_evts_bin3:	-1.02948
CRWenLoeMetSig_n_evts_bin4:	3.91544
CRWenLoeMetSig_n_evts_bin5:	2.12707
CRWenLoeMetSig_n_evts_bin6:	3.15993
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 18.7296

==== SR cutflow ====
passMetTrig:		37219.8
0_el_0_mu:		31257.2
pass_n_jet:		7392.89
pass_lead_jet_pt:		7267.74
pass_sublead_jet_pt:		6740.45
pass_jj_dphi:		5817.35
pass_opp_emisph:		5807.29
pass_jj_deta:		2644
pass_jj_mass:		2346.3
pass_met_tst_et:		1214.36
pass_met_tst_j1_dphi:		1213.91
pass_met_tst_j2_dphi:		1213.86
pass_met_cst_jet:		1170.81

==== CRWe cutflow ====
passLepTrig:		14702.9
1_el_0_mu:		5417.76
el_pt:		5144.3
pass_n_jet:		1697.33
pass_lead_jet_pt:		1226.57
pass_sublead_jet_pt:		629.518
pass_jj_dphi:		414.709
pass_opp_emisph:		392.657
pass_jj_deta:		107.179
pass_jj_mass:		96.9052
pass_met_tst_nolep_et:		46.9237
pass_met_tst_nolep_j1_dphi:		46.9237
pass_met_tst_nolep_j2_dphi:		46.9237
pass_met_cst_jet:		46.448
met_significance:		5.37987

==== CRWm cutflow ====
passLepTrig:		14702.9
0_el_1_mu:		1470.19
mu_pt:		1409.69
pass_n_jet:		301.439
pass_lead_jet_pt:		272.356
pass_sublead_jet_pt:		234.751
pass_jj_dphi:		195.258
pass_opp_emisph:		192.658
pass_jj_deta:		82.1007
pass_jj_mass:		74.551
pass_met_tst_nolep_et:		38.7764
pass_met_tst_nolep_j1_dphi:		38.7764
pass_met_tst_nolep_j2_dphi:		38.7764
pass_met_cst_jet:		37.3487

==== CRZee cutflow ====
passLepTrig:		14702.9
2_el_0_mu:		2655.09
el_pt:		2645.82
opp_charge:		2563.22
ee_mass:		2133.29
pass_n_jet:		551.076
pass_lead_jet_pt:		525.697
pass_sublead_jet_pt:		470.229
pass_jj_dphi:		407.048
pass_opp_emisph:		406.169
pass_jj_deta:		166.57
pass_jj_mass:		149.236
pass_met_tst_nolep_et:		90.3948
pass_met_tst_nolep_j1_dphi:		90.3948
pass_met_tst_nolep_j2_dphi:		90.3948
pass_met_cst_jet:		89.7104

==== CRZmm cutflow ====
passLepTrig:		14702.9
0_el_2_mu:		3422.92
mu_pt:		3414.86
opp_charge:		3413.44
mm_mass:		2816.54
pass_n_jet:		735.576
pass_lead_jet_pt:		724.71
pass_sublead_jet_pt:		646.903
pass_jj_dphi:		561.856
pass_opp_emisph:		562.361
pass_jj_deta:		243.971
pass_jj_mass:		214.874
pass_met_tst_nolep_et:		106.2
pass_met_tst_nolep_j1_dphi:		106.2
pass_met_tst_nolep_j2_dphi:		106.2
pass_met_cst_jet:		105.353

==== CRWeLowMetSig cutflow ====
passLepTrig:		14702.9
1_el_0_mu:		5417.76
el_pt:		5144.3
pass_n_jet:		1697.33
pass_lead_jet_pt:		1226.57
pass_sublead_jet_pt:		629.518
pass_jj_dphi:		414.709
pass_opp_emisph:		392.657
pass_jj_deta:		107.179
pass_jj_mass:		96.9052
pass_met_tst_nolep_et:		46.9237
pass_met_tst_nolep_j1_dphi:		46.9237
pass_met_tst_nolep_j2_dphi:		46.9237
pass_met_cst_jet:		46.448
low_met_significance:		41.0681

