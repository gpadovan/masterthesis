====  summary of selection results  ====
      sample: ttbarEG_RESOLUTION_ALL__1up
========================================

==== summary of evts. in all regions ====
SR:	37.4514
CRWe:	46.0475
CRWm:	50.9193
CRZee:	4.69415
CRZmm:	4.81576
CRWeLowMetSig:	11.0882

==== events in charge-split regions ====
CRWep:	28.1646
CRWen:	17.883
CRWmp:	29.8858
CRWmn:	21.0335
CRWepLowMetSig:	5.02525
CRWenLowMetSig:	6.06293

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	18.9405
SR_n_evts_bin2:	11.4495
SR_n_evts_bin3:	7.06137
== total:	 37.4514

CRZee_n_evts_bin1:	2.06457
CRZee_n_evts_bin2:	1.61469
CRZee_n_evts_bin3:	1.01489
== total:	 4.69415

CRZmm_n_evts_bin1:	2.38513
CRZmm_n_evts_bin2:	1.63111
CRZmm_n_evts_bin3:	0.799522
== total:	 4.81576

CRWep_n_evts_bin1:	13.2166
CRWep_n_evts_bin2:	8.99111
CRWep_n_evts_bin3:	5.95687
== total:	 28.1646

CRWen_n_evts_bin1:	8.19058
CRWen_n_evts_bin2:	4.76959
CRWen_n_evts_bin3:	4.92281
== total:	 17.883

CRWmp_n_evts_bin1:	15.0259
CRWmp_n_evts_bin2:	10.6669
CRWmp_n_evts_bin3:	4.19293
== total:	 29.8858

CRWmn_n_evts_bin1:	11.5785
CRWmn_n_evts_bin2:	6.54697
CRWmn_n_evts_bin3:	2.90805
== total:	 21.0335

CRWepLowMetSig_n_evts_bin1:	3.02968
CRWepLowMetSig_n_evts_bin2:	1.33181
CRWepLowMetSig_n_evts_bin3:	0.66376
== total:	 5.02525

CRWenLoeMetSig_n_evts_bin1:	3.33856
CRWenLoeMetSig_n_evts_bin2:	1.24454
CRWenLoeMetSig_n_evts_bin3:	1.47983
== total:	 6.06293

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	12.2993
SR_n_evts_bin2:	8.78614
SR_n_evts_bin3:	4.23785
SR_n_evts_bin4:	6.64116
SR_n_evts_bin5:	2.66339
SR_n_evts_bin6:	2.82352
SR_n_evts_bin7:	0
== total:	 37.4514

CRZee_n_evts_bin1:	1.83819
CRZee_n_evts_bin2:	1.09058
CRZee_n_evts_bin3:	0.504025
CRZee_n_evts_bin4:	0.226379
CRZee_n_evts_bin5:	0.524109
CRZee_n_evts_bin6:	0.510863
CRZee_n_evts_bin7:	0
== total:	 4.69415

CRZmm_n_evts_bin1:	1.60768
CRZmm_n_evts_bin2:	1.17107
CRZmm_n_evts_bin3:	0.449093
CRZmm_n_evts_bin4:	0.777442
CRZmm_n_evts_bin5:	0.460038
CRZmm_n_evts_bin6:	0.350429
CRZmm_n_evts_bin7:	0
== total:	 4.81576

CRWep_n_evts_bin1:	8.44758
CRWep_n_evts_bin2:	6.30056
CRWep_n_evts_bin3:	4.24015
CRWep_n_evts_bin4:	4.76902
CRWep_n_evts_bin5:	2.69055
CRWep_n_evts_bin6:	1.71672
CRWep_n_evts_bin7:	0
== total:	 28.1646

CRWen_n_evts_bin1:	5.95771
CRWen_n_evts_bin2:	3.46839
CRWen_n_evts_bin3:	2.34566
CRWen_n_evts_bin4:	2.23287
CRWen_n_evts_bin5:	1.30119
CRWen_n_evts_bin6:	2.57715
CRWen_n_evts_bin7:	0
== total:	 17.883

CRWmp_n_evts_bin1:	11.0669
CRWmp_n_evts_bin2:	5.53421
CRWmp_n_evts_bin3:	2.09122
CRWmp_n_evts_bin4:	3.95906
CRWmp_n_evts_bin5:	5.13273
CRWmp_n_evts_bin6:	2.10171
CRWmp_n_evts_bin7:	0
== total:	 29.8858

CRWmn_n_evts_bin1:	7.94467
CRWmn_n_evts_bin2:	4.25064
CRWmn_n_evts_bin3:	2.00281
CRWmn_n_evts_bin4:	3.6338
CRWmn_n_evts_bin5:	2.29634
CRWmn_n_evts_bin6:	0.905242
CRWmn_n_evts_bin7:	0
== total:	 21.0335

CRWepLowMetSig_n_evts_bin1:	1.64772
CRWepLowMetSig_n_evts_bin2:	0.667821
CRWepLowMetSig_n_evts_bin3:	0.0648933
CRWepLowMetSig_n_evts_bin4:	1.38195
CRWepLowMetSig_n_evts_bin5:	0.663986
CRWepLowMetSig_n_evts_bin6:	0.598867
CRWepLowMetSig_n_evts_bin7:	0
== total:	 5.02525

CRWenLoeMetSig_n_evts_bin1:	1.47703
CRWenLoeMetSig_n_evts_bin2:	0.757893
CRWenLoeMetSig_n_evts_bin3:	0.818147
CRWenLoeMetSig_n_evts_bin4:	1.86153
CRWenLoeMetSig_n_evts_bin5:	0.486647
CRWenLoeMetSig_n_evts_bin6:	0.66168
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 6.06293

==== SR cutflow ====
passMetTrig:		43687.6
0_el_0_mu:		16859
pass_n_jet:		996.112
pass_lead_jet_pt:		847.319
pass_sublead_jet_pt:		565.254
pass_jj_dphi:		361.218
pass_opp_emisph:		342.348
pass_jj_deta:		117.458
pass_jj_mass:		102.911
pass_met_tst_et:		39.3027
pass_met_tst_j1_dphi:		39.1659
pass_met_tst_j2_dphi:		39.1659
pass_met_cst_jet:		37.4514

==== CRWe cutflow ====
passLepTrig:		36935.7
1_el_0_mu:		14408.6
el_pt:		13368.8
pass_n_jet:		926.001
pass_lead_jet_pt:		755.89
pass_sublead_jet_pt:		547.892
pass_jj_dphi:		391.76
pass_opp_emisph:		382.026
pass_jj_deta:		130.755
pass_jj_mass:		116.007
pass_met_tst_nolep_et:		58.6064
pass_met_tst_nolep_j1_dphi:		58.4197
pass_met_tst_nolep_j2_dphi:		58.4197
pass_met_cst_jet:		57.1357
met_significance:		46.0475

==== CRWm cutflow ====
passLepTrig:		36935.7
0_el_1_mu:		11562.3
mu_pt:		11071.9
pass_n_jet:		778.133
pass_lead_jet_pt:		638.722
pass_sublead_jet_pt:		455.135
pass_jj_dphi:		320.704
pass_opp_emisph:		310.038
pass_jj_deta:		104.043
pass_jj_mass:		91.3045
pass_met_tst_nolep_et:		51.8722
pass_met_tst_nolep_j1_dphi:		51.8722
pass_met_tst_nolep_j2_dphi:		51.7364
pass_met_cst_jet:		50.9193

==== CRZee cutflow ====
passLepTrig:		36935.7
2_el_0_mu:		1647.07
el_pt:		1582.71
opp_charge:		1479.19
ee_mass:		460.597
pass_n_jet:		48.1345
pass_lead_jet_pt:		40.9221
pass_sublead_jet_pt:		32.8623
pass_jj_dphi:		27.4541
pass_opp_emisph:		27.0165
pass_jj_deta:		9.03044
pass_jj_mass:		8.01547
pass_met_tst_nolep_et:		4.70049
pass_met_tst_nolep_j1_dphi:		4.70049
pass_met_tst_nolep_j2_dphi:		4.70049
pass_met_cst_jet:		4.69415

==== CRZmm cutflow ====
passLepTrig:		36935.7
0_el_2_mu:		1889.11
mu_pt:		1852.97
opp_charge:		1798.18
mm_mass:		567.907
pass_n_jet:		54.8261
pass_lead_jet_pt:		48.4263
pass_sublead_jet_pt:		38.0918
pass_jj_dphi:		32.3159
pass_opp_emisph:		31.7572
pass_jj_deta:		11.1037
pass_jj_mass:		9.42503
pass_met_tst_nolep_et:		4.81576
pass_met_tst_nolep_j1_dphi:		4.81576
pass_met_tst_nolep_j2_dphi:		4.81576
pass_met_cst_jet:		4.81576

==== CRWeLowMetSig cutflow ====
passLepTrig:		36935.7
1_el_0_mu:		14408.6
el_pt:		13368.8
pass_n_jet:		926.001
pass_lead_jet_pt:		755.89
pass_sublead_jet_pt:		547.892
pass_jj_dphi:		391.76
pass_opp_emisph:		382.026
pass_jj_deta:		130.755
pass_jj_mass:		116.007
pass_met_tst_nolep_et:		58.6064
pass_met_tst_nolep_j1_dphi:		58.4197
pass_met_tst_nolep_j2_dphi:		58.4197
pass_met_cst_jet:		57.1357
low_met_significance:		11.0882

