====  summary of selection results  ====
      sample: ttbarJET_GroupedNP_3__1up
========================================

==== summary of evts. in all regions ====
SR:	36.7209
CRWe:	45.8013
CRWm:	50.8304
CRZee:	4.47509
CRZmm:	4.55349
CRWeLowMetSig:	10.5122

==== events in charge-split regions ====
CRWep:	27.5698
CRWen:	18.2315
CRWmp:	29.9422
CRWmn:	20.8882
CRWepLowMetSig:	4.81119
CRWenLowMetSig:	5.70098

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	18.9086
SR_n_evts_bin2:	11.4585
SR_n_evts_bin3:	6.35388
== total:	 36.7209

CRZee_n_evts_bin1:	1.92713
CRZee_n_evts_bin2:	1.55282
CRZee_n_evts_bin3:	0.995137
== total:	 4.47509

CRZmm_n_evts_bin1:	2.23654
CRZmm_n_evts_bin2:	1.51742
CRZmm_n_evts_bin3:	0.799522
== total:	 4.55349

CRWep_n_evts_bin1:	13.0473
CRWep_n_evts_bin2:	8.91663
CRWep_n_evts_bin3:	5.60585
== total:	 27.5698

CRWen_n_evts_bin1:	8.63473
CRWen_n_evts_bin2:	4.8074
CRWen_n_evts_bin3:	4.78942
== total:	 18.2315

CRWmp_n_evts_bin1:	15.1307
CRWmp_n_evts_bin2:	10.6186
CRWmp_n_evts_bin3:	4.19293
== total:	 29.9422

CRWmn_n_evts_bin1:	11.3428
CRWmn_n_evts_bin2:	6.63732
CRWmn_n_evts_bin3:	2.90805
== total:	 20.8882

CRWepLowMetSig_n_evts_bin1:	3.00642
CRWepLowMetSig_n_evts_bin2:	1.14101
CRWepLowMetSig_n_evts_bin3:	0.66376
== total:	 4.81119

CRWenLoeMetSig_n_evts_bin1:	3.17833
CRWenLoeMetSig_n_evts_bin2:	1.04669
CRWenLoeMetSig_n_evts_bin3:	1.47596
== total:	 5.70098

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	12.3709
SR_n_evts_bin2:	8.7951
SR_n_evts_bin3:	3.89593
SR_n_evts_bin4:	6.5377
SR_n_evts_bin5:	2.66339
SR_n_evts_bin6:	2.45795
SR_n_evts_bin7:	0
== total:	 36.7209

CRZee_n_evts_bin1:	1.70084
CRZee_n_evts_bin2:	1.02871
CRZee_n_evts_bin3:	0.503422
CRZee_n_evts_bin4:	0.226292
CRZee_n_evts_bin5:	0.524109
CRZee_n_evts_bin6:	0.491715
CRZee_n_evts_bin7:	0
== total:	 4.47509

CRZmm_n_evts_bin1:	1.57089
CRZmm_n_evts_bin2:	1.17107
CRZmm_n_evts_bin3:	0.449093
CRZmm_n_evts_bin4:	0.665651
CRZmm_n_evts_bin5:	0.346349
CRZmm_n_evts_bin6:	0.350429
CRZmm_n_evts_bin7:	0
== total:	 4.55349

CRWep_n_evts_bin1:	8.28895
CRWep_n_evts_bin2:	6.22591
CRWep_n_evts_bin3:	4.24
CRWep_n_evts_bin4:	4.75837
CRWep_n_evts_bin5:	2.69072
CRWep_n_evts_bin6:	1.36586
CRWep_n_evts_bin7:	0
== total:	 27.5698

CRWen_n_evts_bin1:	6.18684
CRWen_n_evts_bin2:	3.5062
CRWen_n_evts_bin3:	2.34076
CRWen_n_evts_bin4:	2.44789
CRWen_n_evts_bin5:	1.30119
CRWen_n_evts_bin6:	2.44866
CRWen_n_evts_bin7:	0
== total:	 18.2315

CRWmp_n_evts_bin1:	10.8594
CRWmp_n_evts_bin2:	5.48587
CRWmp_n_evts_bin3:	2.09122
CRWmp_n_evts_bin4:	4.27131
CRWmp_n_evts_bin5:	5.13273
CRWmp_n_evts_bin6:	2.10171
CRWmp_n_evts_bin7:	0
== total:	 29.9422

CRWmn_n_evts_bin1:	7.90389
CRWmn_n_evts_bin2:	4.28575
CRWmn_n_evts_bin3:	2.00281
CRWmn_n_evts_bin4:	3.43895
CRWmn_n_evts_bin5:	2.35157
CRWmn_n_evts_bin6:	0.905242
CRWmn_n_evts_bin7:	0
== total:	 20.8882

CRWepLowMetSig_n_evts_bin1:	1.62082
CRWepLowMetSig_n_evts_bin2:	0.667821
CRWepLowMetSig_n_evts_bin3:	0.0648933
CRWepLowMetSig_n_evts_bin4:	1.3856
CRWepLowMetSig_n_evts_bin5:	0.473193
CRWepLowMetSig_n_evts_bin6:	0.598867
CRWepLowMetSig_n_evts_bin7:	0
== total:	 4.81119

CRWenLoeMetSig_n_evts_bin1:	1.47703
CRWenLoeMetSig_n_evts_bin2:	0.75793
CRWenLoeMetSig_n_evts_bin3:	0.818147
CRWenLoeMetSig_n_evts_bin4:	1.7013
CRWenLoeMetSig_n_evts_bin5:	0.28876
CRWenLoeMetSig_n_evts_bin6:	0.657815
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 5.70098

==== SR cutflow ====
passMetTrig:		43684.3
0_el_0_mu:		16861.2
pass_n_jet:		991.526
pass_lead_jet_pt:		843.719
pass_sublead_jet_pt:		561.318
pass_jj_dphi:		358.866
pass_opp_emisph:		339.517
pass_jj_deta:		116.185
pass_jj_mass:		101.665
pass_met_tst_et:		38.4386
pass_met_tst_j1_dphi:		38.3018
pass_met_tst_j2_dphi:		38.3018
pass_met_cst_jet:		36.7209

==== CRWe cutflow ====
passLepTrig:		36938.8
1_el_0_mu:		14411.7
el_pt:		13372
pass_n_jet:		919.303
pass_lead_jet_pt:		749.541
pass_sublead_jet_pt:		542.849
pass_jj_dphi:		389.042
pass_opp_emisph:		379.697
pass_jj_deta:		129.254
pass_jj_mass:		114.159
pass_met_tst_nolep_et:		57.3511
pass_met_tst_nolep_j1_dphi:		57.1644
pass_met_tst_nolep_j2_dphi:		57.1644
pass_met_cst_jet:		56.3135
met_significance:		45.8013

==== CRWm cutflow ====
passLepTrig:		36938.8
0_el_1_mu:		11558.7
mu_pt:		11067.4
pass_n_jet:		773.037
pass_lead_jet_pt:		634.786
pass_sublead_jet_pt:		452.588
pass_jj_dphi:		318.634
pass_opp_emisph:		307.986
pass_jj_deta:		102.941
pass_jj_mass:		90.3116
pass_met_tst_nolep_et:		51.7833
pass_met_tst_nolep_j1_dphi:		51.7833
pass_met_tst_nolep_j2_dphi:		51.6475
pass_met_cst_jet:		50.8304

==== CRZee cutflow ====
passLepTrig:		36938.8
2_el_0_mu:		1645.35
el_pt:		1580.91
opp_charge:		1478.06
ee_mass:		460.34
pass_n_jet:		47.3158
pass_lead_jet_pt:		40.2877
pass_sublead_jet_pt:		32.2411
pass_jj_dphi:		26.9327
pass_opp_emisph:		26.4951
pass_jj_deta:		8.74553
pass_jj_mass:		7.79323
pass_met_tst_nolep_et:		4.48143
pass_met_tst_nolep_j1_dphi:		4.48143
pass_met_tst_nolep_j2_dphi:		4.48143
pass_met_cst_jet:		4.47509

==== CRZmm cutflow ====
passLepTrig:		36938.8
0_el_2_mu:		1888.3
mu_pt:		1851.98
opp_charge:		1797.51
mm_mass:		567.871
pass_n_jet:		54.3287
pass_lead_jet_pt:		47.942
pass_sublead_jet_pt:		37.6487
pass_jj_dphi:		31.8402
pass_opp_emisph:		31.286
pass_jj_deta:		10.8074
pass_jj_mass:		9.12877
pass_met_tst_nolep_et:		4.55349
pass_met_tst_nolep_j1_dphi:		4.55349
pass_met_tst_nolep_j2_dphi:		4.55349
pass_met_cst_jet:		4.55349

==== CRWeLowMetSig cutflow ====
passLepTrig:		36938.8
1_el_0_mu:		14411.7
el_pt:		13372
pass_n_jet:		919.303
pass_lead_jet_pt:		749.541
pass_sublead_jet_pt:		542.849
pass_jj_dphi:		389.042
pass_opp_emisph:		379.697
pass_jj_deta:		129.254
pass_jj_mass:		114.159
pass_met_tst_nolep_et:		57.3511
pass_met_tst_nolep_j1_dphi:		57.1644
pass_met_tst_nolep_j2_dphi:		57.1644
pass_met_cst_jet:		56.3135
low_met_significance:		10.5122

