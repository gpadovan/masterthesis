====  summary of selection results  ====
      sample: Z_strongJET_JER_EffectiveNP_5__1up
========================================

==== summary of evts. in all regions ====
SR:	1208.85
CRWe:	6.17335
CRWm:	36.9257
CRZee:	87.18
CRZmm:	106.327
CRWeLowMetSig:	43.8973

==== events in charge-split regions ====
CRWep:	2.1362
CRWen:	4.03715
CRWmp:	18.6563
CRWmn:	18.2694
CRWepLowMetSig:	23.2102
CRWenLowMetSig:	20.6871

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	554.742
SR_n_evts_bin2:	386.559
SR_n_evts_bin3:	267.556
== total:	 1208.86

CRZee_n_evts_bin1:	39.0808
CRZee_n_evts_bin2:	29.9335
CRZee_n_evts_bin3:	18.1658
== total:	 87.1801

CRZmm_n_evts_bin1:	52.1483
CRZmm_n_evts_bin2:	27.892
CRZmm_n_evts_bin3:	26.2868
== total:	 106.327

CRWep_n_evts_bin1:	1.15487
CRWep_n_evts_bin2:	0.666062
CRWep_n_evts_bin3:	0.315268
== total:	 2.1362

CRWen_n_evts_bin1:	1.79921
CRWen_n_evts_bin2:	1.64529
CRWen_n_evts_bin3:	0.592641
== total:	 4.03715

CRWmp_n_evts_bin1:	9.05346
CRWmp_n_evts_bin2:	4.33261
CRWmp_n_evts_bin3:	5.2702
== total:	 18.6563

CRWmn_n_evts_bin1:	10.2653
CRWmn_n_evts_bin2:	4.46347
CRWmn_n_evts_bin3:	3.54064
== total:	 18.2694

CRWepLowMetSig_n_evts_bin1:	10.164
CRWepLowMetSig_n_evts_bin2:	6.0141
CRWepLowMetSig_n_evts_bin3:	7.03209
== total:	 23.2102

CRWenLoeMetSig_n_evts_bin1:	11.3093
CRWenLoeMetSig_n_evts_bin2:	6.49588
CRWenLoeMetSig_n_evts_bin3:	2.8819
== total:	 20.6871

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	381.623
SR_n_evts_bin2:	207.297
SR_n_evts_bin3:	130.679
SR_n_evts_bin4:	173.119
SR_n_evts_bin5:	179.262
SR_n_evts_bin6:	136.877
SR_n_evts_bin7:	0
== total:	 1208.86

CRZee_n_evts_bin1:	24.7498
CRZee_n_evts_bin2:	15.2811
CRZee_n_evts_bin3:	9.60473
CRZee_n_evts_bin4:	14.331
CRZee_n_evts_bin5:	14.6524
CRZee_n_evts_bin6:	8.56104
CRZee_n_evts_bin7:	0
== total:	 87.1801

CRZmm_n_evts_bin1:	37.1567
CRZmm_n_evts_bin2:	14.368
CRZmm_n_evts_bin3:	13.9974
CRZmm_n_evts_bin4:	14.9917
CRZmm_n_evts_bin5:	13.524
CRZmm_n_evts_bin6:	12.2894
CRZmm_n_evts_bin7:	0
== total:	 106.327

CRWep_n_evts_bin1:	0.252069
CRWep_n_evts_bin2:	0.323533
CRWep_n_evts_bin3:	-0.169528
CRWep_n_evts_bin4:	0.902801
CRWep_n_evts_bin5:	0.34253
CRWep_n_evts_bin6:	0.484796
CRWep_n_evts_bin7:	0
== total:	 2.1362

CRWen_n_evts_bin1:	0.911503
CRWen_n_evts_bin2:	0.562323
CRWen_n_evts_bin3:	0.252532
CRWen_n_evts_bin4:	0.88771
CRWen_n_evts_bin5:	1.08297
CRWen_n_evts_bin6:	0.340109
CRWen_n_evts_bin7:	0
== total:	 4.03715

CRWmp_n_evts_bin1:	6.07463
CRWmp_n_evts_bin2:	0.663107
CRWmp_n_evts_bin3:	1.16699
CRWmp_n_evts_bin4:	2.97882
CRWmp_n_evts_bin5:	3.66951
CRWmp_n_evts_bin6:	4.10321
CRWmp_n_evts_bin7:	0
== total:	 18.6563

CRWmn_n_evts_bin1:	5.50465
CRWmn_n_evts_bin2:	3.56055
CRWmn_n_evts_bin3:	2.27236
CRWmn_n_evts_bin4:	4.76066
CRWmn_n_evts_bin5:	0.902922
CRWmn_n_evts_bin6:	1.26829
CRWmn_n_evts_bin7:	0
== total:	 18.2694

CRWepLowMetSig_n_evts_bin1:	5.19993
CRWepLowMetSig_n_evts_bin2:	3.9353
CRWepLowMetSig_n_evts_bin3:	3.2912
CRWepLowMetSig_n_evts_bin4:	4.96407
CRWepLowMetSig_n_evts_bin5:	2.0788
CRWepLowMetSig_n_evts_bin6:	3.74089
CRWepLowMetSig_n_evts_bin7:	0
== total:	 23.2102

CRWenLoeMetSig_n_evts_bin1:	6.45932
CRWenLoeMetSig_n_evts_bin2:	4.07492
CRWenLoeMetSig_n_evts_bin3:	-0.363604
CRWenLoeMetSig_n_evts_bin4:	4.84999
CRWenLoeMetSig_n_evts_bin5:	2.42097
CRWenLoeMetSig_n_evts_bin6:	3.2455
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 20.6871

==== SR cutflow ====
passMetTrig:		37352.1
0_el_0_mu:		31366.4
pass_n_jet:		7493.31
pass_lead_jet_pt:		7345.48
pass_sublead_jet_pt:		6783.26
pass_jj_dphi:		5863.46
pass_opp_emisph:		5849.93
pass_jj_deta:		2688.31
pass_jj_mass:		2381.7
pass_met_tst_et:		1248.55
pass_met_tst_j1_dphi:		1248.09
pass_met_tst_j2_dphi:		1248.05
pass_met_cst_jet:		1208.85

==== CRWe cutflow ====
passLepTrig:		14730.5
1_el_0_mu:		5331.45
el_pt:		5084.15
pass_n_jet:		1649.53
pass_lead_jet_pt:		1250.42
pass_sublead_jet_pt:		633.799
pass_jj_dphi:		420.467
pass_opp_emisph:		400.449
pass_jj_deta:		110.705
pass_jj_mass:		100.712
pass_met_tst_nolep_et:		50.6826
pass_met_tst_nolep_j1_dphi:		50.6826
pass_met_tst_nolep_j2_dphi:		50.6826
pass_met_cst_jet:		50.0706
met_significance:		6.17335

==== CRWm cutflow ====
passLepTrig:		14730.5
0_el_1_mu:		1470.06
mu_pt:		1408.15
pass_n_jet:		316.08
pass_lead_jet_pt:		280.197
pass_sublead_jet_pt:		243.391
pass_jj_dphi:		198.32
pass_opp_emisph:		195.752
pass_jj_deta:		84.245
pass_jj_mass:		71.8904
pass_met_tst_nolep_et:		38.1116
pass_met_tst_nolep_j1_dphi:		38.1116
pass_met_tst_nolep_j2_dphi:		38.1116
pass_met_cst_jet:		36.9257

==== CRZee cutflow ====
passLepTrig:		14730.5
2_el_0_mu:		2647.79
el_pt:		2637.52
opp_charge:		2555.24
ee_mass:		2137.1
pass_n_jet:		557.524
pass_lead_jet_pt:		532.311
pass_sublead_jet_pt:		481.54
pass_jj_dphi:		416.723
pass_opp_emisph:		415.689
pass_jj_deta:		175.237
pass_jj_mass:		154.075
pass_met_tst_nolep_et:		88.2313
pass_met_tst_nolep_j1_dphi:		88.2098
pass_met_tst_nolep_j2_dphi:		88.2098
pass_met_cst_jet:		87.18

==== CRZmm cutflow ====
passLepTrig:		14730.5
0_el_2_mu:		3443.29
mu_pt:		3433.38
opp_charge:		3432
mm_mass:		2827.59
pass_n_jet:		732.671
pass_lead_jet_pt:		724.738
pass_sublead_jet_pt:		655.514
pass_jj_dphi:		565.851
pass_opp_emisph:		566.109
pass_jj_deta:		244.808
pass_jj_mass:		215.999
pass_met_tst_nolep_et:		107.436
pass_met_tst_nolep_j1_dphi:		107.436
pass_met_tst_nolep_j2_dphi:		107.436
pass_met_cst_jet:		106.327

==== CRWeLowMetSig cutflow ====
passLepTrig:		14730.5
1_el_0_mu:		5331.45
el_pt:		5084.15
pass_n_jet:		1649.53
pass_lead_jet_pt:		1250.42
pass_sublead_jet_pt:		633.799
pass_jj_dphi:		420.467
pass_opp_emisph:		400.449
pass_jj_deta:		110.705
pass_jj_mass:		100.712
pass_met_tst_nolep_et:		50.6826
pass_met_tst_nolep_j1_dphi:		50.6826
pass_met_tst_nolep_j2_dphi:		50.6826
pass_met_cst_jet:		50.0706
low_met_significance:		43.8973

