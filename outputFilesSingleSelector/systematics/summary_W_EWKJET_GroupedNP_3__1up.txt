====  summary of selection results  ====
      sample: W_EWKJET_GroupedNP_3__1up
========================================

==== summary of evts. in all regions ====
SR:	172.144
CRWe:	160.024
CRWm:	211.256
CRZee:	0
CRZmm:	0
CRWeLowMetSig:	104.399

==== events in charge-split regions ====
CRWep:	106.754
CRWen:	53.27
CRWmp:	133.731
CRWmn:	77.5249
CRWepLowMetSig:	58.7701
CRWenLowMetSig:	45.6284

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	33.2381
SR_n_evts_bin2:	51.329
SR_n_evts_bin3:	87.5772
== total:	 172.144

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
== total:	 0

CRWep_n_evts_bin1:	22.9861
CRWep_n_evts_bin2:	30.8863
CRWep_n_evts_bin3:	52.8812
== total:	 106.754

CRWen_n_evts_bin1:	11.045
CRWen_n_evts_bin2:	19.5054
CRWen_n_evts_bin3:	22.7196
== total:	 53.27

CRWmp_n_evts_bin1:	29.3932
CRWmp_n_evts_bin2:	38.6802
CRWmp_n_evts_bin3:	65.6574
== total:	 133.731

CRWmn_n_evts_bin1:	15.5772
CRWmn_n_evts_bin2:	27.5644
CRWmn_n_evts_bin3:	34.3833
== total:	 77.5249

CRWepLowMetSig_n_evts_bin1:	11.1294
CRWepLowMetSig_n_evts_bin2:	18.7824
CRWepLowMetSig_n_evts_bin3:	28.8584
== total:	 58.7701

CRWenLoeMetSig_n_evts_bin1:	13.1922
CRWenLoeMetSig_n_evts_bin2:	14.8125
CRWenLoeMetSig_n_evts_bin3:	17.6238
== total:	 45.6284

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	19.037
SR_n_evts_bin2:	24.8348
SR_n_evts_bin3:	37.4968
SR_n_evts_bin4:	14.2011
SR_n_evts_bin5:	26.4942
SR_n_evts_bin6:	50.0804
SR_n_evts_bin7:	0
== total:	 172.144

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	0
CRZee_n_evts_bin6:	0
CRZee_n_evts_bin7:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0

CRWep_n_evts_bin1:	16.0137
CRWep_n_evts_bin2:	17.0333
CRWep_n_evts_bin3:	23.1492
CRWep_n_evts_bin4:	6.9724
CRWep_n_evts_bin5:	13.853
CRWep_n_evts_bin6:	29.732
CRWep_n_evts_bin7:	0
== total:	 106.754

CRWen_n_evts_bin1:	7.99361
CRWen_n_evts_bin2:	9.53368
CRWen_n_evts_bin3:	10.673
CRWen_n_evts_bin4:	3.05143
CRWen_n_evts_bin5:	9.97168
CRWen_n_evts_bin6:	12.0466
CRWen_n_evts_bin7:	0
== total:	 53.27

CRWmp_n_evts_bin1:	18.3997
CRWmp_n_evts_bin2:	19.8113
CRWmp_n_evts_bin3:	26.7726
CRWmp_n_evts_bin4:	10.9936
CRWmp_n_evts_bin5:	18.8689
CRWmp_n_evts_bin6:	38.8848
CRWmp_n_evts_bin7:	0
== total:	 133.731

CRWmn_n_evts_bin1:	8.67848
CRWmn_n_evts_bin2:	14.8019
CRWmn_n_evts_bin3:	15.6486
CRWmn_n_evts_bin4:	6.89869
CRWmn_n_evts_bin5:	12.7625
CRWmn_n_evts_bin6:	18.7347
CRWmn_n_evts_bin7:	0
== total:	 77.5249

CRWepLowMetSig_n_evts_bin1:	5.7522
CRWepLowMetSig_n_evts_bin2:	8.16971
CRWepLowMetSig_n_evts_bin3:	13.1225
CRWepLowMetSig_n_evts_bin4:	5.37716
CRWepLowMetSig_n_evts_bin5:	10.6127
CRWepLowMetSig_n_evts_bin6:	15.7359
CRWepLowMetSig_n_evts_bin7:	0
== total:	 58.7701

CRWenLoeMetSig_n_evts_bin1:	8.22898
CRWenLoeMetSig_n_evts_bin2:	5.94636
CRWenLoeMetSig_n_evts_bin3:	8.35865
CRWenLoeMetSig_n_evts_bin4:	4.96321
CRWenLoeMetSig_n_evts_bin5:	8.8661
CRWenLoeMetSig_n_evts_bin6:	9.26514
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 45.6284

==== SR cutflow ====
passMetTrig:		4783.38
0_el_0_mu:		2086.28
pass_n_jet:		886.175
pass_lead_jet_pt:		878.801
pass_sublead_jet_pt:		834.044
pass_jj_dphi:		651.113
pass_opp_emisph:		647.384
pass_jj_deta:		341.79
pass_jj_mass:		335.277
pass_met_tst_et:		174.6
pass_met_tst_j1_dphi:		174.6
pass_met_tst_j2_dphi:		174.6
pass_met_cst_jet:		172.144

==== CRWe cutflow ====
passLepTrig:		3580.98
1_el_0_mu:		1745.95
el_pt:		1699.77
pass_n_jet:		961.407
pass_lead_jet_pt:		958.768
pass_sublead_jet_pt:		950.528
pass_jj_dphi:		787.958
pass_opp_emisph:		787.958
pass_jj_deta:		388.405
pass_jj_mass:		378.671
pass_met_tst_nolep_et:		265.881
pass_met_tst_nolep_j1_dphi:		265.881
pass_met_tst_nolep_j2_dphi:		265.881
pass_met_cst_jet:		264.422
met_significance:		160.024

==== CRWm cutflow ====
passLepTrig:		3580.98
0_el_1_mu:		1465.33
mu_pt:		1426.55
pass_n_jet:		815.331
pass_lead_jet_pt:		814.008
pass_sublead_jet_pt:		809.393
pass_jj_dphi:		663.617
pass_opp_emisph:		663.522
pass_jj_deta:		319.878
pass_jj_mass:		310.122
pass_met_tst_nolep_et:		212.606
pass_met_tst_nolep_j1_dphi:		212.606
pass_met_tst_nolep_j2_dphi:		212.606
pass_met_cst_jet:		211.256

==== CRZee cutflow ====
passLepTrig:		3580.98
2_el_0_mu:		1.04501
el_pt:		1.04501
opp_charge:		0.658755
ee_mass:		0.0902424
pass_n_jet:		0.0902424
pass_lead_jet_pt:		0.0902424
pass_sublead_jet_pt:		0.0902424
pass_jj_dphi:		0.0902424
pass_opp_emisph:		0.0902424
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZmm cutflow ====
passLepTrig:		3580.98
0_el_2_mu:		0.526593
mu_pt:		0.526593
opp_charge:		0.29906
mm_mass:		0.0783653
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRWeLowMetSig cutflow ====
passLepTrig:		3580.98
1_el_0_mu:		1745.95
el_pt:		1699.77
pass_n_jet:		961.407
pass_lead_jet_pt:		958.768
pass_sublead_jet_pt:		950.528
pass_jj_dphi:		787.958
pass_opp_emisph:		787.958
pass_jj_deta:		388.405
pass_jj_mass:		378.671
pass_met_tst_nolep_et:		265.881
pass_met_tst_nolep_j1_dphi:		265.881
pass_met_tst_nolep_j2_dphi:		265.881
pass_met_cst_jet:		264.422
low_met_significance:		104.399

