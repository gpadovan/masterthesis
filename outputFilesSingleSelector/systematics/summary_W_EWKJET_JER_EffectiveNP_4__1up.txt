====  summary of selection results  ====
      sample: W_EWKJET_JER_EffectiveNP_4__1up
========================================

==== summary of evts. in all regions ====
SR:	170.393
CRWe:	155.017
CRWm:	205
CRZee:	0
CRZmm:	0
CRWeLowMetSig:	103.772

==== events in charge-split regions ====
CRWep:	102.353
CRWen:	52.6641
CRWmp:	129.369
CRWmn:	75.6315
CRWepLowMetSig:	58.7631
CRWenLowMetSig:	45.0084

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	34.418
SR_n_evts_bin2:	50.0203
SR_n_evts_bin3:	85.9549
== total:	 170.393

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
== total:	 0

CRWep_n_evts_bin1:	22.6407
CRWep_n_evts_bin2:	28.1538
CRWep_n_evts_bin3:	51.5582
== total:	 102.353

CRWen_n_evts_bin1:	11.768
CRWen_n_evts_bin2:	18.2534
CRWen_n_evts_bin3:	22.6428
== total:	 52.6641

CRWmp_n_evts_bin1:	27.5448
CRWmp_n_evts_bin2:	35.8722
CRWmp_n_evts_bin3:	65.9517
== total:	 129.369

CRWmn_n_evts_bin1:	15.4166
CRWmn_n_evts_bin2:	26.9777
CRWmn_n_evts_bin3:	33.2371
== total:	 75.6314

CRWepLowMetSig_n_evts_bin1:	11.333
CRWepLowMetSig_n_evts_bin2:	17.9623
CRWepLowMetSig_n_evts_bin3:	29.4679
== total:	 58.7631

CRWenLoeMetSig_n_evts_bin1:	13.2084
CRWenLoeMetSig_n_evts_bin2:	14.1231
CRWenLoeMetSig_n_evts_bin3:	17.6769
== total:	 45.0085

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	20.6342
SR_n_evts_bin2:	24.2749
SR_n_evts_bin3:	36.2354
SR_n_evts_bin4:	13.7838
SR_n_evts_bin5:	25.7454
SR_n_evts_bin6:	49.7194
SR_n_evts_bin7:	0
== total:	 170.393

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	0
CRZee_n_evts_bin6:	0
CRZee_n_evts_bin7:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0

CRWep_n_evts_bin1:	16.3079
CRWep_n_evts_bin2:	15.8652
CRWep_n_evts_bin3:	22.7599
CRWep_n_evts_bin4:	6.33279
CRWep_n_evts_bin5:	12.2886
CRWep_n_evts_bin6:	28.7983
CRWep_n_evts_bin7:	0
== total:	 102.353

CRWen_n_evts_bin1:	8.10708
CRWen_n_evts_bin2:	9.10198
CRWen_n_evts_bin3:	11.2812
CRWen_n_evts_bin4:	3.66089
CRWen_n_evts_bin5:	9.15139
CRWen_n_evts_bin6:	11.3616
CRWen_n_evts_bin7:	0
== total:	 52.6641

CRWmp_n_evts_bin1:	17.722
CRWmp_n_evts_bin2:	17.8916
CRWmp_n_evts_bin3:	25.6343
CRWmp_n_evts_bin4:	9.82282
CRWmp_n_evts_bin5:	17.9805
CRWmp_n_evts_bin6:	40.3174
CRWmp_n_evts_bin7:	0
== total:	 129.369

CRWmn_n_evts_bin1:	9.30731
CRWmn_n_evts_bin2:	14.6886
CRWmn_n_evts_bin3:	15.0664
CRWmn_n_evts_bin4:	6.1093
CRWmn_n_evts_bin5:	12.2891
CRWmn_n_evts_bin6:	18.1706
CRWmn_n_evts_bin7:	0
== total:	 75.6314

CRWepLowMetSig_n_evts_bin1:	5.89429
CRWepLowMetSig_n_evts_bin2:	8.27514
CRWepLowMetSig_n_evts_bin3:	13.6462
CRWepLowMetSig_n_evts_bin4:	5.43871
CRWepLowMetSig_n_evts_bin5:	9.68714
CRWepLowMetSig_n_evts_bin6:	15.8217
CRWepLowMetSig_n_evts_bin7:	0
== total:	 58.7631

CRWenLoeMetSig_n_evts_bin1:	8.24937
CRWenLoeMetSig_n_evts_bin2:	5.4029
CRWenLoeMetSig_n_evts_bin3:	8.11989
CRWenLoeMetSig_n_evts_bin4:	4.95901
CRWenLoeMetSig_n_evts_bin5:	8.72024
CRWenLoeMetSig_n_evts_bin6:	9.55704
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 45.0085

==== SR cutflow ====
passMetTrig:		4768.61
0_el_0_mu:		2075.64
pass_n_jet:		877.992
pass_lead_jet_pt:		870.742
pass_sublead_jet_pt:		824.925
pass_jj_dphi:		645.886
pass_opp_emisph:		642.111
pass_jj_deta:		338.877
pass_jj_mass:		332.883
pass_met_tst_et:		171.681
pass_met_tst_j1_dphi:		171.681
pass_met_tst_j2_dphi:		171.681
pass_met_cst_jet:		170.393

==== CRWe cutflow ====
passLepTrig:		3573.22
1_el_0_mu:		1738.31
el_pt:		1692.18
pass_n_jet:		952.737
pass_lead_jet_pt:		949.85
pass_sublead_jet_pt:		941.709
pass_jj_dphi:		780.177
pass_opp_emisph:		780.177
pass_jj_deta:		385.711
pass_jj_mass:		376.804
pass_met_tst_nolep_et:		260.506
pass_met_tst_nolep_j1_dphi:		260.506
pass_met_tst_nolep_j2_dphi:		260.506
pass_met_cst_jet:		258.788
met_significance:		155.017

==== CRWm cutflow ====
passLepTrig:		3573.22
0_el_1_mu:		1464.83
mu_pt:		1425.11
pass_n_jet:		804.569
pass_lead_jet_pt:		803.123
pass_sublead_jet_pt:		798.162
pass_jj_dphi:		651.607
pass_opp_emisph:		651.513
pass_jj_deta:		315
pass_jj_mass:		305.116
pass_met_tst_nolep_et:		206.636
pass_met_tst_nolep_j1_dphi:		206.636
pass_met_tst_nolep_j2_dphi:		206.636
pass_met_cst_jet:		205

==== CRZee cutflow ====
passLepTrig:		3573.22
2_el_0_mu:		1.03722
el_pt:		1.03722
opp_charge:		0.658755
ee_mass:		0.0902424
pass_n_jet:		0.0902424
pass_lead_jet_pt:		0.0902424
pass_sublead_jet_pt:		0.0902424
pass_jj_dphi:		0.0902424
pass_opp_emisph:		0.0902424
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZmm cutflow ====
passLepTrig:		3573.22
0_el_2_mu:		0.44496
mu_pt:		0.44496
opp_charge:		0.363589
mm_mass:		0.142894
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRWeLowMetSig cutflow ====
passLepTrig:		3573.22
1_el_0_mu:		1738.31
el_pt:		1692.18
pass_n_jet:		952.737
pass_lead_jet_pt:		949.85
pass_sublead_jet_pt:		941.709
pass_jj_dphi:		780.177
pass_opp_emisph:		780.177
pass_jj_deta:		385.711
pass_jj_mass:		376.804
pass_met_tst_nolep_et:		260.506
pass_met_tst_nolep_j1_dphi:		260.506
pass_met_tst_nolep_j2_dphi:		260.506
pass_met_cst_jet:		258.788
low_met_significance:		103.772

