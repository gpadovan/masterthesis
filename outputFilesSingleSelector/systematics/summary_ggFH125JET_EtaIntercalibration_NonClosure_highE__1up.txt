====  summary of selection results  ====
      sample: ggFH125JET_EtaIntercalibration_NonClosure_highE__1up
========================================

==== summary of evts. in all regions ====
SR:	130.39
CRWe:	0
CRWm:	0
CRZee:	0
CRZmm:	0
CRWeLowMetSig:	0

==== events in charge-split regions ====
CRWep:	0
CRWen:	0
CRWmp:	0
CRWmn:	0
CRWepLowMetSig:	0
CRWenLowMetSig:	0

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	55.1898
SR_n_evts_bin2:	11.5791
SR_n_evts_bin3:	63.6207
== total:	 130.39

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
== total:	 0

CRWep_n_evts_bin1:	0
CRWep_n_evts_bin2:	0
CRWep_n_evts_bin3:	0
== total:	 0

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0
CRWen_n_evts_bin3:	0
== total:	 0

CRWmp_n_evts_bin1:	0
CRWmp_n_evts_bin2:	0
CRWmp_n_evts_bin3:	0
== total:	 0

CRWmn_n_evts_bin1:	0
CRWmn_n_evts_bin2:	0
CRWmn_n_evts_bin3:	0
== total:	 0

CRWepLowMetSig_n_evts_bin1:	0
CRWepLowMetSig_n_evts_bin2:	0
CRWepLowMetSig_n_evts_bin3:	0
== total:	 0

CRWenLoeMetSig_n_evts_bin1:	0
CRWenLoeMetSig_n_evts_bin2:	0
CRWenLoeMetSig_n_evts_bin3:	0
== total:	 0

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	49.0228
SR_n_evts_bin2:	8.28512
SR_n_evts_bin3:	40.2431
SR_n_evts_bin4:	6.16696
SR_n_evts_bin5:	3.29402
SR_n_evts_bin6:	23.3776
SR_n_evts_bin7:	0
== total:	 130.39

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	0
CRZee_n_evts_bin6:	0
CRZee_n_evts_bin7:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0

CRWep_n_evts_bin1:	0
CRWep_n_evts_bin2:	0
CRWep_n_evts_bin3:	0
CRWep_n_evts_bin4:	0
CRWep_n_evts_bin5:	0
CRWep_n_evts_bin6:	0
CRWep_n_evts_bin7:	0
== total:	 0

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0
CRWen_n_evts_bin3:	0
CRWen_n_evts_bin4:	0
CRWen_n_evts_bin5:	0
CRWen_n_evts_bin6:	0
CRWen_n_evts_bin7:	0
== total:	 0

CRWmp_n_evts_bin1:	0
CRWmp_n_evts_bin2:	0
CRWmp_n_evts_bin3:	0
CRWmp_n_evts_bin4:	0
CRWmp_n_evts_bin5:	0
CRWmp_n_evts_bin6:	0
CRWmp_n_evts_bin7:	0
== total:	 0

CRWmn_n_evts_bin1:	0
CRWmn_n_evts_bin2:	0
CRWmn_n_evts_bin3:	0
CRWmn_n_evts_bin4:	0
CRWmn_n_evts_bin5:	0
CRWmn_n_evts_bin6:	0
CRWmn_n_evts_bin7:	0
== total:	 0

CRWepLowMetSig_n_evts_bin1:	0
CRWepLowMetSig_n_evts_bin2:	0
CRWepLowMetSig_n_evts_bin3:	0
CRWepLowMetSig_n_evts_bin4:	0
CRWepLowMetSig_n_evts_bin5:	0
CRWepLowMetSig_n_evts_bin6:	0
CRWepLowMetSig_n_evts_bin7:	0
== total:	 0

CRWenLoeMetSig_n_evts_bin1:	0
CRWenLoeMetSig_n_evts_bin2:	0
CRWenLoeMetSig_n_evts_bin3:	0
CRWenLoeMetSig_n_evts_bin4:	0
CRWenLoeMetSig_n_evts_bin5:	0
CRWenLoeMetSig_n_evts_bin6:	0
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 0

==== SR cutflow ====
passMetTrig:		1500.8
0_el_0_mu:		1500.8
pass_n_jet:		477.101
pass_lead_jet_pt:		473.385
pass_sublead_jet_pt:		454.842
pass_jj_dphi:		440.838
pass_opp_emisph:		440.838
pass_jj_deta:		199.413
pass_jj_mass:		178.564
pass_met_tst_et:		130.39
pass_met_tst_j1_dphi:		130.39
pass_met_tst_j2_dphi:		130.39
pass_met_cst_jet:		130.39

==== CRWe cutflow ====
passLepTrig:		8.38602
1_el_0_mu:		0
el_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0
met_significance:		0

==== CRWm cutflow ====
passLepTrig:		8.38602
0_el_1_mu:		0
mu_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZee cutflow ====
passLepTrig:		8.38602
2_el_0_mu:		0
el_pt:		0
opp_charge:		0
ee_mass:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZmm cutflow ====
passLepTrig:		8.38602
0_el_2_mu:		0
mu_pt:		0
opp_charge:		0
mm_mass:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRWeLowMetSig cutflow ====
passLepTrig:		8.38602
1_el_0_mu:		0
el_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0
low_met_significance:		0

