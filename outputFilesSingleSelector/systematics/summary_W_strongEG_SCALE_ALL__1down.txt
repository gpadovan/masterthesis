====  summary of selection results  ====
      sample: W_strongEG_SCALE_ALL__1down
========================================

==== summary of evts. in all regions ====
SR:	929.812
CRWe:	549.664
CRWm:	887.785
CRZee:	0.134099
CRZmm:	0.182382
CRWeLowMetSig:	334.571

==== events in charge-split regions ====
CRWep:	353.84
CRWen:	195.824
CRWmp:	566.489
CRWmn:	321.296
CRWepLowMetSig:	171.06
CRWenLowMetSig:	163.512

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	404.53
SR_n_evts_bin2:	329.27
SR_n_evts_bin3:	196.013
== total:	 929.813

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	-0.00645132
CRZee_n_evts_bin3:	0.14055
== total:	 0.134099

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0.182382
CRZmm_n_evts_bin3:	0
== total:	 0.182382

CRWep_n_evts_bin1:	135.386
CRWep_n_evts_bin2:	130.202
CRWep_n_evts_bin3:	88.2516
== total:	 353.84

CRWen_n_evts_bin1:	94.0708
CRWen_n_evts_bin2:	60.8638
CRWen_n_evts_bin3:	40.8896
== total:	 195.824

CRWmp_n_evts_bin1:	248.817
CRWmp_n_evts_bin2:	192.566
CRWmp_n_evts_bin3:	125.106
== total:	 566.489

CRWmn_n_evts_bin1:	161.613
CRWmn_n_evts_bin2:	98.0553
CRWmn_n_evts_bin3:	61.6285
== total:	 321.296

CRWepLowMetSig_n_evts_bin1:	74.2658
CRWepLowMetSig_n_evts_bin2:	61.1379
CRWepLowMetSig_n_evts_bin3:	35.6556
== total:	 171.059

CRWenLoeMetSig_n_evts_bin1:	70.5051
CRWenLoeMetSig_n_evts_bin2:	59.5429
CRWenLoeMetSig_n_evts_bin3:	33.4636
== total:	 163.512

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	279.403
SR_n_evts_bin2:	190.863
SR_n_evts_bin3:	100.953
SR_n_evts_bin4:	125.127
SR_n_evts_bin5:	138.407
SR_n_evts_bin6:	95.0599
SR_n_evts_bin7:	0
== total:	 929.813

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0.0966545
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	-0.103106
CRZee_n_evts_bin6:	0.14055
CRZee_n_evts_bin7:	0
== total:	 0.134099

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0.095122
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0.08726
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0.182382

CRWep_n_evts_bin1:	90.4647
CRWep_n_evts_bin2:	68.7996
CRWep_n_evts_bin3:	56.0406
CRWep_n_evts_bin4:	44.9217
CRWep_n_evts_bin5:	61.4028
CRWep_n_evts_bin6:	32.211
CRWep_n_evts_bin7:	0
== total:	 353.84

CRWen_n_evts_bin1:	63.5441
CRWen_n_evts_bin2:	38.265
CRWen_n_evts_bin3:	22.6233
CRWen_n_evts_bin4:	30.5267
CRWen_n_evts_bin5:	22.5988
CRWen_n_evts_bin6:	18.2664
CRWen_n_evts_bin7:	0
== total:	 195.824

CRWmp_n_evts_bin1:	169.256
CRWmp_n_evts_bin2:	99.1806
CRWmp_n_evts_bin3:	57.9037
CRWmp_n_evts_bin4:	79.5617
CRWmp_n_evts_bin5:	93.3851
CRWmp_n_evts_bin6:	67.2021
CRWmp_n_evts_bin7:	0
== total:	 566.489

CRWmn_n_evts_bin1:	118.514
CRWmn_n_evts_bin2:	47.4341
CRWmn_n_evts_bin3:	32.9972
CRWmn_n_evts_bin4:	43.0986
CRWmn_n_evts_bin5:	50.6211
CRWmn_n_evts_bin6:	28.6313
CRWmn_n_evts_bin7:	0
== total:	 321.296

CRWepLowMetSig_n_evts_bin1:	50.2218
CRWepLowMetSig_n_evts_bin2:	30.87
CRWepLowMetSig_n_evts_bin3:	16.323
CRWepLowMetSig_n_evts_bin4:	24.0441
CRWepLowMetSig_n_evts_bin5:	30.268
CRWepLowMetSig_n_evts_bin6:	19.3327
CRWepLowMetSig_n_evts_bin7:	0
== total:	 171.059

CRWenLoeMetSig_n_evts_bin1:	51.2134
CRWenLoeMetSig_n_evts_bin2:	24.0106
CRWenLoeMetSig_n_evts_bin3:	18.179
CRWenLoeMetSig_n_evts_bin4:	19.2917
CRWenLoeMetSig_n_evts_bin5:	35.5323
CRWenLoeMetSig_n_evts_bin6:	15.2847
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 163.512

==== SR cutflow ====
passMetTrig:		82819.6
0_el_0_mu:		48281.2
pass_n_jet:		11534.7
pass_lead_jet_pt:		10710.5
pass_sublead_jet_pt:		7907.42
pass_jj_dphi:		6247.41
pass_opp_emisph:		6145.48
pass_jj_deta:		2840.24
pass_jj_mass:		2557.27
pass_met_tst_et:		958.99
pass_met_tst_j1_dphi:		958.836
pass_met_tst_j2_dphi:		958.747
pass_met_cst_jet:		929.812

==== CRWe cutflow ====
passLepTrig:		54323.3
1_el_0_mu:		22926.2
el_pt:		22281
pass_n_jet:		5789.63
pass_lead_jet_pt:		5609.88
pass_sublead_jet_pt:		5107.06
pass_jj_dphi:		4413.6
pass_opp_emisph:		4409.03
pass_jj_deta:		1924.75
pass_jj_mass:		1739.66
pass_met_tst_nolep_et:		900.96
pass_met_tst_nolep_j1_dphi:		900.96
pass_met_tst_nolep_j2_dphi:		900.96
pass_met_cst_jet:		884.236
met_significance:		549.664

==== CRWm cutflow ====
passLepTrig:		54323.3
0_el_1_mu:		20454.9
mu_pt:		19872.8
pass_n_jet:		5139.79
pass_lead_jet_pt:		5043.4
pass_sublead_jet_pt:		4592.19
pass_jj_dphi:		4016.63
pass_opp_emisph:		4011.13
pass_jj_deta:		1764.07
pass_jj_mass:		1589.65
pass_met_tst_nolep_et:		904.039
pass_met_tst_nolep_j1_dphi:		903.954
pass_met_tst_nolep_j2_dphi:		903.954
pass_met_cst_jet:		887.785

==== CRZee cutflow ====
passLepTrig:		54323.3
2_el_0_mu:		38.8254
el_pt:		36.0811
opp_charge:		17.9093
ee_mass:		5.6419
pass_n_jet:		0.909106
pass_lead_jet_pt:		0.909106
pass_sublead_jet_pt:		0.909106
pass_jj_dphi:		0.909106
pass_opp_emisph:		0.909106
pass_jj_deta:		0.180299
pass_jj_mass:		0.180299
pass_met_tst_nolep_et:		0.134099
pass_met_tst_nolep_j1_dphi:		0.134099
pass_met_tst_nolep_j2_dphi:		0.134099
pass_met_cst_jet:		0.134099

==== CRZmm cutflow ====
passLepTrig:		54323.3
0_el_2_mu:		33.4402
mu_pt:		32.7473
opp_charge:		19.1317
mm_mass:		4.01131
pass_n_jet:		1.25432
pass_lead_jet_pt:		1.25432
pass_sublead_jet_pt:		1.25432
pass_jj_dphi:		0.182382
pass_opp_emisph:		0.182382
pass_jj_deta:		0.182382
pass_jj_mass:		0.182382
pass_met_tst_nolep_et:		0.182382
pass_met_tst_nolep_j1_dphi:		0.182382
pass_met_tst_nolep_j2_dphi:		0.182382
pass_met_cst_jet:		0.182382

==== CRWeLowMetSig cutflow ====
passLepTrig:		54323.3
1_el_0_mu:		22926.2
el_pt:		22281
pass_n_jet:		5789.63
pass_lead_jet_pt:		5609.88
pass_sublead_jet_pt:		5107.06
pass_jj_dphi:		4413.6
pass_opp_emisph:		4409.03
pass_jj_deta:		1924.75
pass_jj_mass:		1739.66
pass_met_tst_nolep_et:		900.96
pass_met_tst_nolep_j1_dphi:		900.96
pass_met_tst_nolep_j2_dphi:		900.96
pass_met_cst_jet:		884.236
low_met_significance:		334.571

