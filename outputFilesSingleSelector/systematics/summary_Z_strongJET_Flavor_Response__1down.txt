====  summary of selection results  ====
      sample: Z_strongJET_Flavor_Response__1down
========================================

==== summary of evts. in all regions ====
SR:	1177.89
CRWe:	6.11251
CRWm:	37.0131
CRZee:	88.5215
CRZmm:	106.293
CRWeLowMetSig:	39.9647

==== events in charge-split regions ====
CRWep:	2.15658
CRWen:	3.95592
CRWmp:	17.7953
CRWmn:	19.2179
CRWepLowMetSig:	20.6941
CRWenLowMetSig:	19.2706

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	541.751
SR_n_evts_bin2:	380.201
SR_n_evts_bin3:	255.942
== total:	 1177.89

CRZee_n_evts_bin1:	39.0605
CRZee_n_evts_bin2:	31.6794
CRZee_n_evts_bin3:	17.7818
== total:	 88.5217

CRZmm_n_evts_bin1:	52.5978
CRZmm_n_evts_bin2:	27.6173
CRZmm_n_evts_bin3:	26.0776
== total:	 106.293

CRWep_n_evts_bin1:	1.65726
CRWep_n_evts_bin2:	0.204441
CRWep_n_evts_bin3:	0.294883
== total:	 2.15658

CRWen_n_evts_bin1:	1.64618
CRWen_n_evts_bin2:	1.61861
CRWen_n_evts_bin3:	0.691131
== total:	 3.95592

CRWmp_n_evts_bin1:	7.75677
CRWmp_n_evts_bin2:	5.76761
CRWmp_n_evts_bin3:	4.27091
== total:	 17.7953

CRWmn_n_evts_bin1:	11.0628
CRWmn_n_evts_bin2:	4.91029
CRWmn_n_evts_bin3:	3.24475
== total:	 19.2178

CRWepLowMetSig_n_evts_bin1:	8.7477
CRWepLowMetSig_n_evts_bin2:	4.82443
CRWepLowMetSig_n_evts_bin3:	7.12195
== total:	 20.6941

CRWenLoeMetSig_n_evts_bin1:	9.79144
CRWenLoeMetSig_n_evts_bin2:	7.23865
CRWenLoeMetSig_n_evts_bin3:	2.24047
== total:	 19.2706

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	378.743
SR_n_evts_bin2:	201.773
SR_n_evts_bin3:	127.349
SR_n_evts_bin4:	163.008
SR_n_evts_bin5:	178.428
SR_n_evts_bin6:	128.592
SR_n_evts_bin7:	0
== total:	 1177.89

CRZee_n_evts_bin1:	26.1169
CRZee_n_evts_bin2:	16.5127
CRZee_n_evts_bin3:	9.64297
CRZee_n_evts_bin4:	12.9436
CRZee_n_evts_bin5:	15.1667
CRZee_n_evts_bin6:	8.13883
CRZee_n_evts_bin7:	0
== total:	 88.5216

CRZmm_n_evts_bin1:	37.3943
CRZmm_n_evts_bin2:	15.2641
CRZmm_n_evts_bin3:	14.2576
CRZmm_n_evts_bin4:	15.2035
CRZmm_n_evts_bin5:	12.3532
CRZmm_n_evts_bin6:	11.82
CRZmm_n_evts_bin7:	0
== total:	 106.293

CRWep_n_evts_bin1:	1.15892
CRWep_n_evts_bin2:	0.324318
CRWep_n_evts_bin3:	-0.187961
CRWep_n_evts_bin4:	0.498343
CRWep_n_evts_bin5:	-0.119877
CRWep_n_evts_bin6:	0.482844
CRWep_n_evts_bin7:	0
== total:	 2.15658

CRWen_n_evts_bin1:	1.11639
CRWen_n_evts_bin2:	0.535767
CRWen_n_evts_bin3:	0.252532
CRWen_n_evts_bin4:	0.529789
CRWen_n_evts_bin5:	1.08285
CRWen_n_evts_bin6:	0.438599
CRWen_n_evts_bin7:	0
== total:	 3.95592

CRWmp_n_evts_bin1:	5.92585
CRWmp_n_evts_bin2:	1.54121
CRWmp_n_evts_bin3:	0.594243
CRWmp_n_evts_bin4:	1.83091
CRWmp_n_evts_bin5:	4.2264
CRWmp_n_evts_bin6:	3.67666
CRWmp_n_evts_bin7:	0
== total:	 17.7953

CRWmn_n_evts_bin1:	5.96417
CRWmn_n_evts_bin2:	3.65235
CRWmn_n_evts_bin3:	2.07236
CRWmn_n_evts_bin4:	5.09864
CRWmn_n_evts_bin5:	1.25794
CRWmn_n_evts_bin6:	1.17239
CRWmn_n_evts_bin7:	0
== total:	 19.2179

CRWepLowMetSig_n_evts_bin1:	4.23673
CRWepLowMetSig_n_evts_bin2:	3.27265
CRWepLowMetSig_n_evts_bin3:	3.46842
CRWepLowMetSig_n_evts_bin4:	4.51097
CRWepLowMetSig_n_evts_bin5:	1.55178
CRWepLowMetSig_n_evts_bin6:	3.65352
CRWepLowMetSig_n_evts_bin7:	0
== total:	 20.6941

CRWenLoeMetSig_n_evts_bin1:	5.87546
CRWenLoeMetSig_n_evts_bin2:	4.93237
CRWenLoeMetSig_n_evts_bin3:	-0.919452
CRWenLoeMetSig_n_evts_bin4:	3.91599
CRWenLoeMetSig_n_evts_bin5:	2.30629
CRWenLoeMetSig_n_evts_bin6:	3.15993
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 19.2706

==== SR cutflow ====
passMetTrig:		37928.1
0_el_0_mu:		31871.4
pass_n_jet:		7381.45
pass_lead_jet_pt:		7259.84
pass_sublead_jet_pt:		6737.21
pass_jj_dphi:		5813.3
pass_opp_emisph:		5802.05
pass_jj_deta:		2654.07
pass_jj_mass:		2365
pass_met_tst_et:		1217.91
pass_met_tst_j1_dphi:		1217.46
pass_met_tst_j2_dphi:		1217.46
pass_met_cst_jet:		1177.89

==== CRWe cutflow ====
passLepTrig:		14713.6
1_el_0_mu:		5321.16
el_pt:		5045.5
pass_n_jet:		1590.81
pass_lead_jet_pt:		1221.69
pass_sublead_jet_pt:		640.087
pass_jj_dphi:		416.35
pass_opp_emisph:		395.52
pass_jj_deta:		108.875
pass_jj_mass:		100.297
pass_met_tst_nolep_et:		45.1639
pass_met_tst_nolep_j1_dphi:		45.1639
pass_met_tst_nolep_j2_dphi:		45.1639
pass_met_cst_jet:		46.0772
met_significance:		6.11251

==== CRWm cutflow ====
passLepTrig:		14713.6
0_el_1_mu:		1504.27
mu_pt:		1445.8
pass_n_jet:		319.484
pass_lead_jet_pt:		284.789
pass_sublead_jet_pt:		241.332
pass_jj_dphi:		195.635
pass_opp_emisph:		194.176
pass_jj_deta:		84.4555
pass_jj_mass:		78.2228
pass_met_tst_nolep_et:		38.2543
pass_met_tst_nolep_j1_dphi:		38.2543
pass_met_tst_nolep_j2_dphi:		38.2543
pass_met_cst_jet:		37.0131

==== CRZee cutflow ====
passLepTrig:		14713.6
2_el_0_mu:		2693.4
el_pt:		2683.11
opp_charge:		2599.16
ee_mass:		2165.58
pass_n_jet:		559.46
pass_lead_jet_pt:		532.095
pass_sublead_jet_pt:		473.32
pass_jj_dphi:		408.208
pass_opp_emisph:		407.137
pass_jj_deta:		169.32
pass_jj_mass:		152.037
pass_met_tst_nolep_et:		89.2556
pass_met_tst_nolep_j1_dphi:		89.2556
pass_met_tst_nolep_j2_dphi:		89.2556
pass_met_cst_jet:		88.5215

==== CRZmm cutflow ====
passLepTrig:		14713.6
0_el_2_mu:		3466.21
mu_pt:		3457.05
opp_charge:		3455.65
mm_mass:		2856.08
pass_n_jet:		731.994
pass_lead_jet_pt:		715.096
pass_sublead_jet_pt:		638.422
pass_jj_dphi:		556.961
pass_opp_emisph:		557.742
pass_jj_deta:		241.597
pass_jj_mass:		211.459
pass_met_tst_nolep_et:		107.362
pass_met_tst_nolep_j1_dphi:		107.362
pass_met_tst_nolep_j2_dphi:		107.362
pass_met_cst_jet:		106.293

==== CRWeLowMetSig cutflow ====
passLepTrig:		14713.6
1_el_0_mu:		5321.16
el_pt:		5045.5
pass_n_jet:		1590.81
pass_lead_jet_pt:		1221.69
pass_sublead_jet_pt:		640.087
pass_jj_dphi:		416.35
pass_opp_emisph:		395.52
pass_jj_deta:		108.875
pass_jj_mass:		100.297
pass_met_tst_nolep_et:		45.1639
pass_met_tst_nolep_j1_dphi:		45.1639
pass_met_tst_nolep_j2_dphi:		45.1639
pass_met_cst_jet:		46.0772
low_met_significance:		39.9647

