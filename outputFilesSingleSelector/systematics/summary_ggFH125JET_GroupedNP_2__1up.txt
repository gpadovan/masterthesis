====  summary of selection results  ====
      sample: ggFH125JET_GroupedNP_2__1up
========================================

==== summary of evts. in all regions ====
SR:	130.718
CRWe:	0
CRWm:	0
CRZee:	0
CRZmm:	0
CRWeLowMetSig:	0

==== events in charge-split regions ====
CRWep:	0
CRWen:	0
CRWmp:	0
CRWmn:	0
CRWepLowMetSig:	0
CRWenLowMetSig:	0

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	51.1869
SR_n_evts_bin2:	17.9754
SR_n_evts_bin3:	61.5559
== total:	 130.718

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
== total:	 0

CRWep_n_evts_bin1:	0
CRWep_n_evts_bin2:	0
CRWep_n_evts_bin3:	0
== total:	 0

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0
CRWen_n_evts_bin3:	0
== total:	 0

CRWmp_n_evts_bin1:	0
CRWmp_n_evts_bin2:	0
CRWmp_n_evts_bin3:	0
== total:	 0

CRWmn_n_evts_bin1:	0
CRWmn_n_evts_bin2:	0
CRWmn_n_evts_bin3:	0
== total:	 0

CRWepLowMetSig_n_evts_bin1:	0
CRWepLowMetSig_n_evts_bin2:	0
CRWepLowMetSig_n_evts_bin3:	0
== total:	 0

CRWenLoeMetSig_n_evts_bin1:	0
CRWenLoeMetSig_n_evts_bin2:	0
CRWenLoeMetSig_n_evts_bin3:	0
== total:	 0

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	45.0199
SR_n_evts_bin2:	14.6813
SR_n_evts_bin3:	38.1784
SR_n_evts_bin4:	6.16696
SR_n_evts_bin5:	3.29402
SR_n_evts_bin6:	23.3776
SR_n_evts_bin7:	0
== total:	 130.718

CRZee_n_evts_bin1:	0
CRZee_n_evts_bin2:	0
CRZee_n_evts_bin3:	0
CRZee_n_evts_bin4:	0
CRZee_n_evts_bin5:	0
CRZee_n_evts_bin6:	0
CRZee_n_evts_bin7:	0
== total:	 0

CRZmm_n_evts_bin1:	0
CRZmm_n_evts_bin2:	0
CRZmm_n_evts_bin3:	0
CRZmm_n_evts_bin4:	0
CRZmm_n_evts_bin5:	0
CRZmm_n_evts_bin6:	0
CRZmm_n_evts_bin7:	0
== total:	 0

CRWep_n_evts_bin1:	0
CRWep_n_evts_bin2:	0
CRWep_n_evts_bin3:	0
CRWep_n_evts_bin4:	0
CRWep_n_evts_bin5:	0
CRWep_n_evts_bin6:	0
CRWep_n_evts_bin7:	0
== total:	 0

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0
CRWen_n_evts_bin3:	0
CRWen_n_evts_bin4:	0
CRWen_n_evts_bin5:	0
CRWen_n_evts_bin6:	0
CRWen_n_evts_bin7:	0
== total:	 0

CRWmp_n_evts_bin1:	0
CRWmp_n_evts_bin2:	0
CRWmp_n_evts_bin3:	0
CRWmp_n_evts_bin4:	0
CRWmp_n_evts_bin5:	0
CRWmp_n_evts_bin6:	0
CRWmp_n_evts_bin7:	0
== total:	 0

CRWmn_n_evts_bin1:	0
CRWmn_n_evts_bin2:	0
CRWmn_n_evts_bin3:	0
CRWmn_n_evts_bin4:	0
CRWmn_n_evts_bin5:	0
CRWmn_n_evts_bin6:	0
CRWmn_n_evts_bin7:	0
== total:	 0

CRWepLowMetSig_n_evts_bin1:	0
CRWepLowMetSig_n_evts_bin2:	0
CRWepLowMetSig_n_evts_bin3:	0
CRWepLowMetSig_n_evts_bin4:	0
CRWepLowMetSig_n_evts_bin5:	0
CRWepLowMetSig_n_evts_bin6:	0
CRWepLowMetSig_n_evts_bin7:	0
== total:	 0

CRWenLoeMetSig_n_evts_bin1:	0
CRWenLoeMetSig_n_evts_bin2:	0
CRWenLoeMetSig_n_evts_bin3:	0
CRWenLoeMetSig_n_evts_bin4:	0
CRWenLoeMetSig_n_evts_bin5:	0
CRWenLoeMetSig_n_evts_bin6:	0
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 0

==== SR cutflow ====
passMetTrig:		1539.66
0_el_0_mu:		1539.66
pass_n_jet:		454.893
pass_lead_jet_pt:		453.063
pass_sublead_jet_pt:		435.264
pass_jj_dphi:		423.934
pass_opp_emisph:		423.934
pass_jj_deta:		211.047
pass_jj_mass:		189.943
pass_met_tst_et:		130.718
pass_met_tst_j1_dphi:		130.718
pass_met_tst_j2_dphi:		130.718
pass_met_cst_jet:		130.718

==== CRWe cutflow ====
passLepTrig:		8.41253
1_el_0_mu:		0
el_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0
met_significance:		0

==== CRWm cutflow ====
passLepTrig:		8.41253
0_el_1_mu:		0
mu_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZee cutflow ====
passLepTrig:		8.41253
2_el_0_mu:		0
el_pt:		0
opp_charge:		0
ee_mass:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRZmm cutflow ====
passLepTrig:		8.41253
0_el_2_mu:		0
mu_pt:		0
opp_charge:		0
mm_mass:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0

==== CRWeLowMetSig cutflow ====
passLepTrig:		8.41253
1_el_0_mu:		0
el_pt:		0
pass_n_jet:		0
pass_lead_jet_pt:		0
pass_sublead_jet_pt:		0
pass_jj_dphi:		0
pass_opp_emisph:		0
pass_jj_deta:		0
pass_jj_mass:		0
pass_met_tst_nolep_et:		0
pass_met_tst_nolep_j1_dphi:		0
pass_met_tst_nolep_j2_dphi:		0
pass_met_cst_jet:		0
low_met_significance:		0

