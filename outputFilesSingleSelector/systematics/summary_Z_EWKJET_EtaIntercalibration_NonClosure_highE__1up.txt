====  summary of selection results  ====
      sample: Z_EWKJET_EtaIntercalibration_NonClosure_highE__1up
========================================

==== summary of evts. in all regions ====
SR:	196.521
CRWe:	0.815261
CRWm:	4.39175
CRZee:	18.3909
CRZmm:	22.0977
CRWeLowMetSig:	5.74133

==== events in charge-split regions ====
CRWep:	0.636842
CRWen:	0.17842
CRWmp:	1.74386
CRWmn:	2.6479
CRWepLowMetSig:	2.81453
CRWenLowMetSig:	2.9268

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	39.9259
SR_n_evts_bin2:	60.8012
SR_n_evts_bin3:	95.7936
== total:	 196.521

CRZee_n_evts_bin1:	4.51448
CRZee_n_evts_bin2:	5.07449
CRZee_n_evts_bin3:	8.80195
== total:	 18.3909

CRZmm_n_evts_bin1:	4.31052
CRZmm_n_evts_bin2:	7.1947
CRZmm_n_evts_bin3:	10.5925
== total:	 22.0977

CRWep_n_evts_bin1:	0.204044
CRWep_n_evts_bin2:	0.135162
CRWep_n_evts_bin3:	0.297636
== total:	 0.636842

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0.040077
CRWen_n_evts_bin3:	0.138343
== total:	 0.17842

CRWmp_n_evts_bin1:	0.393166
CRWmp_n_evts_bin2:	0.448717
CRWmp_n_evts_bin3:	0.901973
== total:	 1.74386

CRWmn_n_evts_bin1:	0.633111
CRWmn_n_evts_bin2:	0.814764
CRWmn_n_evts_bin3:	1.20002
== total:	 2.6479

CRWepLowMetSig_n_evts_bin1:	0.461561
CRWepLowMetSig_n_evts_bin2:	1.12383
CRWepLowMetSig_n_evts_bin3:	1.22914
== total:	 2.81453

CRWenLoeMetSig_n_evts_bin1:	0.429975
CRWenLoeMetSig_n_evts_bin2:	0.838916
CRWenLoeMetSig_n_evts_bin3:	1.65791
== total:	 2.9268

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	26.1779
SR_n_evts_bin2:	30.7246
SR_n_evts_bin3:	40.6773
SR_n_evts_bin4:	13.748
SR_n_evts_bin5:	30.0766
SR_n_evts_bin6:	55.1161
SR_n_evts_bin7:	0
== total:	 196.521

CRZee_n_evts_bin1:	2.92461
CRZee_n_evts_bin2:	3.02125
CRZee_n_evts_bin3:	4.03323
CRZee_n_evts_bin4:	1.58987
CRZee_n_evts_bin5:	2.05325
CRZee_n_evts_bin6:	4.76873
CRZee_n_evts_bin7:	0
== total:	 18.3909

CRZmm_n_evts_bin1:	3.03753
CRZmm_n_evts_bin2:	3.02069
CRZmm_n_evts_bin3:	4.62823
CRZmm_n_evts_bin4:	1.27299
CRZmm_n_evts_bin5:	4.174
CRZmm_n_evts_bin6:	5.96429
CRZmm_n_evts_bin7:	0
== total:	 22.0977

CRWep_n_evts_bin1:	0.0930242
CRWep_n_evts_bin2:	0.0512438
CRWep_n_evts_bin3:	0.0813908
CRWep_n_evts_bin4:	0.111019
CRWep_n_evts_bin5:	0.0839181
CRWep_n_evts_bin6:	0.216246
CRWep_n_evts_bin7:	0
== total:	 0.636842

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0.040077
CRWen_n_evts_bin3:	0.0862128
CRWen_n_evts_bin4:	0
CRWen_n_evts_bin5:	0
CRWen_n_evts_bin6:	0.0521298
CRWen_n_evts_bin7:	0
== total:	 0.17842

CRWmp_n_evts_bin1:	0.305338
CRWmp_n_evts_bin2:	0.344283
CRWmp_n_evts_bin3:	0.245892
CRWmp_n_evts_bin4:	0.0878283
CRWmp_n_evts_bin5:	0.104434
CRWmp_n_evts_bin6:	0.65608
CRWmp_n_evts_bin7:	0
== total:	 1.74386

CRWmn_n_evts_bin1:	0.25797
CRWmn_n_evts_bin2:	0.271299
CRWmn_n_evts_bin3:	0.469519
CRWmn_n_evts_bin4:	0.375141
CRWmn_n_evts_bin5:	0.543465
CRWmn_n_evts_bin6:	0.730502
CRWmn_n_evts_bin7:	0
== total:	 2.6479

CRWepLowMetSig_n_evts_bin1:	0.274074
CRWepLowMetSig_n_evts_bin2:	0.497681
CRWepLowMetSig_n_evts_bin3:	0.526755
CRWepLowMetSig_n_evts_bin4:	0.187487
CRWepLowMetSig_n_evts_bin5:	0.626149
CRWepLowMetSig_n_evts_bin6:	0.702386
CRWepLowMetSig_n_evts_bin7:	0
== total:	 2.81453

CRWenLoeMetSig_n_evts_bin1:	0.203366
CRWenLoeMetSig_n_evts_bin2:	0.170073
CRWenLoeMetSig_n_evts_bin3:	0.879844
CRWenLoeMetSig_n_evts_bin4:	0.226609
CRWenLoeMetSig_n_evts_bin5:	0.668844
CRWenLoeMetSig_n_evts_bin6:	0.778066
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 2.9268

==== SR cutflow ====
passMetTrig:		1669.14
0_el_0_mu:		1389.79
pass_n_jet:		771.039
pass_lead_jet_pt:		769.692
pass_sublead_jet_pt:		764.584
pass_jj_dphi:		634.169
pass_opp_emisph:		633.952
pass_jj_deta:		294.477
pass_jj_mass:		287.51
pass_met_tst_et:		197.325
pass_met_tst_j1_dphi:		197.325
pass_met_tst_j2_dphi:		197.325
pass_met_cst_jet:		196.521

==== CRWe cutflow ====
passLepTrig:		504.205
1_el_0_mu:		114.085
el_pt:		110.027
pass_n_jet:		52.3619
pass_lead_jet_pt:		50.0449
pass_sublead_jet_pt:		45.3369
pass_jj_dphi:		29.3162
pass_opp_emisph:		28.8487
pass_jj_deta:		11.1283
pass_jj_mass:		10.8378
pass_met_tst_nolep_et:		6.65609
pass_met_tst_nolep_j1_dphi:		6.65609
pass_met_tst_nolep_j2_dphi:		6.60459
pass_met_cst_jet:		6.5566
met_significance:		0.815261

==== CRWm cutflow ====
passLepTrig:		504.205
0_el_1_mu:		40.6348
mu_pt:		39.8667
pass_n_jet:		20.1211
pass_lead_jet_pt:		19.9868
pass_sublead_jet_pt:		19.2733
pass_jj_dphi:		15.2215
pass_opp_emisph:		15.1674
pass_jj_deta:		7.57678
pass_jj_mass:		7.43213
pass_met_tst_nolep_et:		4.39175
pass_met_tst_nolep_j1_dphi:		4.39175
pass_met_tst_nolep_j2_dphi:		4.39175
pass_met_cst_jet:		4.39175

==== CRZee cutflow ====
passLepTrig:		504.205
2_el_0_mu:		142.621
el_pt:		142.565
opp_charge:		140.42
ee_mass:		123.737
pass_n_jet:		72.5065
pass_lead_jet_pt:		72.3079
pass_sublead_jet_pt:		71.3743
pass_jj_dphi:		60.1583
pass_opp_emisph:		60.1583
pass_jj_deta:		27.3156
pass_jj_mass:		26.4555
pass_met_tst_nolep_et:		18.4789
pass_met_tst_nolep_j1_dphi:		18.4789
pass_met_tst_nolep_j2_dphi:		18.4789
pass_met_cst_jet:		18.3909

==== CRZmm cutflow ====
passLepTrig:		504.205
0_el_2_mu:		168.31
mu_pt:		168.15
opp_charge:		168.15
mm_mass:		147.257
pass_n_jet:		86.5106
pass_lead_jet_pt:		86.4665
pass_sublead_jet_pt:		85.4232
pass_jj_dphi:		71.3389
pass_opp_emisph:		71.2155
pass_jj_deta:		34.3874
pass_jj_mass:		33.6004
pass_met_tst_nolep_et:		22.2389
pass_met_tst_nolep_j1_dphi:		22.2389
pass_met_tst_nolep_j2_dphi:		22.2389
pass_met_cst_jet:		22.0977

==== CRWeLowMetSig cutflow ====
passLepTrig:		504.205
1_el_0_mu:		114.085
el_pt:		110.027
pass_n_jet:		52.3619
pass_lead_jet_pt:		50.0449
pass_sublead_jet_pt:		45.3369
pass_jj_dphi:		29.3162
pass_opp_emisph:		28.8487
pass_jj_deta:		11.1283
pass_jj_mass:		10.8378
pass_met_tst_nolep_et:		6.65609
pass_met_tst_nolep_j1_dphi:		6.65609
pass_met_tst_nolep_j2_dphi:		6.60459
pass_met_cst_jet:		6.5566
low_met_significance:		5.74133

