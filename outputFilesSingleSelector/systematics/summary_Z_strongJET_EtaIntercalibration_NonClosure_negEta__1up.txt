====  summary of selection results  ====
      sample: Z_strongJET_EtaIntercalibration_NonClosure_negEta__1up
========================================

==== summary of evts. in all regions ====
SR:	1169.95
CRWe:	5.3365
CRWm:	37.622
CRZee:	89.7271
CRZmm:	105.214
CRWeLowMetSig:	40.9901

==== events in charge-split regions ====
CRWep:	1.61396
CRWen:	3.72254
CRWmp:	18.9087
CRWmn:	18.7133
CRWepLowMetSig:	22.3815
CRWenLowMetSig:	18.6085

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	537.774
SR_n_evts_bin2:	377.816
SR_n_evts_bin3:	254.362
== total:	 1169.95

CRZee_n_evts_bin1:	41.4244
CRZee_n_evts_bin2:	30.5177
CRZee_n_evts_bin3:	17.7851
== total:	 89.7272

CRZmm_n_evts_bin1:	52.2912
CRZmm_n_evts_bin2:	25.9515
CRZmm_n_evts_bin3:	26.9708
== total:	 105.214

CRWep_n_evts_bin1:	1.24893
CRWep_n_evts_bin2:	0.0681989
CRWep_n_evts_bin3:	0.296836
== total:	 1.61396

CRWen_n_evts_bin1:	1.33377
CRWen_n_evts_bin2:	1.69167
CRWen_n_evts_bin3:	0.697102
== total:	 3.72254

CRWmp_n_evts_bin1:	9.08088
CRWmp_n_evts_bin2:	4.58022
CRWmp_n_evts_bin3:	5.24763
== total:	 18.9087

CRWmn_n_evts_bin1:	10.458
CRWmn_n_evts_bin2:	4.51683
CRWmn_n_evts_bin3:	3.73848
== total:	 18.7133

CRWepLowMetSig_n_evts_bin1:	9.64839
CRWepLowMetSig_n_evts_bin2:	5.86117
CRWepLowMetSig_n_evts_bin3:	6.87197
== total:	 22.3815

CRWenLoeMetSig_n_evts_bin1:	10.6714
CRWenLoeMetSig_n_evts_bin2:	5.78793
CRWenLoeMetSig_n_evts_bin3:	2.14918
== total:	 18.6086

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	370.651
SR_n_evts_bin2:	201.196
SR_n_evts_bin3:	125.438
SR_n_evts_bin4:	167.122
SR_n_evts_bin5:	176.619
SR_n_evts_bin6:	128.924
SR_n_evts_bin7:	0
== total:	 1169.95

CRZee_n_evts_bin1:	27.5807
CRZee_n_evts_bin2:	15.5878
CRZee_n_evts_bin3:	9.58545
CRZee_n_evts_bin4:	13.8437
CRZee_n_evts_bin5:	14.9299
CRZee_n_evts_bin6:	8.19963
CRZee_n_evts_bin7:	0
== total:	 89.7272

CRZmm_n_evts_bin1:	37.4723
CRZmm_n_evts_bin2:	13.7752
CRZmm_n_evts_bin3:	14.6977
CRZmm_n_evts_bin4:	14.8189
CRZmm_n_evts_bin5:	12.1763
CRZmm_n_evts_bin6:	12.2731
CRZmm_n_evts_bin7:	0
== total:	 105.214

CRWep_n_evts_bin1:	1.00423
CRWep_n_evts_bin2:	0.188076
CRWep_n_evts_bin3:	-0.187961
CRWep_n_evts_bin4:	0.244703
CRWep_n_evts_bin5:	-0.119877
CRWep_n_evts_bin6:	0.484796
CRWep_n_evts_bin7:	0
== total:	 1.61396

CRWen_n_evts_bin1:	0.913443
CRWen_n_evts_bin2:	0.608824
CRWen_n_evts_bin3:	0.252532
CRWen_n_evts_bin4:	0.420324
CRWen_n_evts_bin5:	1.08285
CRWen_n_evts_bin6:	0.44457
CRWen_n_evts_bin7:	0
== total:	 3.72254

CRWmp_n_evts_bin1:	6.37413
CRWmp_n_evts_bin2:	1.56223
CRWmp_n_evts_bin3:	1.14249
CRWmp_n_evts_bin4:	2.70674
CRWmp_n_evts_bin5:	3.01799
CRWmp_n_evts_bin6:	4.10514
CRWmp_n_evts_bin7:	0
== total:	 18.9087

CRWmn_n_evts_bin1:	5.26884
CRWmn_n_evts_bin2:	3.5847
CRWmn_n_evts_bin3:	2.34888
CRWmn_n_evts_bin4:	5.18917
CRWmn_n_evts_bin5:	0.932129
CRWmn_n_evts_bin6:	1.3896
CRWmn_n_evts_bin7:	0
== total:	 18.7133

CRWepLowMetSig_n_evts_bin1:	4.23917
CRWepLowMetSig_n_evts_bin2:	4.15801
CRWepLowMetSig_n_evts_bin3:	3.51733
CRWepLowMetSig_n_evts_bin4:	5.40921
CRWepLowMetSig_n_evts_bin5:	1.70316
CRWepLowMetSig_n_evts_bin6:	3.35464
CRWepLowMetSig_n_evts_bin7:	0
== total:	 22.3815

CRWenLoeMetSig_n_evts_bin1:	6.756
CRWenLoeMetSig_n_evts_bin2:	3.66087
CRWenLoeMetSig_n_evts_bin3:	-1.01081
CRWenLoeMetSig_n_evts_bin4:	3.91544
CRWenLoeMetSig_n_evts_bin5:	2.12707
CRWenLoeMetSig_n_evts_bin6:	3.15999
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 18.6086

==== SR cutflow ====
passMetTrig:		37240.2
0_el_0_mu:		31282.7
pass_n_jet:		7401.72
pass_lead_jet_pt:		7276.36
pass_sublead_jet_pt:		6746.3
pass_jj_dphi:		5822.68
pass_opp_emisph:		5812.63
pass_jj_deta:		2645.22
pass_jj_mass:		2345.45
pass_met_tst_et:		1213.74
pass_met_tst_j1_dphi:		1213.29
pass_met_tst_j2_dphi:		1213.25
pass_met_cst_jet:		1169.95

==== CRWe cutflow ====
passLepTrig:		14715.4
1_el_0_mu:		5429.53
el_pt:		5153.75
pass_n_jet:		1707.13
pass_lead_jet_pt:		1223.54
pass_sublead_jet_pt:		626.803
pass_jj_dphi:		413.524
pass_opp_emisph:		391.196
pass_jj_deta:		107.07
pass_jj_mass:		96.4948
pass_met_tst_nolep_et:		46.8023
pass_met_tst_nolep_j1_dphi:		46.8023
pass_met_tst_nolep_j2_dphi:		46.8023
pass_met_cst_jet:		46.3266
met_significance:		5.3365

==== CRWm cutflow ====
passLepTrig:		14715.4
0_el_1_mu:		1468.34
mu_pt:		1407.81
pass_n_jet:		301.221
pass_lead_jet_pt:		271.645
pass_sublead_jet_pt:		233.937
pass_jj_dphi:		194.728
pass_opp_emisph:		192.127
pass_jj_deta:		82.0325
pass_jj_mass:		74.2098
pass_met_tst_nolep_et:		39.0497
pass_met_tst_nolep_j1_dphi:		39.0497
pass_met_tst_nolep_j2_dphi:		39.0497
pass_met_cst_jet:		37.622

==== CRZee cutflow ====
passLepTrig:		14715.4
2_el_0_mu:		2657.04
el_pt:		2647.71
opp_charge:		2564.47
ee_mass:		2133.92
pass_n_jet:		551.472
pass_lead_jet_pt:		525.895
pass_sublead_jet_pt:		471.623
pass_jj_dphi:		408.062
pass_opp_emisph:		407.182
pass_jj_deta:		166.854
pass_jj_mass:		149.47
pass_met_tst_nolep_et:		90.4116
pass_met_tst_nolep_j1_dphi:		90.4116
pass_met_tst_nolep_j2_dphi:		90.4116
pass_met_cst_jet:		89.7271

==== CRZmm cutflow ====
passLepTrig:		14715.4
0_el_2_mu:		3420.03
mu_pt:		3411.98
opp_charge:		3410.55
mm_mass:		2816.15
pass_n_jet:		736.061
pass_lead_jet_pt:		725.195
pass_sublead_jet_pt:		647.984
pass_jj_dphi:		562.94
pass_opp_emisph:		563.445
pass_jj_deta:		244.219
pass_jj_mass:		215.278
pass_met_tst_nolep_et:		106.06
pass_met_tst_nolep_j1_dphi:		106.06
pass_met_tst_nolep_j2_dphi:		106.06
pass_met_cst_jet:		105.214

==== CRWeLowMetSig cutflow ====
passLepTrig:		14715.4
1_el_0_mu:		5429.53
el_pt:		5153.75
pass_n_jet:		1707.13
pass_lead_jet_pt:		1223.54
pass_sublead_jet_pt:		626.803
pass_jj_dphi:		413.524
pass_opp_emisph:		391.196
pass_jj_deta:		107.07
pass_jj_mass:		96.4948
pass_met_tst_nolep_et:		46.8023
pass_met_tst_nolep_j1_dphi:		46.8023
pass_met_tst_nolep_j2_dphi:		46.8023
pass_met_cst_jet:		46.3266
low_met_significance:		40.9901

