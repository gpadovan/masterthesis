====  summary of selection results  ====
      sample: Z_EWKJET_GroupedNP_2__1down
========================================

==== summary of evts. in all regions ====
SR:	193.175
CRWe:	0.808204
CRWm:	4.17506
CRZee:	17.5832
CRZmm:	20.7127
CRWeLowMetSig:	5.58229

==== events in charge-split regions ====
CRWep:	0.629739
CRWen:	0.178465
CRWmp:	1.75513
CRWmn:	2.41993
CRWepLowMetSig:	2.9385
CRWenLowMetSig:	2.64379

==== events in 3-bin strategy regions ====
SR_n_evts_bin1:	38.9482
SR_n_evts_bin2:	62.1796
SR_n_evts_bin3:	92.0473
== total:	 193.175

CRZee_n_evts_bin1:	4.27
CRZee_n_evts_bin2:	4.98183
CRZee_n_evts_bin3:	8.33139
== total:	 17.5832

CRZmm_n_evts_bin1:	3.74999
CRZmm_n_evts_bin2:	7.31615
CRZmm_n_evts_bin3:	9.64655
== total:	 20.7127

CRWep_n_evts_bin1:	0.197159
CRWep_n_evts_bin2:	0.134944
CRWep_n_evts_bin3:	0.297636
== total:	 0.629739

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0.0401225
CRWen_n_evts_bin3:	0.138343
== total:	 0.178465

CRWmp_n_evts_bin1:	0.393166
CRWmp_n_evts_bin2:	0.517951
CRWmp_n_evts_bin3:	0.84401
== total:	 1.75513

CRWmn_n_evts_bin1:	0.542929
CRWmn_n_evts_bin2:	0.865105
CRWmn_n_evts_bin3:	1.0119
== total:	 2.41993

CRWepLowMetSig_n_evts_bin1:	0.51668
CRWepLowMetSig_n_evts_bin2:	1.16656
CRWepLowMetSig_n_evts_bin3:	1.25526
== total:	 2.9385

CRWenLoeMetSig_n_evts_bin1:	0.373345
CRWenLoeMetSig_n_evts_bin2:	0.883006
CRWenLoeMetSig_n_evts_bin3:	1.38744
== total:	 2.64379

==== events in 7-bin strategy regions ====
SR_n_evts_bin1:	26.4281
SR_n_evts_bin2:	30.9562
SR_n_evts_bin3:	38.622
SR_n_evts_bin4:	12.5201
SR_n_evts_bin5:	31.2234
SR_n_evts_bin6:	53.4252
SR_n_evts_bin7:	0
== total:	 193.175

CRZee_n_evts_bin1:	2.71938
CRZee_n_evts_bin2:	3.00109
CRZee_n_evts_bin3:	3.93052
CRZee_n_evts_bin4:	1.55062
CRZee_n_evts_bin5:	1.98074
CRZee_n_evts_bin6:	4.40087
CRZee_n_evts_bin7:	0
== total:	 17.5832

CRZmm_n_evts_bin1:	2.76897
CRZmm_n_evts_bin2:	3.04156
CRZmm_n_evts_bin3:	4.1828
CRZmm_n_evts_bin4:	0.98102
CRZmm_n_evts_bin5:	4.27459
CRZmm_n_evts_bin6:	5.46375
CRZmm_n_evts_bin7:	0
== total:	 20.7127

CRWep_n_evts_bin1:	0.0931847
CRWep_n_evts_bin2:	0.0512438
CRWep_n_evts_bin3:	0.0813908
CRWep_n_evts_bin4:	0.103974
CRWep_n_evts_bin5:	0.0837004
CRWep_n_evts_bin6:	0.216246
CRWep_n_evts_bin7:	0
== total:	 0.629739

CRWen_n_evts_bin1:	0
CRWen_n_evts_bin2:	0.0401225
CRWen_n_evts_bin3:	0.0862128
CRWen_n_evts_bin4:	0
CRWen_n_evts_bin5:	0
CRWen_n_evts_bin6:	0.0521298
CRWen_n_evts_bin7:	0
== total:	 0.178465

CRWmp_n_evts_bin1:	0.305338
CRWmp_n_evts_bin2:	0.343467
CRWmp_n_evts_bin3:	0.244153
CRWmp_n_evts_bin4:	0.0878283
CRWmp_n_evts_bin5:	0.174483
CRWmp_n_evts_bin6:	0.599856
CRWmp_n_evts_bin7:	0
== total:	 1.75513

CRWmn_n_evts_bin1:	0.25797
CRWmn_n_evts_bin2:	0.310668
CRWmn_n_evts_bin3:	0.43015
CRWmn_n_evts_bin4:	0.28496
CRWmn_n_evts_bin5:	0.554437
CRWmn_n_evts_bin6:	0.581747
CRWmn_n_evts_bin7:	0
== total:	 2.41993

CRWepLowMetSig_n_evts_bin1:	0.274074
CRWepLowMetSig_n_evts_bin2:	0.49778
CRWepLowMetSig_n_evts_bin3:	0.52685
CRWepLowMetSig_n_evts_bin4:	0.242606
CRWepLowMetSig_n_evts_bin5:	0.668779
CRWepLowMetSig_n_evts_bin6:	0.728409
CRWepLowMetSig_n_evts_bin7:	0
== total:	 2.9385

CRWenLoeMetSig_n_evts_bin1:	0.24477
CRWenLoeMetSig_n_evts_bin2:	0.304568
CRWenLoeMetSig_n_evts_bin3:	0.654825
CRWenLoeMetSig_n_evts_bin4:	0.128575
CRWenLoeMetSig_n_evts_bin5:	0.578437
CRWenLoeMetSig_n_evts_bin6:	0.732615
CRWenLoeMetSig_n_evts_bin7:	0
== total:	 2.64379

==== SR cutflow ====
passMetTrig:		1585
0_el_0_mu:		1319.7
pass_n_jet:		762.719
pass_lead_jet_pt:		761.218
pass_sublead_jet_pt:		756.157
pass_jj_dphi:		629.23
pass_opp_emisph:		628.959
pass_jj_deta:		293.722
pass_jj_mass:		286.887
pass_met_tst_et:		194.27
pass_met_tst_j1_dphi:		194.27
pass_met_tst_j2_dphi:		194.27
pass_met_cst_jet:		193.175

==== CRWe cutflow ====
passLepTrig:		476.766
1_el_0_mu:		108.105
el_pt:		104.253
pass_n_jet:		51.9047
pass_lead_jet_pt:		49.6986
pass_sublead_jet_pt:		44.6582
pass_jj_dphi:		29.2466
pass_opp_emisph:		28.873
pass_jj_deta:		10.9131
pass_jj_mass:		10.6459
pass_met_tst_nolep_et:		6.48995
pass_met_tst_nolep_j1_dphi:		6.48995
pass_met_tst_nolep_j2_dphi:		6.43849
pass_met_cst_jet:		6.3905
met_significance:		0.808204

==== CRWm cutflow ====
passLepTrig:		476.766
0_el_1_mu:		37.9109
mu_pt:		37.1902
pass_n_jet:		19.5611
pass_lead_jet_pt:		19.3868
pass_sublead_jet_pt:		18.6778
pass_jj_dphi:		14.8412
pass_opp_emisph:		14.7871
pass_jj_deta:		7.43628
pass_jj_mass:		7.21977
pass_met_tst_nolep_et:		4.17506
pass_met_tst_nolep_j1_dphi:		4.17506
pass_met_tst_nolep_j2_dphi:		4.17506
pass_met_cst_jet:		4.17506

==== CRZee cutflow ====
passLepTrig:		476.766
2_el_0_mu:		135.399
el_pt:		135.297
opp_charge:		133.185
ee_mass:		117.83
pass_n_jet:		71.2127
pass_lead_jet_pt:		71.0141
pass_sublead_jet_pt:		70.1631
pass_jj_dphi:		59.0561
pass_opp_emisph:		59.0561
pass_jj_deta:		26.5224
pass_jj_mass:		25.8649
pass_met_tst_nolep_et:		17.659
pass_met_tst_nolep_j1_dphi:		17.659
pass_met_tst_nolep_j2_dphi:		17.659
pass_met_cst_jet:		17.5832

==== CRZmm cutflow ====
passLepTrig:		476.766
0_el_2_mu:		158.466
mu_pt:		158.306
opp_charge:		158.306
mm_mass:		138.695
pass_n_jet:		84.6579
pass_lead_jet_pt:		84.614
pass_sublead_jet_pt:		83.5931
pass_jj_dphi:		70.0847
pass_opp_emisph:		69.9613
pass_jj_deta:		33.0522
pass_jj_mass:		32.1209
pass_met_tst_nolep_et:		20.8913
pass_met_tst_nolep_j1_dphi:		20.8913
pass_met_tst_nolep_j2_dphi:		20.8913
pass_met_cst_jet:		20.7127

==== CRWeLowMetSig cutflow ====
passLepTrig:		476.766
1_el_0_mu:		108.105
el_pt:		104.253
pass_n_jet:		51.9047
pass_lead_jet_pt:		49.6986
pass_sublead_jet_pt:		44.6582
pass_jj_dphi:		29.2466
pass_opp_emisph:		28.873
pass_jj_deta:		10.9131
pass_jj_mass:		10.6459
pass_met_tst_nolep_et:		6.48995
pass_met_tst_nolep_j1_dphi:		6.48995
pass_met_tst_nolep_j2_dphi:		6.43849
pass_met_cst_jet:		6.3905
low_met_significance:		5.58229

