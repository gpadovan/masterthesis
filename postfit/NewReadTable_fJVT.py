from array import *
from ROOT import *
import  sys, getopt, os
import  math

if len(sys.argv) != 3: 
  print ">>>>> Launch as python ReadTable.py NAME_OF_TABLE_DIRECTORY NUMBERBIN<<<<<"
  raise RuntimeError()

table_set = sys.argv[1]

#path = "./Yields/" + table_set + "/"
path = "/afs/cern.ch/user/g/gpadovan/work/StatsTools/HistFitterCode/HistFitterTutorial/" + table_set + "/"

#bins = [200,250,300,350,400,500,600,700,800,900,1000,1100,1200,1300] #non serve
#sbins = ['200','250','300','350','400','500','600','700','800','900','1000','1100','1200'] #non serve: le mie regioni sono regioneBin e le tratto come una regione
#binArray = array('d',bins)
#samples = ['Obs','Znunu','Wmunu','Wenu','Wtaunu','Zmumu','Zee','Ztautau','ttbar','singletop','diboson','NCB','multijet','errMCtot','MCtot']
#samples = ['Obs','Wmunu','Wenu','Wtaunu','Zmumu','Zee','Ztautau','Znunu','diboson','top','NCB','multijet','errMCtot','MCtot'] #miei sample; Obs, sono i dati
samples = ['Obs','multijet','W_strong','Z_strong','W_EWK','Z_EWK','ttbar','VBFH125','ggFH125','eleFakes','errMCtot','MCtot']

#regions = ['SR','CR1mu0b','CR1e0b','CR2mu','CR2e','CR1L1b'] #mie regioni di conrrolo sgn
regions = ['oneEleNegCR1','oneEleNegCR2','oneEleNegCR3','oneEleNegLowSigCR1','oneEleNegLowSigCR2','oneEleNegLowSigCR3','oneElePosCR1','oneElePosCR2','oneElePosCR3','oneElePosLowSigCR1','oneElePosLowSigCR2','oneElePosLowSigCR3','oneMuNegCR1','oneMuNegCR2','oneMuNegCR3','oneMuPosCR1','oneMuPosCR2','oneMuPosCR3','SR1_cuts','SR2_cuts','SR3_cuts','twoEleCR1','twoEleCR2','twoEleCR3','twoMuCR1','twoMuCR2','twoMuCR3']


h = dict() #dizionerio = mappa di C++

which = ['pre','post']
for t in which:
    h[t]=dict() #l iesimo elelnto e una mappa
    for reg in regions:
        h[t][reg]=dict()
        for s in samples:
            h[t][reg][s] = TH1F(reg+'_'+s+'_'+t,'',1,0,1) #alla fine e' una mappa 3D i cui elementi sono istogrammi. Gli istogrammi hanno N bin, come volgio io. Conviene fare istogrammi di 3 bin e poi comporre tutto. Posso anche fare istogrammi di singolo bin, pero' poi e' piu' difficile ricostruire.

fileList = []
#for i in range(len(sbins)): fileList.append('table_' + table_set + "_" + sbins[i]+'.tex')
fileList.append('VBFHinv_fJVT_'+str(sys.argv[2])+'.tex')

#for i,fileName in enumerate(fileList):
for i,fileName in enumerate(fileList):
    input_file = path+fileName 
    table = open(input_file,"r")

    for line in table:
        if 'begin' in line: continue
        if 'end' in line: continue
        if 'hline' in line: continue
        if '&' not in line: continue
        if 'After' in line: continue
        if 'table' in line: continue
        if 'textbf' in line: continue

        if 'Fitted' in line or 'Observed' in line: t = 'post'
        elif 'MC exp' in line: t = 'pre'

        elements = line.split('&')

        if 'Observed' in line: 
          elements.pop(0)
          for k,r in enumerate(regions):
                print line
                h['pre'][r]['Obs'].SetBinContent(i+1,float(elements[k].replace('$','').replace(' ','').replace('\\\\','')))
                h['pre'][r]['Obs'].SetBinError(i+1,math.sqrt(float(elements[k].replace('$','').replace(' ','').replace('\\\\',''))))
                h['post'][r]['Obs'].SetBinContent(i+1,float(elements[k].replace('$','').replace(' ','').replace('\\\\','')))
                h['post'][r]['Obs'].SetBinError(i+1,math.sqrt(float(elements[k].replace('$','').replace(' ','').replace('\\\\',''))))
          continue

        if t == 'post':
            sample = (elements[0].split())[1] 
            cbin = sample.split('\_VBF')
            #print cbin
            elements.pop(0)

            for k,r in enumerate(regions):
                cbin[0] = cbin[0].replace('\\','')
                if 'pm' in elements[k]:
                  el = elements[k].split('\pm')
                  print el,r,k,s
                  print cbin[0]
                  if 'bkg' in line: cbin[0] = 'MCtot'
                  h[t][r][cbin[0]].SetBinContent(i+1,float(el[0].replace('$','')))
                  h[t][r][cbin[0]].SetBinError(i+1,float(el[1].replace('$','').replace(' ','').replace('\\\\',''))) 
                elif '}^{' in elements[k]:
                  el = elements[k].split('{')
                  h[t][r][cbin[0]].SetBinContent(i+1,float(el[0].replace('_','').replace('$','').replace('\\\\','')))
                  h[t][r][cbin[0]].SetBinError(i+1,(float(el[1].replace('-','').replace('}^',''))+float(el[2].replace('+','').replace('}$','').replace('\\\\','')))/2)

        if t == 'pre':

            #if 'SM' in line: continue

            sample = (elements[0].split())[2] 
            cbin = sample.split('\_VBF')#
            elements.pop(0)

            for k,r in enumerate(regions):
                cbin[0] = cbin[0].replace('\\','')
                if 'pm' in elements[k]:
                  el = elements[k].split('\pm')
                  if 'SM' in line: cbin[0] = 'MCtot'
                  h[t][r][cbin[0]].SetBinContent(i+1,float(el[0].replace('$','')))
                  h[t][r][cbin[0]].SetBinError(i+1,float(el[1].replace('$','').replace(' ','').replace('\\\\',''))) 
                elif '}^{' in elements[k]:
                  el = elements[k].split('{')
                  h[t][r][cbin[0]].SetBinContent(i+1,float(el[0].replace('_','').replace('$','').replace('\\\\','')))
                  h[t][r][cbin[0]].SetBinError(i+1,(float(el[1].replace('-','').replace('}^',''))+float(el[2].replace('+','').replace('}$','').replace('\\\\','')))/2)


for t in which:
    output = TFile("output_"+t+"fit_275_" + table_set + "_fJVT_" + str(sys.argv[2]) +".root", "recreate")
    for reg in regions:
        for s in samples:
            h[t][reg][s].SetName(reg+'_'+s)
            h[t][reg][s].Write()
